package yhannanum.converter;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import yhannanum.share.Constants;
import yhannanum.share.FileIO;
import yhannanum.share.StringUtil;
import kr.ac.kaist.swrc.jhannanum.hannanum.Workflow;
import kr.ac.kaist.swrc.jhannanum.hannanum.WorkflowFactory;
import kr.ac.kaist.swrc.jhannanum.comm.Eojeol;
import kr.ac.kaist.swrc.jhannanum.comm.Sentence;
import kr.ac.kaist.swrc.jhannanum.comm.PlainSentence;

public class SentenceConverter {

    public static final int ONLY_PLAIN_SENTENCE = 1;
    public static final int ONLY_PLAIN_EOJEOL = 2;
    public static final int ONLY_MORPHEME = 3;
    public static final int ONLY_TAG = 4;
    public static final int ONLY_KOREAN_TAG = 5;
    public static final int MORPHEME_AND_TAG = 6;
    public static final int PLAIN_EOJEOL_AND_MORPHEME = 7;
    public static final int PLAIN_EOJEOL_AND_MORPHEME_AND_TAG = 8;

	private String[] simple22Tags =
            {"S", "SP", "SF", "SL", "SR", "SD", "SE", "SU", "SY", "F",
                    "N", "NC", "ncp", "ncpa", "ncps", "ncn", "ncr",
                    "NQ", "nqpa", "nqpb", "nqpc", "nqq",
                    "NB", "nbu", "nbs", "nbn",
                    "NP", "npp", "npd",
                    "NN", "nnc", "nno",
                    "PV", "PA", "PX", "MM", "MA",
                    "II", "JC", "JX", "JP",
                    "EP", "EC", "ET", "EF",
                    "XP", "XS"};
	private String[] simple09KoreanTags =
            {"기호", "쉼표", "마침표", "여는 따옴표 및 묶음표", "닫는 따옴표 및 묶음표", "이음표", "줄임표", "단위 기호", "기타기호", "외국어",
                    "체언", "보통명사", "서술성명사", "동작성 명사", "상태성 명사", "비서술성 명사", "비서술성 직위 명사",
                    "고유명사", "성", "이름", "성+이름", "기타 - 일반",
                    "의존명사", "단위성 의존명사", "비단위성 의존명사(~하다가 붙는 것)", "비단위성 의존명사",
                    "대명사", "인칭대명사", "지시대명사",
                    "수사", "양수사", "서수사",
                    "동사", "형용사", "보조용언", "관형사", "부사",
                    "감탄사", "격조사", "보조사", "서술격조사",
                    "선어말어미", "연결어미", "전성어미", "종결어미",
                    "접두사", "접미사"};
	
	public <SENTENCE_TYPE> String convertSentenceToString(SENTENCE_TYPE sentence, int convert_type, String eojeolSeperator, String blockSeperator, String baseSeperator) {
		return convertSentenceToString(sentence, convert_type, eojeolSeperator, blockSeperator, baseSeperator, "\n");
	}
	
	public <SENTENCE_TYPE> String convertSentenceToString(SENTENCE_TYPE sentence, int convert_type, String eojeolSeperator, String blockSeperator) {
		return convertSentenceToString(sentence, convert_type, eojeolSeperator, blockSeperator, "/", "\n");
	}
	
	public <SENTENCE_TYPE> String convertSentenceToString(SENTENCE_TYPE sentence, int convert_type, String eojeolSeperator) {
		return convertSentenceToString(sentence, convert_type, eojeolSeperator, " ", "/", "\n");
	}
	
	public <SENTENCE_TYPE> String convertSentenceToString(SENTENCE_TYPE sentence, int convert_type) {
		return convertSentenceToString(sentence, convert_type, "+", " ", "/", "\n");
	}
	
	public <SENTENCE_TYPE> String convertSentenceToString(SENTENCE_TYPE sentence_type, int convert_type, String eojeolSeperator, String blockSeperator, String baseSeperator, String eojeolTypeSeperator) {
		Object objClass = sentence_type.getClass();
		String resultString = null;
		if (PlainSentence.class.equals(objClass)) {
			PlainSentence plainSentence = (PlainSentence)sentence_type;
			switch(convert_type) {
			case ONLY_PLAIN_SENTENCE:
				resultString = plainSentence.getSentence();
				break;
			default:
				break;
			}
		} else if (Sentence.class.equals(objClass)) {
			Sentence sentence = (Sentence)sentence_type;
			switch(convert_type) {
			case ONLY_PLAIN_SENTENCE: {
				String[] plainEojeols = sentence.getPlainEojeols();
				resultString = StringUtil.concatenateStrings(resultString, " ", plainEojeols, " ");
				break;
			}
			case ONLY_PLAIN_EOJEOL: {
				String[] plainEojeols = sentence.getPlainEojeols();
				resultString = StringUtil.concatenateStrings(resultString, " ", plainEojeols, eojeolSeperator);
				break;
			}
			case ONLY_MORPHEME: {
				Eojeol[] eojeols = sentence.getEojeols();
				for (Eojeol eojeol : eojeols) {
					String[] morphemes = eojeol.getMorphemes();
					resultString = StringUtil.concatenateStrings(resultString, eojeolSeperator, morphemes, blockSeperator);
				}
				break;
			}
			case ONLY_TAG: {
				Eojeol[] eojeols = sentence.getEojeols();
				for (Eojeol eojeol : eojeols) {
					String[] tags = eojeol.getTags();
					resultString = StringUtil.concatenateStrings(resultString, eojeolSeperator, tags, blockSeperator);
				}
				break;
			}
			case ONLY_KOREAN_TAG: {
				Eojeol[] eojeols = sentence.getEojeols();
				for (Eojeol eojeol : eojeols) {
					String[] tags = eojeol.getTags();
					String[] koreanTags = new String[tags.length];
					List simple22TagsList = Arrays.asList(simple22Tags);
					for (int indexOfTag = 0; indexOfTag < tags.length; indexOfTag++) {
						String tag = tags[indexOfTag];
						int indexOfSimple22Tag = simple22TagsList.indexOf(tag);
						koreanTags[indexOfTag] = simple09KoreanTags[indexOfSimple22Tag];
					}
					resultString = StringUtil.concatenateStrings(resultString, eojeolSeperator, koreanTags, blockSeperator);
				}
				break;
			}
			case MORPHEME_AND_TAG: {
				Eojeol[] eojeols = sentence.getEojeols();
				for (Eojeol eojeol : eojeols) {
					String[] morphemes = eojeol.getMorphemes();
					String[] tags = eojeol.getTags();
					String[] combinations = StringUtil.combinationOfStrings(morphemes, baseSeperator, tags);
					resultString = StringUtil.concatenateStrings(resultString, eojeolSeperator, combinations, blockSeperator);
				}
				break;
			}
			case PLAIN_EOJEOL_AND_MORPHEME: {
				String[] plainEojeols = sentence.getPlainEojeols();
				Eojeol[] eojeols = sentence.getEojeols();
				String[] combinations = new String[plainEojeols.length];
				for (int indexOfEojeol = 0; indexOfEojeol < plainEojeols.length; indexOfEojeol++) {
					String[] morphemes = eojeols[indexOfEojeol].getMorphemes();
					for (String morpheme : morphemes) {
						if(combinations[indexOfEojeol] == null) {
							combinations[indexOfEojeol] = plainEojeols[indexOfEojeol] + eojeolTypeSeperator;
						} else {
							combinations[indexOfEojeol] += blockSeperator;
						}
						combinations[indexOfEojeol] += morpheme;
					}
				}
				resultString = StringUtil.concatenateStrings(resultString, "", combinations, eojeolSeperator);
				break;
			}
			case PLAIN_EOJEOL_AND_MORPHEME_AND_TAG: {
				String[] plainEojeols = sentence.getPlainEojeols();
				Eojeol[] eojeols = sentence.getEojeols();
				String[] fullCombinations = new String[plainEojeols.length];
				for (int indexOfEojeol = 0; indexOfEojeol < plainEojeols.length; indexOfEojeol++) {
					String[] morphemes = eojeols[indexOfEojeol].getMorphemes();
					String[] tags = eojeols[indexOfEojeol].getTags();
					String[] combinations = StringUtil.combinationOfStrings(morphemes, baseSeperator, tags);
					fullCombinations[indexOfEojeol] = StringUtil.concatenateStrings(plainEojeols[indexOfEojeol], eojeolTypeSeperator, combinations, blockSeperator);
				}
				resultString = StringUtil.concatenateStrings(resultString, "", fullCombinations, eojeolSeperator);
				break;
			}
			default:
				break;
			};
		} else {
			System.out.println("object class error");
		}
		if (resultString == null) {
			return "";
		}
		return resultString;
	}
	
	public <SENTENCE_TYPE> JSONObject convertSentenceToJSON(JSONObject defaultObject, String key, SENTENCE_TYPE sentence_type, int convert_type) throws JSONException{
		Object objClass = sentence_type.getClass();
		JSONArray eojeolJSONArray = null;
		if (defaultObject == null) {
			defaultObject = new JSONObject();
		}
		if (PlainSentence.class.equals(objClass)) {
			PlainSentence plainSentence = (PlainSentence)sentence_type;
			switch(convert_type) {
			case ONLY_PLAIN_SENTENCE:
				eojeolJSONArray = new JSONArray(plainSentence.getSentence());
				if (key == null) {
					key = "sentence";
				}
				break;
			default:
				break;
			}
		} else if (Sentence.class.equals(objClass)) {
			Sentence sentence = (Sentence)sentence_type;
			switch(convert_type) {
			case ONLY_PLAIN_SENTENCE: {
				String[] plainEojeols = sentence.getPlainEojeols();
				if (key == null) {
					key = "sentence";
				}
				defaultObject.put(key, StringUtil.concatenateStrings(null, " ", plainEojeols, " "));
				break;
			}
			case ONLY_PLAIN_EOJEOL: {
				String[] plainEojeols = sentence.getPlainEojeols();
				eojeolJSONArray = new JSONArray(plainEojeols);
				if (key == null) {
					key = "eojeol";
				}
				break;
			}
			case ONLY_MORPHEME: {
				Eojeol[] eojeols = sentence.getEojeols();
				eojeolJSONArray = new JSONArray();
				for (Eojeol eojeol : eojeols) {
					String[] morphemes = eojeol.getMorphemes();
					JSONArray morphemeJSONArray = new JSONArray(morphemes);
					eojeolJSONArray.put(morphemeJSONArray);
				}
				if (key == null) {
					key = "morphemes";
				}
				break;
			}
			case ONLY_TAG: {
				Eojeol[] eojeols = sentence.getEojeols();
				eojeolJSONArray = new JSONArray();
				for (Eojeol eojeol : eojeols) {
					String[] tags = eojeol.getTags();
					JSONArray tagJSONArray = new JSONArray(tags);
					eojeolJSONArray.put(tagJSONArray);
				}
				if (key == null) {
					key = "tags";
				}
				break;
			}
			case ONLY_KOREAN_TAG: {
				Eojeol[] eojeols = sentence.getEojeols();
				eojeolJSONArray = new JSONArray();
				for (Eojeol eojeol : eojeols) {
					String[] tags = eojeol.getTags();
					String[] koreanTags = new String[tags.length];
					List simple22TagsList = Arrays.asList(simple22Tags);
					for (int indexOfTag = 0; indexOfTag < tags.length; indexOfTag++) {
						String tag = tags[indexOfTag];
						int indexOfSimple22Tag = simple22TagsList.indexOf(tag);
						koreanTags[indexOfTag] = simple09KoreanTags[indexOfSimple22Tag];
					}
					JSONArray koreanTagJSONArray = new JSONArray(koreanTags);
					eojeolJSONArray.put(koreanTagJSONArray);
				}
				if (key == null) {
					key = "koreantags";
				}
				break;
			}
			case MORPHEME_AND_TAG: {
				Eojeol[] eojeols = sentence.getEojeols();
				eojeolJSONArray = new JSONArray();
				for (Eojeol eojeol : eojeols) {
					String[] morphemes = eojeol.getMorphemes();
					String[] tags = eojeol.getTags();
					JSONArray eojeolInsideJSONArray = new JSONArray();
					for (int indexOfMorpheme = 0; indexOfMorpheme < morphemes.length; indexOfMorpheme++) {
						JSONObject morphemeAndTagJSONObject = new JSONObject();
						morphemeAndTagJSONObject.put("morpheme", morphemes[indexOfMorpheme]);
						morphemeAndTagJSONObject.put("tag", tags[indexOfMorpheme]);
						eojeolInsideJSONArray.put(morphemeAndTagJSONObject);
					}
					eojeolJSONArray.put(eojeolInsideJSONArray);
				}
				if (key == null) {
					key = "morphemes_and_tags";
				}
				break;
			}
			case PLAIN_EOJEOL_AND_MORPHEME: {
				String[] plainEojeols = sentence.getPlainEojeols();
				Eojeol[] eojeols = sentence.getEojeols();
				eojeolJSONArray = new JSONArray();
				for (int indexOfEojeol = 0; indexOfEojeol < plainEojeols.length; indexOfEojeol++) {
					JSONObject eojeolJSONObject = new JSONObject();
					eojeolJSONObject.put("eojeol", plainEojeols[indexOfEojeol]);
					String[] morphemes = eojeols[indexOfEojeol].getMorphemes();
					eojeolJSONObject.put("morphemes", new JSONArray(morphemes));
					eojeolJSONArray.put(eojeolJSONObject);
				}
				if (key == null) {
					key = "eojeols_and_morphemesArray";
				}
				break;
			}
			
			case PLAIN_EOJEOL_AND_MORPHEME_AND_TAG: {
				String[] plainEojeols = sentence.getPlainEojeols();
				Eojeol[] eojeols = sentence.getEojeols();
				eojeolJSONArray = new JSONArray();
				for (int indexOfEojeol = 0; indexOfEojeol < plainEojeols.length; indexOfEojeol++) {
					JSONObject eojeolJSONObject = new JSONObject();
					eojeolJSONObject.put("eojeol", plainEojeols[indexOfEojeol]);
					String[] morphemes = eojeols[indexOfEojeol].getMorphemes();
					String[] tags = eojeols[indexOfEojeol].getTags();
					JSONArray eojeolInsideJSONArray = new JSONArray();
					for (int indexOfMorpheme = 0; indexOfMorpheme < morphemes.length; indexOfMorpheme++) {
						JSONObject morphemeAndTagJSONObject = new JSONObject();
						morphemeAndTagJSONObject.put("morpheme", morphemes[indexOfMorpheme]);
						morphemeAndTagJSONObject.put("tag", tags[indexOfMorpheme]);
						eojeolInsideJSONArray.put(morphemeAndTagJSONObject);
					}
					eojeolJSONObject.put("morpheme_and_tag", eojeolInsideJSONArray);
					eojeolJSONArray.put(eojeolJSONObject);
				}
				if (key == null) {
					key = "eojeols_and_morphemesArray_and_tagsArray";
				}
				break;
			}
			
			default:
				break;
			};
		} else {
			System.out.println("object class error");
		}
		if (convert_type != ONLY_PLAIN_SENTENCE) {
			defaultObject.put(key, eojeolJSONArray);
		}
		return defaultObject;
	}
	
	public static void main(String[] args) {
		try {
			File readDirectory = new File(Constants.CRAWLER_JSON_DIRECTORY_PATH);
			File writeDirectory = new File(Constants.RULE_TAG_MARKER_SOURCE_DIRECTORY);
			String[] readFileList = readDirectory.list();
			String[] writeFileList = writeDirectory.list();

			//List<String> readFileStringList = ArrayUtil.substractList(readFileList, writeFileList);
			List<String> readFileStringList = Arrays.asList(readFileList);
			System.out.println("number of files : " + readFileStringList.size());
			
			Workflow workflow = WorkflowFactory.getPredefinedWorkflow(WorkflowFactory.WORKFLOW_POS_SIMPLE_22); 
			workflow.activateWorkflow(true);
			SentenceConverter sc = new SentenceConverter();
			
			for (String fileName : readFileStringList) {
				System.out.println((readFileStringList.indexOf(fileName) + 1) + "/" + readFileStringList.size() + " " + fileName);
				
				String filePath = readDirectory.getPath() + "/" + fileName;
				String writePath = writeDirectory.getPath(); 
				
				if (FileIO.getExtension(filePath).equals("json")) {
					String jsonFile = FileIO.readFile(filePath);
					JSONTokener tokener = new JSONTokener(jsonFile);
					JSONObject object = new JSONObject(tokener);
					String content = (String)object.get("content");
					
					if (content.length() == 0) continue;
					
					content +=".";
					
					workflow.analyze(content);
					
					JSONArray resultArray = new JSONArray();
					
					// String analyzedString = null;
					
					while (true) {
						Sentence s = workflow.getResultOfSentence(new Sentence(0, 0, false));
						JSONObject resultObject = sc.convertSentenceToJSON(null, null, s, ONLY_PLAIN_SENTENCE);
						resultObject = sc.convertSentenceToJSON(resultObject, null, s, ONLY_MORPHEME);
						resultObject = sc.convertSentenceToJSON(resultObject, null, s, ONLY_KOREAN_TAG);
						resultObject = sc.convertSentenceToJSON(resultObject, null, s, ONLY_TAG);
                        resultObject = sc.convertSentenceToJSON(resultObject, null, s, ONLY_PLAIN_EOJEOL);
						resultArray.put(resultObject);

						/*
						String plainSentence = sc.convertSentenceToString(s, CONVERT_TYPE.ONLY_PLAINSENTENCE);
						String morpheme = sc.convertSentenceToString(s, CONVERT_TYPE.ONLY_MORPHEME, " ", "+");
						if (analyzedString == null) {
							analyzedString = plainSentence;
						} else {
							analyzedString += "\n\n" + plainSentence;
						}
						if (analyzedString == null ) {
							analyzedString = morpheme;
						} else {
							analyzedString += "\n" + morpheme;
						}
						*/
						if (s.isEndOfDocument()) {
							break;
						}
					}
					object.put("morphemeAnalyze", resultArray);
					// object.put("morphemeAnalyze", analyzedString);
					FileIO.writeFile(writePath + "/" + FileIO.removeExtension(fileName) + ".json", object.toString());
				}
			}
			workflow.close();
			System.out.println("converting done");
		} catch (Exception e) {
			
		}
	}

}
