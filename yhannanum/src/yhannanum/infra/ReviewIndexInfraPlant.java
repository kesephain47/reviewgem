package yhannanum.infra;

import kr.ac.kaist.swrc.jhannanum.comm.Sentence;
import kr.ac.kaist.swrc.jhannanum.hannanum.WorkflowFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import yhannanum.comm.Crawling;
import yhannanum.converter.SentenceConverter;
import yhannanum.db.CrawlingTable;
import yhannanum.share.ArrayUtil;
import yhannanum.share.Constants;
import yhannanum.share.FileIO;
import yhannanum.share.JSONUtil;

import java.io.*;
import java.util.*;

/**
 * Created by Arthur on 2014. 5. 13..
 */
public class ReviewIndexInfraPlant extends InfraPlant {
    private HashMap<String, Integer> wordMap = new HashMap<String, Integer>();
    private Integer[] uids = null;
    private final static String INDEX_FILE_NAME = "/index.json";
    private final static String UIDS_FILE_NAME = "/uids.json";
    private File writeDirectory = FileIO.getDirectory(Constants.REVIEW_INDEX_DIRECTORY_PATH);

    public ReviewIndexInfraPlant(String readDirectoryName, String writeDirectoryName, int workflowFlag, int[] convertTypes, boolean override) throws Exception {
        super(readDirectoryName, writeDirectoryName, workflowFlag, convertTypes, override);

        CrawlingTable crawlingTable = new CrawlingTable();
        crawlingTable.connect();
        Crawling[] crawlings = crawlingTable.readCrawlingTable("uid > 2768");
        crawlingTable.close();

        String savedUids = FileIO.readFile(writeDirectory.getPath() + UIDS_FILE_NAME);
        Integer[] savedUidsArray = null;
        if (savedUids == null) savedUidsArray = new Integer[0];
        else {
            savedUids = savedUids.replaceAll("[\\[\\]]", "");
            if (savedUids.length() == 0) savedUidsArray = new Integer[0];
            else {
                String[] savedUidsStrings = savedUids.split(", ");
                savedUidsArray = new Integer[savedUidsStrings.length];
                for (int i = 0; i < savedUidsStrings.length; i++) {
                    savedUidsArray[i] = Integer.valueOf(savedUidsStrings[i]);
                }
            }
        }

        if (override) {
            uids = Crawling.getUids(crawlings);
        } else {
            List<Integer> uidsList = ArrayUtil.substractList(Crawling.getUids(crawlings), savedUidsArray);

            uids = uidsList.toArray(new Integer[uidsList.size()]);
        }

        HashMap<Integer, String> fileNameForUid = Crawling.getFileNameForUid(crawlings);

        String[] selectedFileNames = new String[uids.length];
        for (int i = 0; i < uids.length; i++) {
            selectedFileNames[i] = fileNameForUid.get(uids[i]);
        }

        setToReadFileNames(selectedFileNames);

        wordMap = JSONUtil.makeJsonStringToHashMap(FileIO.readFile(writeDirectory.getPath() + INDEX_FILE_NAME));

        File reviewDirectory = new File(getWriteDirectory().getPath() + "/review");
        if (!reviewDirectory.exists()) {
            reviewDirectory.mkdir();
            File searchDirectory = new File(reviewDirectory.getPath() + "/search");
            if (!searchDirectory.exists()) {
                searchDirectory.mkdir();
            }
        }
    }

    private void readWordsFromDirectory() {
        String[] directoryNumbers = writeDirectory.list();
        for (String directoryNumber : directoryNumbers) {
            File subDirectory = new File(writeDirectory.getPath() + "/" + directoryNumber);
            if (subDirectory.isFile()) continue;
            String[] words = subDirectory.list();
            for (String word : words) {
                wordMap.put(word, Integer.valueOf(directoryNumber));
            }
        }
    }

    @Override
    public boolean skipReadFile(int fileIndex) { return false; }

    @Override
    public boolean skipAnalyzing(int fileIndex) {
        return false;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T processConvertedSetence(int fileIndex, Sentence sentence, JSONObject convertedSentence) throws Exception {
        Integer reviewId = uids[fileIndex];
        getToReadFileName(fileIndex);
        Integer sentenceId = sentence.getSentenceID();
        JSONArray morphemeArray = convertedSentence.getJSONArray("morphemes");
        for (int i = 0; i < morphemeArray.length(); i++) {
            JSONArray eojeol = morphemeArray.getJSONArray(i);
            for (int j = 0; j < eojeol.length(); j++) {
                String morpheme = eojeol.getString(j);
                addStringToIndicesInfo(morpheme, reviewId, sentenceId, i);
            }
        }
        JSONArray eojeolArray = convertedSentence.getJSONArray("eojeol");
        for (int i = 0; i < eojeolArray.length(); i++) {
            String eojeol = eojeolArray.getString(i);
            addStringToIndicesInfo(eojeol, reviewId, sentenceId, i);
        }
        return (T)convertedSentence.getString("sentence");
    }

    /**
     * Add only content and analyzing morpheme results.
     * @param originalJsonFile - original json file, which contains contents exactly same with read file
     * @param convertedSentences - converted sentences array
     * @param <T> - type of return.
     * @return - return JSONObject
     * @throws JSONException
     */
    @Override
    @SuppressWarnings("unchecked")
    public <T> T processConvertedSentences(int fileIndex, JSONObject originalJsonFile, JSONArray convertedSentences) throws Exception {
        JSONObject resultObject = new JSONObject();
        resultObject.put("file_name", getToReadFileName(fileIndex));
        resultObject.put("sentences", convertedSentences);
        return (T)resultObject;
    }
    @Override
    public boolean writeToFile() {
        return true;
    }

    @Override
    public String setFilePathToWrite(int fileIndex, String fileName) throws IOException {
        return "/review/search/" + uids[fileIndex].toString() + "_sentences.json";
    }

    private int NUMBER_OF_DIRECTORY = 0;
    private int NUMBER_OF_FILES = 0;

    @SuppressWarnings("unchecked")
    private void addStringToIndicesInfo(String string, Integer reviewId, Integer sentenceId, Integer eojeolIndex) throws Exception {
        if (string.length() == 0 || string.length() > 20 || string.contains("/") || string.contains(".") || string.contains(",") || string.substring(0, 1).equals("-")) return;

        if (!wordMap.containsKey(string)) {
            wordMap.put(string, NUMBER_OF_DIRECTORY);
            NUMBER_OF_FILES++;
            if (NUMBER_OF_FILES >= 1000) {
                NUMBER_OF_DIRECTORY++;
                NUMBER_OF_FILES = 0;
            }
        }

        File subDirectory = FileIO.getDirectory(writeDirectory.getPath() + "/" + wordMap.get(string));
        File stringDirectory = FileIO.getDirectory(subDirectory.getPath() + "/" + string);
        File reviewDirectory = FileIO.getDirectory(stringDirectory.getPath() + "/" + reviewId);
        String filePath = reviewDirectory.getPath() + "/" + sentenceId;
        if (!(new File(filePath)).exists()) {
            FileIO.writeFile(filePath, "[]");
        }
        String inputString = FileIO.readFile(filePath);
        JSONArray inputJson = new JSONArray(inputString);

        boolean has = false;
        for (int i = 0; i < inputJson.length(); i++) {
            if (inputJson.getInt(i) == eojeolIndex) has = true;
        }
        if (!has) inputJson.put(eojeolIndex);

        String outputString = inputJson.toString();
        FileIO.writeFile(filePath, outputString);
    }

    public void saveToFile() throws IOException {
        String returnString = JSONUtil.makeHashMapToJsonString(wordMap);
        FileIO.writeFile(writeDirectory.getPath() + INDEX_FILE_NAME, returnString);
        FileIO.writeFile(writeDirectory.getPath() + UIDS_FILE_NAME, Arrays.deepToString(uids));
    }

    /**
     * add only sentence, morpheme, eojeols
     * @param args whether override or not.
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        boolean override = (args.length > 0 && args[0].equals("true"));
        int[] convertTypes = {SentenceConverter.ONLY_MORPHEME, SentenceConverter.ONLY_PLAIN_EOJEOL, SentenceConverter.ONLY_PLAIN_SENTENCE};
        ReviewIndexInfraPlant reviewIndexInfraPlant = new ReviewIndexInfraPlant(Constants.CRAWLER_JSON_DIRECTORY_PATH,
                Constants.REVIEW_SUMMARY_DIRECTORY_PATH, WorkflowFactory.WORKFLOW_POS_SIMPLE_22, convertTypes, override);
        reviewIndexInfraPlant.analyzeFiles(true);
        reviewIndexInfraPlant.close(true);
        reviewIndexInfraPlant.saveToFile();
    }
}
