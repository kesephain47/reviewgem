package yhannanum.infra;

import kr.ac.kaist.swrc.jhannanum.comm.Sentence;
import kr.ac.kaist.swrc.jhannanum.hannanum.WorkflowFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import yhannanum.comm.*;
import yhannanum.converter.SentenceConverter;
import yhannanum.db.CrawlingTable;
import yhannanum.db.RuleTable;
import yhannanum.db.SubmitTable;
import yhannanum.share.Constants;
import yhannanum.share.FileIO;

import java.io.IOException;
import java.lang.Exception;
import java.util.*;

/**
 * Created by Arthur on 2014. 4. 23..
 */
public class MarkerInfraPlant extends InfraPlant {
    public MarkerInfraPlant(String readDirectoryName, String writeDirectoryName, int workflowFlag, int[] convertTypes) throws Exception {
        super(readDirectoryName, writeDirectoryName, workflowFlag, convertTypes);
    }

    @Override
    public boolean skipReadFile(int fileIndex) { return false; }

    @Override
    public boolean skipAnalyzing(int fileIndex) {
        return false;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T processConvertedSetence(int fileIndex, Sentence sentence, JSONObject convertedSentence) throws Exception {
        Set<Integer> highlightingEojeolIndices = new HashSet<Integer>();
        for (RuleArgRelation ruleArgRelation : ruleArgRelations) {
            RuleArgument target = ruleArgRelation.getTarget();
            RuleArgument attr = ruleArgRelation.getAttr();
            RuleArgument sentiment = ruleArgRelation.getSentiment();

            List<MorphemeIndex[]> targetIndices = RuleArgument.getOneDepthArgIndicesList(RuleArgument.getArgIndicesIncludedInSentence(sentence, target));
            List<MorphemeIndex[]> attrIndices = RuleArgument.getOneDepthArgIndicesList(RuleArgument.getArgIndicesIncludedInSentence(sentence, attr));
            List<MorphemeIndex[]> sentimentIndices = RuleArgument.getOneDepthArgIndicesList(RuleArgument.getArgIndicesIncludedInSentence(sentence, sentiment));

            if (target != null && target.getMorphemes() != null && target.getMorphemes().length > 0 &&
                    (targetIndices == null || targetIndices.size() == 0)) continue;
            if (attr != null && attr.getMorphemes() != null && attr.getMorphemes().length > 0 &&
                    (attrIndices == null || attrIndices.size() == 0)) continue;
            if (sentiment != null && sentiment.getMorphemes() != null && sentiment.getMorphemes().length > 0 &&
                    (sentimentIndices == null || sentimentIndices.size() == 0)) continue;

            if (targetIndices != null) addEojeolIndicesToList(highlightingEojeolIndices, targetIndices);
            if (attrIndices != null) addEojeolIndicesToList(highlightingEojeolIndices, attrIndices);
            if (sentimentIndices != null) addEojeolIndicesToList(highlightingEojeolIndices, sentimentIndices);
        }

        if (highlightingEojeolIndices.size() > 0) {
            Set<String> highlightingEojeols = new HashSet<String>();
            JSONArray eojeolArray = convertedSentence.getJSONArray("eojeol");
            for (Integer index : highlightingEojeolIndices) {
                highlightingEojeols.add(eojeolArray.getString(index));
            }
            String sentenceString = convertedSentence.getString("sentence");
            JSONArray indicesArray = new JSONArray();
            for (String eojeol : highlightingEojeols) {
                for (int i = -1; (i = sentenceString.indexOf(eojeol, i + 1)) != -1;) {
                    int lastEojeolIndex = i + eojeol.length() - 1;
                    indicesArray.put(new Integer[]{i, lastEojeolIndex});
                }
            }
            convertedSentence.put("highlighting", indicesArray.toString());
        }

        return (T)convertedSentence;
    }

    /**
     * Add analyzing morpheme results to original json file.
     * @param originalJsonFile - original json file, which contains contents exactly same with read file
     * @param convertedSentences - converted sentences array
     * @param <T> - type of return.
     * @return - return JSONObject
     * @throws JSONException
     */
    @Override
    @SuppressWarnings("unchecked")
    public <T> T processConvertedSentences(int fileIndex, JSONObject originalJsonFile, JSONArray convertedSentences) throws Exception {
        originalJsonFile.put("morphemeAnalyze", convertedSentences);
        return (T)originalJsonFile;
    }
    @Override
    public boolean writeToFile() {
        return true;
    }

    @Override
    public String setFilePathToWrite(int fileIndex, String fileName) throws IOException {
        return fileName;
    }

    public void addEojeolIndicesToList(Set<Integer> set, List<MorphemeIndex[]> indices) {
        for (MorphemeIndex[] targetIndex : indices) {
            for (MorphemeIndex index : targetIndex) {
                set.add(index.getEojeol());
            }
        }
    }

    private Rule[] rules = null;

    private RuleArgRelation[] ruleArgRelations = null;

    public void setToReadFileNames(boolean override, boolean withOption, String option) throws Exception {
        if (!withOption) {
            super.setToReadFileNames(override);
        } else {
            if (option.equals("all")) {
                super.setToReadFileNames(override);
            } else if (option.equals("afterPpomppu")) {
                int lastPpomppuId = 2768;
                CrawlingTable crawlingTable = new CrawlingTable();
                crawlingTable.connect();
                Crawling[] crawlings = crawlingTable.readCrawlingTable("uid > " + String.valueOf(lastPpomppuId));
                String[] names = new String[crawlings.length];
                for (int i = 0; i < crawlings.length; i++) {
                    names[i] = FileIO.extractNameFromPath(crawlings[i].getPath());
                }
                crawlingTable.close();
                super.setToReadFileNames(names);
            } else if (option.equals("afterSubmit")) {
                SubmitTable submitTable = new SubmitTable();
                submitTable.connect();
                int largestSubmitedReviewId = submitTable.readMaxSubmitedReviewId();
                CrawlingTable crawlingTable = new CrawlingTable();
                crawlingTable.setConnect(submitTable.getConnect());
                Crawling[] crawlings = crawlingTable.readCrawlingTable("uid > " + String.valueOf(largestSubmitedReviewId));
                String[] names = new String[crawlings.length];
                for (int i = 0; i < crawlings.length; i++) {
                    names[i] = FileIO.extractNameFromPath(crawlings[i].getPath());
                }
                crawlingTable.close();
                submitTable.close();
                super.setToReadFileNames(names);
            } else if (option.contains("next_review_id:")) {
                int lastId = Integer.valueOf(option.replace("next_review_id:", ""));
                CrawlingTable crawlingTable = new CrawlingTable();
                crawlingTable.connect();
                Crawling[] crawlings = crawlingTable.readCrawlingTable("uid >= " + String.valueOf(lastId));
                String[] names = new String[crawlings.length];
                for (int i = 0; i < crawlings.length; i++) {
                    names[i] = FileIO.extractNameFromPath(crawlings[i].getPath());
                }
                crawlingTable.close();
                super.setToReadFileNames(names);
            } else {
                Integer readReviewId = new Integer(option);
                CrawlingTable crawlingTable = new CrawlingTable();
                crawlingTable.connect();
                int nextReviewId = crawlingTable.readMinCrawledReviewId("uid > " + readReviewId);
                Crawling[] crawling = crawlingTable.readCrawlingTable("uid = " + String.valueOf(nextReviewId));
                String name = FileIO.extractNameFromPath(crawling[0].getPath());
                crawlingTable.close();
                super.setToReadFileNames(name);
            }
        }
    }

    @SuppressWarnings("unchecked")
    public void setPredefinedDataSet() throws Exception {
        RuleTable ruleTable = new RuleTable();
        ruleTable.connect();
        rules = ruleTable.readRules("sentiment is not null");
        ruleTable.close();

        ruleArgRelations = RuleArgRelation.getRuleRelations(rules,
                new String[]{"NC", "NB", "NN", "NP", "NQ", "PV", "PA", "PX", "F", "MA", "MM", "SE", "SP", "SU"});
    }

    /**
     * add only sentence, morpheme, eojeols, korean_tag
     * @param args whether override or not.
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        boolean override = (args.length > 0 && args[0].equals("true"));
        boolean withOption = (args.length > 1);
        String option = (withOption ? args[1] : null);
        int[] convertTypes = new int[]{SentenceConverter.ONLY_PLAIN_SENTENCE, SentenceConverter.ONLY_MORPHEME, SentenceConverter.ONLY_KOREAN_TAG,
                SentenceConverter.ONLY_PLAIN_EOJEOL, SentenceConverter.ONLY_TAG};
        MarkerInfraPlant markerInfraPlant = new MarkerInfraPlant(Constants.CRAWLER_JSON_DIRECTORY_PATH, Constants.RULE_TAG_MARKER_SOURCE_DIRECTORY,
                WorkflowFactory.WORKFLOW_POS_SIMPLE_22, convertTypes);
        markerInfraPlant.setToReadFileNames(override, withOption, option);
        markerInfraPlant.setPredefinedDataSet();
        markerInfraPlant.analyzeFiles(true);
        markerInfraPlant.close(true);
    }
}