package yhannanum.infra;

import kr.ac.kaist.swrc.jhannanum.comm.Sentence;
import kr.ac.kaist.swrc.jhannanum.hannanum.WorkflowFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import yhannanum.comm.*;
import yhannanum.converter.SentenceConverter;
import yhannanum.counter.RuleCoverageCounter;
import yhannanum.db.*;
import yhannanum.share.*;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.util.*;

/**
 * Created by Arthur on 2014. 5. 5..
 */
public class HighLightingInfraPlant extends InfraPlant {
    private boolean skipReadFile = false;

    private boolean skipAnalyze = false;

    private Rule[] rules = null;

    private RuleArgument[] attrSet = null;

    private RuleArgument[] sentimentSet = null;

    private RuleArgRelation[] ruleArgRelations = null;

    private CrawledFile[] crawledFiles = null;

    private ArrayList<JSONObject> sentenceHighLightResultOutputList = new ArrayList<JSONObject>();

    private JSONObject fileIndices = new JSONObject();

    private int initialSentenceHighLightResultCount = 0;

    private HashMap<Integer, Integer> crawledFileIndexForReviewId = new HashMap<Integer, Integer>();

    private HighLightResultIndex highLightResultIndex = new HighLightResultIndex();

    private HashMap<Integer, Submit> submitForCrawlUid = new HashMap<Integer, Submit>();

    private HashMap<Integer, Integer> countForRuleId = new HashMap<Integer, Integer>();

    private final List<String> originAttrStrings = Arrays.asList("기타", "디스플레이", "디자인", "카메라", "휴대성", "휴대폰 자체 성능", "내구성", "그립감", "배터리",
            "가격", "해상도");

    private class CrawledFile {
        private Crawling crawling = null;

        private String title = null;

        private Date date = null;

        private JSONArray images = null;

        private String thumbs = null;

        private boolean skipThisFile = false;

        CrawledFile(Crawling crawling) {
            this.crawling = crawling;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void setDate(Date date) {
            this.date = date;
        }

        public void setDate(String date) throws ParseException {
            this.date = DateUtil.dateFromString(date);
        }

        public void setImages(JSONArray images) {
            this.images = images;
        }

        public void setThumbs(String thumbs) {
            this.thumbs = thumbs;
        }

        public void setSkipThisFile(boolean skipThisFile) {
            this.skipThisFile = skipThisFile;
        }

        public void setKeyInfos(JSONObject keyInfos) throws JSONException, ParseException {
            if (keyInfos == null) return;
            if (keyInfos.has("title")) title = keyInfos.getString("title");
            if (keyInfos.has("date")) date = DateUtil.dateFromDateString(keyInfos.getString("date"));
            if (keyInfos.has("images")) images = keyInfos.getJSONArray("images");
            if (keyInfos.has("thumb")) thumbs = keyInfos.getString("thumb");
        }

        public String getTitle() { return title; }

        public Date getDate() { return date; }

        public String getDateString() {
            if (date != null) return DateUtil.reviewCreationDateString(date);
            else return "";
        }

        public JSONArray getImages() { return images; }

        public String getThumbs() { return thumbs; }

        public Crawling getCrawling() { return crawling; }

        public boolean isSkipThisFile() { return skipThisFile; }

        public JSONObject getKeyInfos() throws JSONException {
            JSONObject keyInfos = new JSONObject();
            keyInfos.put("uid", crawling.getUid());
            if (title != null) keyInfos.put("title", title);
            if (date != null) keyInfos.put("date", date);
            if (images != null) keyInfos.put("images", images);
            if (thumbs != null) keyInfos.put("thumb", thumbs);
            return keyInfos;
        }
    }

    private ProsConsForProduct prosConsForProduct = new ProsConsForProduct();

    public HighLightingInfraPlant(String readDirectoryName, String writeDirectoryName, int workflowFlag, int[] convertTypes,
                                  boolean skipReadFile, boolean skipAnalyze) throws Exception {
        super(readDirectoryName, writeDirectoryName, workflowFlag, convertTypes);
        this.skipReadFile = skipReadFile;
        this.skipAnalyze = skipAnalyze;
    }

    @Override
    public boolean skipReadFile(int fileIndex) { return crawledFiles[fileIndex].isSkipThisFile(); }

    @Override
    public boolean skipAnalyzing(int fileIndex) {
        return crawledFiles[fileIndex].isSkipThisFile();
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T processConvertedSetence(int fileIndex, Sentence sentence, JSONObject convertedSentence) throws Exception {
        List<Integer> attrRuleIds = RuleArgument.getIncludedArgsRuleIds(sentence, attrSet);
        List<Integer> sentiRuleIds = RuleArgument.getIncludedArgsRuleIds(sentence, sentimentSet);

        Set<Integer> mergedSet = ArrayUtil.getMergedSet(attrRuleIds, sentiRuleIds);

        RsfWorkerResult rsfWorkerResult = new RsfWorkerResult(sentence.getDocumentID(), sentence.getSentenceID(), sentence);

        for (Integer ruleId : mergedSet) {
            rsfWorkerResult.checkAndAddRuleResult(sentence, ruleId, ruleArgRelations[ruleId]);
        }

        JSONArray eojeols = convertedSentence.getJSONArray("eojeol");

        if (rsfWorkerResult.getRuleIds() != null && rsfWorkerResult.getRuleIds().size() > 0) {
            Crawling crawling = crawledFiles[sentence.getDocumentID()].getCrawling();
            List<Integer> ruleIndices = rsfWorkerResult.getRuleIds();
            JSONObject sentenceHighLightResults = new JSONObject();
            for (int i = 0; i < ruleIndices.size(); i++) {
                int ruleIndex = ruleIndices.get(i);
                Rule rule = rules[ruleIndex];

                MorphemeIndex[] targetIndices = null;
                if (rsfWorkerResult.getTargetIndices().get(i) != null)
                    targetIndices = MorphemeIndex.getMergedIndices(rsfWorkerResult.getTargetIndices().get(i));

                MorphemeIndex[] attrIndices = null;
                if (rsfWorkerResult.getAttrIndices().get(i) != null)
                    attrIndices = MorphemeIndex.getMergedIndices(rsfWorkerResult.getAttrIndices().get(i));

                MorphemeIndex[] sentimentIndices = null;
                if (rsfWorkerResult.getSentimentIndices().get(i) != null)
                    sentimentIndices = MorphemeIndex.getMergedIndices(rsfWorkerResult.getSentimentIndices().get(i));

                int totalIndices = 0;
                if (targetIndices != null) totalIndices += targetIndices.length;
                if (attrIndices != null) totalIndices += attrIndices.length;
                if (sentimentIndices != null) totalIndices += sentimentIndices.length;
                if (totalIndices == 0) continue;

                int productId = crawling.getProductId();
                String originAttr = rule.getOriginAttr();
                int sentimentType = rule.getSentimentType();
                int reviewId = crawling.getUid();
                HashMap<Integer, Integer> allAllHashMap = highLightResultIndex.getAllAllHashMap(productId, reviewId);
                HashMap<Integer, Integer> allSentimentalHashMap = highLightResultIndex.getAllSentimentalHashMap(productId, sentimentType, reviewId);
                HashMap<Integer, Integer> filterAllHashMap = highLightResultIndex.getFilterAllHashMap(productId, originAttr, reviewId);
                HashMap<Integer, Integer> filterSentimentalHashMap = highLightResultIndex.getFilterSentimentalHashMap(productId, originAttr, sentimentType, reviewId);

                int sentenceId = sentence.getSentenceID();
                if (!filterSentimentalHashMap.containsKey(sentenceId)) {
                    SentenceHighLightResult sentenceHighLightResult = new SentenceHighLightResult(crawling.getUid(),
                            sentence.getSentenceID(), convertedSentence.getString("sentence"), sentence, originAttr, sentimentType,
                            targetIndices, attrIndices, sentimentIndices);
                    int sentenceHlResultIndex = sentenceHighLightResultOutputList.size() + sentenceHighLightResults.length();
                    sentenceHighLightResults.put(String.valueOf(sentenceHlResultIndex), sentenceHighLightResult);
                    allAllHashMap.put(sentenceId, sentenceHlResultIndex);
                    allSentimentalHashMap.put(sentenceId, sentenceHlResultIndex);
                    filterAllHashMap.put(sentenceId, sentenceHlResultIndex);
                    filterSentimentalHashMap.put(sentenceId, sentenceHlResultIndex);
                } else {
                    int sentenceHlResultIndex = filterSentimentalHashMap.get(sentenceId);
                    SentenceHighLightResult sentenceHighLightResult = (SentenceHighLightResult)sentenceHighLightResults.get(String.valueOf(sentenceHlResultIndex));
                    sentenceHighLightResult.addIndices(originAttr, sentimentType, targetIndices, attrIndices, sentimentIndices);
                }

                int firstEojeolIndex = 0;
                if (attrIndices != null) {
                    firstEojeolIndex = attrIndices[0].getEojeol();
                }
                int lastEojeolIndex = eojeols.length() - 1;
                if (sentimentIndices != null) {
                    lastEojeolIndex = sentimentIndices[sentimentIndices.length - 1].getEojeol();
                }
                StringBuilder compressSentenceBuilder = new StringBuilder();
                for (int j = firstEojeolIndex; j <= lastEojeolIndex; j++) {
                    if (j != firstEojeolIndex) compressSentenceBuilder.append(" ");
                    compressSentenceBuilder.append(eojeols.getString(j));
                }
                // ToDo 장단점에 대해서도 파일로 저장 및 불러오기 과정이 필요함.
                prosConsForProduct.addSentence(productId, sentimentType, originAttr, rule.getId(), compressSentenceBuilder.toString());

                Integer ruleId = rule.getId();
                Integer countForTargetRuleId = countForRuleId.get(ruleId);
                countForTargetRuleId++;
                countForRuleId.put(ruleId, countForTargetRuleId);
            }

            String[] keys = JSONObject.getNames(sentenceHighLightResults);

            if (keys != null) {
                for (String key : keys) {
                    SentenceHighLightResult sentenceHighLightResult = (SentenceHighLightResult)sentenceHighLightResults.get(key);
                    JSONObject sentenceHighLightResultOutput = new JSONObject();
                    sentenceHighLightResultOutput.put("reviewId", crawling.getUid());
                    List<String> positiveOriginAttr = sentenceHighLightResult.getPositiveOriginAttr();
                    if (positiveOriginAttr.size() > 0) sentenceHighLightResultOutput.put("positiveOriginAttr", positiveOriginAttr);
                    List<String> negativeOriginAttr = sentenceHighLightResult.getNegativeOriginAttr();
                    if (negativeOriginAttr.size() > 0) sentenceHighLightResultOutput.put("negativeOriginAttr", negativeOriginAttr);
                    List<String> neutralOriginAttr = sentenceHighLightResult.getNeutralOriginAttr();
                    if (neutralOriginAttr.size() > 0) sentenceHighLightResultOutput.put("neutralOriginAttr", neutralOriginAttr);
                    JSONObject sentenceHighLightOutputJsonObject = sentenceHighLightResult.getSentenceHighLightOutputJsonObject();
                    sentenceHighLightResultOutput.put("highlight", sentenceHighLightOutputJsonObject.toString());
                    sentenceHighLightResultOutput.put("sentence", sentenceHighLightOutputJsonObject.getString("sentence"));
                    sentenceHighLightResultOutputList.add(sentenceHighLightResultOutput);
                }
            }
        }
        return null;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T processConvertedSentences(int fileIndex, JSONObject originalJsonFile, JSONArray convertedSentences) throws Exception {
        crawledFiles[fileIndex].setTitle(originalJsonFile.getString("title"));
        if (originalJsonFile.has("date")) {
            crawledFiles[fileIndex].setDate(originalJsonFile.getString("date"));
        }
        if (originalJsonFile.has("images")) {
            crawledFiles[fileIndex].setImages(originalJsonFile.getJSONArray("images"));
        }
        if (originalJsonFile.has("thumb")) {
            crawledFiles[fileIndex].setThumbs(originalJsonFile.getString("thumb"));
        }

        return null;
    }

    @Override
    public boolean writeToFile() {
        return false;
    }

    @Override
    public String setFilePathToWrite(int fileIndex, String fileName) throws IOException {
        return null;
    }

    @Override
    public void close(boolean showProgress) {
        super.close(showProgress);
    }

    @SuppressWarnings("unchecked")
    public void setPredefinedDataSet() throws Exception {
        RuleTable ruleTable = new RuleTable();
        ruleTable.connect();
        rules = ruleTable.readRules("sentiment is not null");
        ruleTable.close();

        for (Rule rule : rules) {
            countForRuleId.put(rule.getId(), 0);
        }

        Class rClass = Class.forName("yhannanum.comm.Rule");
        Method getAttrs = rClass.getMethod("getAttrs");
        Method getAttrIndices = rClass.getMethod("getAttrIndices");
        Method getAttrTags = rClass.getMethod("getAttrTags");
        Method getSentiments = rClass.getMethod("getSentiments");
        Method getSentimentIndices = rClass.getMethod("getSentimentIndices");
        Method getSentimentTags = rClass.getMethod("getSentimentTags");
        attrSet = RuleArgument.getRuleArguSet(rules, getAttrs, getAttrIndices, getAttrTags, true);
        sentimentSet = RuleArgument.getRuleArguSet(rules, getSentiments, getSentimentIndices, getSentimentTags, true,
                new String[]{"NC", "NB", "NN", "NP", "NQ", "PV", "PA", "PX", "F", "MA", "MM", "SE", "SP", "SU"});

        ruleArgRelations = RuleArgRelation.getRuleRelations(rules,
                new String[]{"NC", "NB", "NN", "NP", "NQ", "PV", "PA", "PX", "F", "MA", "MM", "SE", "SP", "SU"});
    }

    public void setSubmitedReviewFileNames() throws Exception {
        SubmitTable submitTable = new SubmitTable();
        submitTable.connect();
        Submit[] totalSubmits = submitTable.readSubmitTable();
        for (Submit submit : totalSubmits) {
            submitForCrawlUid.put(submit.getCrawling_uid(), submit);
        }

        String reviewIds = "";
        boolean addComma = false;
        for (int i = 0; i < totalSubmits.length; i++) {
            Submit submit = totalSubmits[i];
            if (submit.getReview_point() != 1) continue;
            if (addComma) reviewIds += ", ";
            reviewIds += submit.getCrawling_uid();
            if (!addComma) addComma = true;
        }
        CrawlingTable crawlingTable = new CrawlingTable();
        crawlingTable.setConnect(submitTable.getConnect());
        Crawling[] crawlings = crawlingTable.readCrawlingTable("uid > 2768 AND disuse <> 1 AND uid not in (" + reviewIds + ")", "url");
        String[] names = new String[crawlings.length];
        crawledFiles = new CrawledFile[crawlings.length];
        for (int i = 0; i < crawledFiles.length; i++) {
            crawledFiles[i] = new CrawledFile(crawlings[i]);
            crawledFileIndexForReviewId.put(crawledFiles[i].getCrawling().getUid(), i);
            names[i] = FileIO.extractNameFromPath(crawledFiles[i].getCrawling().getPath());
        }

        crawlingTable.close();
        submitTable.close();

        setToReadFileNames(names);

        if (!skipAnalyze) {
            File reviewDirectory = FileIO.getDirectory(getWriteDirectory().getPath() + "/review");
            File highLightingDirectory = FileIO.getDirectory(reviewDirectory.getPath() + "/highlighting");

            String indicesString = FileIO.readFile(highLightingDirectory.getPath() + "/indices.json");
            if (indicesString != null) {
                JSONObject indicesJsonObject = new JSONObject(indicesString);
                highLightResultIndex.setAllAllHashMap(indicesJsonObject.getJSONObject(HighLightResultIndex.ALL_ALL));
                highLightResultIndex.setAllSentimentalHashMap(indicesJsonObject.getJSONObject(HighLightResultIndex.ALL_SENTIMENTAL));
                highLightResultIndex.setFilterAllHashMap(indicesJsonObject.getJSONObject(HighLightResultIndex.FILTER_ALL));
                highLightResultIndex.setFilterSentimentalHashMap(indicesJsonObject.getJSONObject(HighLightResultIndex.FILTER_SENTIMENTAL));
            }

            String sentenceHighLightResultOutputListString = "[" + FileIO.readFile(highLightingDirectory.getPath() + "/output_string_json_list") + "]";
            JSONArray sentenceHighLightResultJsonArray = new JSONArray(sentenceHighLightResultOutputListString);
            initialSentenceHighLightResultCount = sentenceHighLightResultJsonArray.length();
            for (int i = 0; i < sentenceHighLightResultJsonArray.length(); i++) {
                sentenceHighLightResultOutputList.add(sentenceHighLightResultJsonArray.getJSONObject(i));
            }

            String keyInfosString = FileIO.readFile(highLightingDirectory.getPath() + "/key_infos.json");
            JSONArray keyInfosJsonArray = new JSONArray(keyInfosString);
            for (CrawledFile crawledFile : crawledFiles) {
                int crawledFileUid = crawledFile.getCrawling().getUid();
                for (int i = 0; i < keyInfosJsonArray.length(); i++) {
                    if (crawledFileUid == keyInfosJsonArray.getJSONObject(i).getInt("uid")) {
                        crawledFile.setKeyInfos(keyInfosJsonArray.getJSONObject(i));
                        crawledFile.setSkipThisFile(true);
                        break;
                    }
                }
            }

            String prosConsSentencesString = FileIO.readFile(highLightingDirectory.getPath() + "/pos_neg_sentences.json");
            JSONArray prosConsSentenceJsonArray = new JSONArray(prosConsSentencesString);
            for (int i = 0; i < prosConsSentenceJsonArray.length(); i++) {
                JSONObject prosConsSentenceJsonObject = prosConsSentenceJsonArray.getJSONObject(i);
                prosConsForProduct.setSentence(prosConsSentenceJsonObject);
            }
        }
    }

    private JSONObject getDefaultReviewOutputJsonObject(Integer reviewId, HashMap<Integer, Integer> reviewHashMap, String[] keysForOutput, String originAttr, int sentimentType) throws JSONException {
        return getDefaultReviewOutputJsonObject(reviewId, reviewHashMap, Arrays.asList(keysForOutput), originAttr, sentimentType);
    }

    private JSONObject getDefaultReviewOutputJsonObject(Integer reviewId, HashMap<Integer, Integer> reviewHashMap, List<String> keysForOutput, String originAttr, int sentimentType) throws JSONException {
        JSONObject reviewOutput = new JSONObject();
        JsonObjectStringBuilder reviewOutputStringBuilder = new JsonObjectStringBuilder();
        if (keysForOutput.contains("review_id")) {
            reviewOutputStringBuilder.putValue("review_id", reviewId);
        }
        if (keysForOutput.contains("count")) {
            reviewOutputStringBuilder.putValue("count", reviewHashMap.size());
        }
        if (keysForOutput.contains("url") || keysForOutput.contains("title") || keysForOutput.contains("creation_time") ||
                keysForOutput.contains("images") || keysForOutput.contains("thumb") || keysForOutput.contains("sentences") ||
                keysForOutput.contains("sentences_without_highlight") || keysForOutput.contains("sentences_with_sentiment")) {
            Integer[] sentenceIds = reviewHashMap.keySet().toArray(new Integer[reviewHashMap.size()]);
            int crawlingIndex = crawledFileIndexForReviewId.get(reviewId);
            CrawledFile crawledFile = crawledFiles[crawlingIndex];
            if (keysForOutput.contains("url")) {
                reviewOutputStringBuilder.putValue("url", crawledFile.getCrawling().getUrl());
            }
            if (keysForOutput.contains("title")) {
                reviewOutputStringBuilder.putValue("title", crawledFile.getTitle());
            }
            if (keysForOutput.contains("creation_time")) {
                reviewOutputStringBuilder.putValue("creation_time", crawledFile.getDateString());
            }
            if (keysForOutput.contains("images")) {
                if (crawledFile.getImages() != null) reviewOutputStringBuilder.putJson("images", crawledFile.getImages());
                else reviewOutputStringBuilder.putValue("images", "");
            }
            if (keysForOutput.contains("thumb")) {
                if (crawledFile.getThumbs() != null) reviewOutputStringBuilder.putValue("thumb", crawledFile.getThumbs());
                else reviewOutputStringBuilder.putValue("thumb", "");
            }
            if (keysForOutput.contains("sentences") || keysForOutput.contains("sentences_without_highlight")) {
                reviewOutput.put("numberOfSentences", sentenceIds.length);
                boolean withoutHighlight = false;
                if (keysForOutput.contains("sentences_without_highlight")) withoutHighlight = true;
                JsonArrayStringBuilder sentencesOutputStringBuilder = new JsonArrayStringBuilder();
                for (Integer sentenceId : sentenceIds) {
                    int sentenceIndex = reviewHashMap.get(sentenceId);
                    JSONObject sentenceHighLightResultOutput = sentenceHighLightResultOutputList.get(sentenceIndex);
                    if (withoutHighlight) {
                        JsonObjectStringBuilder sentenceOutputStringBuilder = new JsonObjectStringBuilder();
                        sentenceOutputStringBuilder.putValue("sentence", sentenceHighLightResultOutput.getString("sentence"));
                        sentencesOutputStringBuilder.putJson(sentenceOutputStringBuilder);
                    } else {
                        sentencesOutputStringBuilder.putJson(sentenceHighLightResultOutput.getString("highlight"));
                    }
                }
                reviewOutputStringBuilder.putJson("sentences", sentencesOutputStringBuilder);
            } else if (keysForOutput.contains("sentences_with_sentiment")) {
                JSONObject sentimentalSentenceOutput = new JSONObject();
                int numberOfSentences = 0;
                for (Integer sentenceId : sentenceIds) {
                    int sentenceIndex = reviewHashMap.get(sentenceId);
                    JSONObject sentenceHighLightResultOutput = sentenceHighLightResultOutputList.get(sentenceIndex);
                    boolean hasPositive = false;
                    boolean hasNegative = false;
                    boolean hasNeutral = false;
                    if (originAttr == null) {
                        if ((sentimentType < 0 || sentimentType == 0) &&
                                sentenceHighLightResultOutput.has("positiveOriginAttr") &&
                                sentenceHighLightResultOutput.getJSONArray("positiveOriginAttr").length() > 0) {
                            hasPositive = true;
                        }
                        if ((sentimentType < 0 || sentimentType == 1) &&
                                sentenceHighLightResultOutput.has("negativeOriginAttr") &&
                                sentenceHighLightResultOutput.getJSONArray("negativeOriginAttr").length() > 0) {
                            hasNegative = true;
                        }
                        if ((sentimentType < 0 || sentimentType == 2) &&
                                sentenceHighLightResultOutput.has("neutralOriginAttr") &&
                                sentenceHighLightResultOutput.getJSONArray("neutralOriginAttr").length() > 0) {
                            hasNeutral = true;
                        }
                    } else {
                        if ((sentimentType < 0 || sentimentType == 0) &&
                                sentenceHighLightResultOutput.has("positiveOriginAttr")) {
                            JSONArray positiveOriginAttr = sentenceHighLightResultOutput.getJSONArray("positiveOriginAttr");
                            for (int i = 0; i < positiveOriginAttr.length(); i++) {
                                if (positiveOriginAttr.getString(i).equals(originAttr)) {
                                    hasPositive = true;
                                    break;
                                }
                            }
                        }
                        if ((sentimentType < 0 || sentimentType == 1) &&
                                sentenceHighLightResultOutput.has("negativeOriginAttr")) {
                            JSONArray negativeOriginAttr = sentenceHighLightResultOutput.getJSONArray("negativeOriginAttr");
                            for (int i = 0;  i < negativeOriginAttr.length(); i++) {
                                if (negativeOriginAttr.getString(i).equals(originAttr)) {
                                    hasNegative = true;
                                    break;
                                }
                            }
                        }
                        if ((sentimentType < 0 || sentimentType == 2) &&
                                sentenceHighLightResultOutput.has("neutralOriginAttr")) {
                            JSONArray neutralOriginAttr = sentenceHighLightResultOutput.getJSONArray("neutralOriginAttr");
                            for (int i = 0; i < neutralOriginAttr.length(); i++) {
                                if (neutralOriginAttr.getString(i).equals(originAttr)) {
                                    hasNeutral = true;
                                    break;
                                }
                            }
                        }
                    }
                    if (hasPositive) {
                        if (!sentimentalSentenceOutput.has("0")) {
                            sentimentalSentenceOutput.put("0", new JSONArray());
                        }
                        JSONArray sentencesOutput = sentimentalSentenceOutput.getJSONArray("0");
                        sentencesOutput.put(sentenceHighLightResultOutput.getString("highlight"));
                        numberOfSentences++;
                    }
                    if (hasNegative) {
                        if (!sentimentalSentenceOutput.has("1")) {
                            sentimentalSentenceOutput.put("1", new JSONArray());
                        }
                        JSONArray sentencesOutput = sentimentalSentenceOutput.getJSONArray("1");
                        sentencesOutput.put(sentenceHighLightResultOutput.getString("highlight"));
                        numberOfSentences++;
                    }
                    if (hasNeutral) {
                        if (!sentimentalSentenceOutput.has("2")) {
                            sentimentalSentenceOutput.put("2", new JSONArray());
                        }
                        JSONArray sentencesOutput = sentimentalSentenceOutput.getJSONArray("2");
                        sentencesOutput.put(sentenceHighLightResultOutput.getString("highlight"));
                        numberOfSentences++;
                    }
                }
                if (!sentimentalSentenceOutput.has("0") && !sentimentalSentenceOutput.has("1")) return null;
                JsonObjectStringBuilder sentimentalSentenceOutputStringBuilder = new JsonObjectStringBuilder();
                if (sentimentalSentenceOutput.has("0")) {
                    JsonArrayStringBuilder positiveOriginAttrStringBuilder = new JsonArrayStringBuilder();
                    JSONArray positiveOriginAttr = sentimentalSentenceOutput.getJSONArray("0");
                    for (int i = 0; i < positiveOriginAttr.length(); i++) {
                        positiveOriginAttrStringBuilder.putJson(positiveOriginAttr.getString(i));
                    }
                    sentimentalSentenceOutputStringBuilder.putJson("0", positiveOriginAttrStringBuilder);
                }
                if (sentimentalSentenceOutput.has("1")) {
                    JsonArrayStringBuilder negativeOriginAttrStringBuilder = new JsonArrayStringBuilder();
                    JSONArray negativeOriginAttr = sentimentalSentenceOutput.getJSONArray("1");
                    for (int i = 0; i < negativeOriginAttr.length(); i++) {
                        negativeOriginAttrStringBuilder.putJson(negativeOriginAttr.getString(i));
                    }
                    sentimentalSentenceOutputStringBuilder.putJson("1", negativeOriginAttrStringBuilder);
                }
                if (sentimentalSentenceOutput.has("2")) {
                    JsonArrayStringBuilder neutralOriginAttrStringBuilder = new JsonArrayStringBuilder();
                    JSONArray neutralOriginAttr = sentimentalSentenceOutput.getJSONArray("2");
                    for (int i = 0; i < neutralOriginAttr.length(); i++) {
                        neutralOriginAttrStringBuilder.putJson(neutralOriginAttr.getString(i));
                    }
                    sentimentalSentenceOutputStringBuilder.putJson("2", neutralOriginAttrStringBuilder);
                }
                reviewOutputStringBuilder.putJson("sentences", sentimentalSentenceOutputStringBuilder);
                reviewOutput.put("numberOfSentences", numberOfSentences);
            }
        }
        reviewOutput.put("string", reviewOutputStringBuilder);
        return reviewOutput;
    }

    private JSONObject getReviewOutputJsonArray(HashMap<Integer, HashMap> hashMap, String[] keysForOutput) throws JSONException, IOException {
        return getReviewOutputJsonArray(hashMap, -1, keysForOutput, null, -1);
    }

    private JSONObject getReviewOutputJsonArray(HashMap<Integer, HashMap> hashMap, int limitForReviewId, String[] keysForOutput) throws JSONException, IOException {
        return getReviewOutputJsonArray(hashMap, limitForReviewId, keysForOutput, null, -1);
    }

    @SuppressWarnings("unchecked")
    private JSONObject getReviewOutputJsonArray(HashMap<Integer, HashMap> hashMap, int limitForReviewId, String[] keysForOutput, String originAttr, int sentimentType) throws JSONException, IOException {
        Integer[] reviewIds = getSortReviewIdsByNumberOfSentences(hashMap);
        JsonArrayStringBuilder reviewOutputArrayStringBuilder = new JsonArrayStringBuilder();
        int numberOfSentences = 0;
        int length = ((limitForReviewId >= 0 && reviewIds.length >= limitForReviewId) ? limitForReviewId : reviewIds.length);
        int totalNumberOfBytes = "{".getBytes("UTF-8").length;
        JSONArray byteIndicesForReviews = new JSONArray();
        ArrayList<Integer> reviewIdsHavingOutput = new ArrayList<Integer>();
        for (int i = 0; i < length; i++) {
            Integer reviewId = reviewIds[i];
            HashMap<Integer, Integer> reviewHashMap = hashMap.get(reviewId);
            JSONObject reviewOutput = getDefaultReviewOutputJsonObject(reviewId, reviewHashMap, keysForOutput, originAttr, sentimentType);
            if (reviewOutput == null) continue;
            reviewIdsHavingOutput.add(reviewId);
            String reviewOutputString = reviewOutput.getString("string");
            reviewOutputArrayStringBuilder.putJson(reviewOutputString);
            if (reviewOutput.has("numberOfSentences")) numberOfSentences += reviewOutput.getInt("numberOfSentences");
            int byteLength = reviewOutputString.getBytes("UTF-8").length;
            JSONArray byteIndicesForReview = new JSONArray();
            byteIndicesForReview.put(totalNumberOfBytes);
            byteIndicesForReview.put(totalNumberOfBytes + byteLength);
            byteIndicesForReviews.put(byteIndicesForReview);
            totalNumberOfBytes += byteLength + ",".getBytes("UTF-8").length;
        }
        JSONObject reviewOutputJsonObject = new JSONObject();
        reviewOutputJsonObject.put("sortedReviewIds", reviewIdsHavingOutput.toArray(new Integer[reviewIdsHavingOutput.size()]));
        reviewOutputJsonObject.put("string", reviewOutputArrayStringBuilder);
        reviewOutputJsonObject.put("numberOfSentences", numberOfSentences);
        reviewOutputJsonObject.put("indices", byteIndicesForReviews);
        return reviewOutputJsonObject;
    }

    @SuppressWarnings("unchecked")
    private void getMainReviewOutputJsonArray(JsonArrayStringBuilder destinationJsonArrayStringBuilder, HashMap<Integer, HashMap> hashMap, Integer productId) throws JSONException {
        Integer[] reviewIds = getSortReviewIdsByNumberOfSentences(hashMap);
        int lastIndexOfSource = (reviewIds.length >= 5 ? 5 : reviewIds.length);
        for (int i = 0; i < lastIndexOfSource; i++) {
            Integer reviewId = reviewIds[i];;

            HashMap<Integer, Integer> reviewHashMap = hashMap.get(reviewId);
            String[] keysForOutput = new String[]{"title", "thumb"};
            JSONObject reviewOutput = getDefaultReviewOutputJsonObject(reviewId, reviewHashMap, keysForOutput, null, -1);
            String reviewOutputString = reviewOutput.getString("string");
            JsonObjectStringBuilder reviewOutputStringBuilder = new JsonObjectStringBuilder(reviewOutputString);
            reviewOutputStringBuilder.putValue("product_id", productId);

            destinationJsonArrayStringBuilder.putJson(reviewOutputStringBuilder);
        }
    }

    public void saveToFile() throws Exception {
        saveHighLightingResults();
        saveProductFilterSentimentalJson();
        saveProductFilterJson();
        saveProductAllSentimentalJson();
        saveProductAllJson();

        FileIO.writeFile(getWriteDirectory().getPath() + "/product/indices.json", fileIndices.toString());

        RuleCoverageCounter ruleCoverageCounter = new RuleCoverageCounter(submitForCrawlUid, rules, countForRuleId);
        ruleCoverageCounter.saveForRuleId();
        ruleCoverageCounter.saveForDate();
    }

    private void saveHighLightingResults() throws Exception {
        File reviewDirectory = FileIO.getDirectory(getWriteDirectory().getPath() + "/review");
        File highLightingDirectory = FileIO.getDirectory(reviewDirectory.getPath() + "/highlighting");

        JsonObjectStringBuilder indicesJsonStringBuilder = new JsonObjectStringBuilder();
        indicesJsonStringBuilder.putJson(HighLightResultIndex.ALL_ALL, highLightResultIndex.getAllAllHashMapJsonString());
        indicesJsonStringBuilder.putJson(HighLightResultIndex.ALL_SENTIMENTAL, highLightResultIndex.getAllSentimentalJsonString());
        indicesJsonStringBuilder.putJson(HighLightResultIndex.FILTER_ALL, highLightResultIndex.getFilterAllJsonString());
        indicesJsonStringBuilder.putJson(HighLightResultIndex.FILTER_SENTIMENTAL, highLightResultIndex.getFilterSentimentalJsonString());
        FileIO.writeFile(highLightingDirectory.getPath() + "/indices.json", indicesJsonStringBuilder.toString());

        StringBuilder sentenceHighLightResultOutputListString = new StringBuilder();
        for (int i = initialSentenceHighLightResultCount; i < sentenceHighLightResultOutputList.size(); i++) {
            if (i != initialSentenceHighLightResultCount) sentenceHighLightResultOutputListString.append(", ");
            sentenceHighLightResultOutputListString.append(sentenceHighLightResultOutputList.get(i).toString());
        }

        if (skipAnalyze) {
            FileIO.writeFile(highLightingDirectory.getPath() + "/output_string_json_list", sentenceHighLightResultOutputListString.toString());
        } else if (sentenceHighLightResultOutputList.size() - initialSentenceHighLightResultCount > 0) {
            String sentenceResults = FileIO.readFile(highLightingDirectory.getPath() + "/output_string_json_list");
            if (sentenceResults != null && sentenceResults.length() > 0) {
                sentenceHighLightResultOutputListString.insert(0, ", ");
            }
            FileIO.writeFile(highLightingDirectory.getPath() + "/output_string_json_list", sentenceHighLightResultOutputListString.toString(), true);
        }

        JSONArray crawledFileInfos = new JSONArray();
        for (int i = 0; i < crawledFiles.length; i++) {
            JSONObject crawledFileInfo = crawledFiles[i].getKeyInfos();
            crawledFileInfos.put(crawledFileInfo);
        }
        FileIO.writeFile(highLightingDirectory.getPath() + "/key_infos.json", crawledFileInfos.toString());

        JSONArray prosConsJsonArray = prosConsForProduct.toJsonArray();
        FileIO.writeFile(highLightingDirectory.getPath() + "/pos_neg_sentences.json", prosConsJsonArray.toString());
    }

    private String[] getSortedOriginAttrForSentimentType(JSONArray originAttrSentimentCounts, int sentimentType) throws JSONException {
        for (int i = 0; i < originAttrSentimentCounts.length(); i++) {
            for (int j = i + 1; j < originAttrSentimentCounts.length(); j++) {
                JSONObject originAttrCount1 = originAttrSentimentCounts.getJSONObject(i);
                JSONObject originAttrCount2 = originAttrSentimentCounts.getJSONObject(j);
                int positive1 = originAttrCount1.getInt("positive");
                int negative1 = originAttrCount1.getInt("negative");
                int positive2 = originAttrCount2.getInt("positive");
                int negative2 = originAttrCount2.getInt("negative");
                float sentimentTypeRatio1 = positive1 + negative1;
                float sentimentTypeRatio2 = positive2 + negative2;
                if (sentimentType == 0) {
                    sentimentTypeRatio1 = (float)positive1 / sentimentTypeRatio1;
                    sentimentTypeRatio2 = (float)positive2 / sentimentTypeRatio2;
                } else if (sentimentType == 1) {
                    sentimentTypeRatio1 = (float)negative1 / sentimentTypeRatio1;
                    sentimentTypeRatio2 = (float)negative2 / sentimentTypeRatio2;
                }
                if (originAttrCount1.getString("key").equals("기타") ||
                        (!originAttrCount2.getString("key").equals("기타") &&
                                (sentimentTypeRatio1 < sentimentTypeRatio2 ||
                                        (sentimentTypeRatio1 == sentimentTypeRatio2 && (sentimentType == 0 && positive1 < positive2 ||sentimentType == 1 && negative1 < negative2))))) {
                    originAttrSentimentCounts.put(i, originAttrCount2);
                    originAttrSentimentCounts.put(j, originAttrCount1);
                }
            }
        }
        String[] originAttrKeys = new String[originAttrSentimentCounts.length()];
        for (int i = 0; i < originAttrSentimentCounts.length(); i++) {
            String originAttr = originAttrSentimentCounts.getJSONObject(i).getString("key");
            if (originAttr.equals("자체 성능")) originAttr = "휴대폰 자체 성능";
            originAttrKeys[i] = originAttr;
        }
        return originAttrKeys;
    }

    private void saveProductPosNegSentences(int productId, JSONArray originAttrSentimentCounts) throws Exception {
        ProsConsTable prosConsTable = new ProsConsTable();
        prosConsTable.connect();
        if (productId == 0) return;

        int countOfProsProductId = prosConsTable.getProductIdCount(true, "product_id = " + productId);
        String[] positiveOriginAttrs = getSortedOriginAttrForSentimentType(originAttrSentimentCounts, 0);
        JSONArray positiveSentences = prosConsForProduct.getSentenceJsonArray(productId, 0, positiveOriginAttrs);
        if (countOfProsProductId > 0) {
            prosConsTable.updateProsConsSentences(true, productId, positiveSentences);
        } else {
            prosConsTable.insertProsConsSentences(true, productId, positiveSentences);
        }

        int countOfConsProductId = prosConsTable.getProductIdCount(false, "product_id = " + productId);
        String[] negativeOriginAttrs = getSortedOriginAttrForSentimentType(originAttrSentimentCounts, 1);
        JSONArray negativeSentences = prosConsForProduct.getSentenceJsonArray(productId, 1, negativeOriginAttrs);
        if (countOfConsProductId > 0) {
            prosConsTable.updateProsConsSentences(false, productId, negativeSentences);
        } else {
            prosConsTable.insertProsConsSentences(false, productId, negativeSentences);
        }
        prosConsTable.close();
    }

    @SuppressWarnings("unchecked")
    private void saveProductAllJson() throws JSONException, IOException {
        HashMap<Integer, HashMap> allAllHashMap = highLightResultIndex.getAllAllHashMap();
        Set<Integer> productIds = allAllHashMap.keySet();
        File writeDirectory = getWriteDirectory();
        JsonArrayStringBuilder mainReviewOutputStringBuilder = new JsonArrayStringBuilder();
        String[] keysForBriefOutput = new String[]{"review_id", "count", "url", "title", "creation_time", "images", "sentences_without_highlight"};
        String[] keysForSentimentOutput = new String[]{"review_id", "count", "url", "title", "creation_time", "images", "sentences_with_sentiment"};
        for (Integer productId : productIds) {
            if (productId == 0) continue;
            File productDirectory = FileIO.getDirectory(writeDirectory.getPath() + "/product/" + productId);
            HashMap<Integer, HashMap> productHashMap = allAllHashMap.get(productId);

            String briefProductOutput = getReviewOutputJsonArray(productHashMap, 3, keysForBriefOutput).getString("string");
            FileIO.writeFile(productDirectory.getPath() + "/brief.json", briefProductOutput);

            JSONObject productOutput = getReviewOutputJsonArray(productHashMap, keysForSentimentOutput);
            String productOutputString = productOutput.getString("string");
            JSONArray reviewIndices = productOutput.getJSONArray("indices");
            FileIO.writeFile(productDirectory.getPath() + "/all_all.json", productOutputString);
            fileIndices.put(productId + "/all_all.json", reviewIndices);

            JSONArray reviewIndicesForDate = getSortedReviewIndicesByDate((Integer[])productOutput.get("sortedReviewIds"), productHashMap, reviewIndices);
            fileIndices.put(productId + "/all_all.json/date", reviewIndicesForDate);

            getMainReviewOutputJsonArray(mainReviewOutputStringBuilder, productHashMap, productId);
        }
        FileIO.writeFile(writeDirectory.getPath() + "/main_reviews.json", mainReviewOutputStringBuilder.toString());
    }

    @SuppressWarnings("unchecked")
    private void saveProductAllSentimentalJson() throws JSONException, IOException {
        HashMap<Integer, HashMap> allSentimentalHashMap = highLightResultIndex.getAllSentimentalHashMap();
        Set<Integer> productIds = allSentimentalHashMap.keySet();
        File writeDirectory = getWriteDirectory();
        String[] keysForOutput = new String[]{"review_id", "count", "url", "title", "creation_time", "images", "sentences"};
        for (Integer productId : productIds) {
            if (productId == 0) continue;
            File productDirectory = FileIO.getDirectory(writeDirectory.getPath() + "/product/" + productId);
            HashMap<Integer, HashMap> productHashMap = allSentimentalHashMap.get(productId);
            Set<Integer> sentimentTypes = productHashMap.keySet();
            JsonObjectStringBuilder productOutputStringBuilder = new JsonObjectStringBuilder();
            int startingFileIndex = "{".getBytes("UTF-8").length;
            for (Integer sentimentType : sentimentTypes) {
                if (sentimentType == 2) continue;
                startingFileIndex += ("\"" + sentimentType + "\":").getBytes("UTF-8").length;
                HashMap<Integer, HashMap> sentimentTypeHashMap = productHashMap.get(sentimentType);
                JSONObject sentimentTypeOutput = getReviewOutputJsonArray(sentimentTypeHashMap, keysForOutput);
                productOutputStringBuilder.putJson(sentimentType.toString(), sentimentTypeOutput.getString("string"));
                JSONArray fileIndicesForSentimentType = sentimentTypeOutput.getJSONArray("indices");
                for (int i = 0; i < fileIndicesForSentimentType.length(); i++) {
                    JSONArray fileIndexForSentimentType = fileIndicesForSentimentType.getJSONArray(i);
                    fileIndexForSentimentType.put(0, fileIndexForSentimentType.getInt(0) + startingFileIndex);
                    fileIndexForSentimentType.put(1, fileIndexForSentimentType.getInt(1) + startingFileIndex);
                }
                fileIndices.put(productId + "/all_sentimental.json/" + sentimentType, fileIndicesForSentimentType);

                JSONArray fileIndicesForSentimentTypeForDate = getSortedReviewIndicesByDate((Integer[])sentimentTypeOutput.get("sortedReviewIds"), sentimentTypeHashMap, fileIndicesForSentimentType);
                fileIndices.put(productId + "/all_sentimental.json/" + sentimentType + "/date", fileIndicesForSentimentTypeForDate);

                startingFileIndex += sentimentTypeOutput.getString("string").getBytes("UTF-8").length + ",".getBytes("UTF-8").length;
            }
            FileIO.writeFile(productDirectory.getPath() + "/all_sentimental.json", productOutputStringBuilder.toString());
        }
    }

    @SuppressWarnings("unchecked")
    private void saveProductFilterJson() throws JSONException, IOException {
        HashMap<Integer, HashMap> filterAllHashMap = highLightResultIndex.getFilterAllHashMap();
        Set<Integer> productIds = filterAllHashMap.keySet();
        File writeDirectory = getWriteDirectory();
        String[] keysForOutput = new String[]{"review_id", "count", "url", "title", "creation_time", "images", "sentences_with_sentiment"};
        for (Integer productId : productIds) {
            if (productId == 0) continue;
            File productDirectory = FileIO.getDirectory(writeDirectory.getPath() + "/product/" + productId);
            HashMap<String, HashMap> productHashMap = filterAllHashMap.get(productId);
            Set<String> originAttrs = productHashMap.keySet();
            for (String originAttr : originAttrs) {
                File originAttrDirectory = FileIO.getDirectory(productDirectory.getPath() + "/attr");
                HashMap<Integer, HashMap> originAttrHashMap = productHashMap.get(originAttr);

                int originAttrIndex = originAttrStrings.indexOf(originAttr);

                JSONObject originAttrOutput = getReviewOutputJsonArray(originAttrHashMap, -1, keysForOutput, originAttr, -1);
                String originAttrOutputString = originAttrOutput.getString("string");
                JSONArray originAttrOutputIndices = originAttrOutput.getJSONArray("indices");
                FileIO.writeFile(originAttrDirectory.getPath() + "/" + originAttrIndex + "_all.json", originAttrOutputString);

                fileIndices.put(productId + "/attr/" + originAttrIndex + "_all.json", originAttrOutputIndices);

                JSONArray originAttrOutputIndicesForDate = getSortedReviewIndicesByDate((Integer[])originAttrOutput.get("sortedReviewIds"), originAttrHashMap, originAttrOutputIndices);
                fileIndices.put(productId + "/attr/" + originAttrIndex + "_all.json/date", originAttrOutputIndicesForDate);
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void saveProductFilterSentimentalJson() throws Exception {
        HashMap<Integer, HashMap> filterSentimentalHashMap = highLightResultIndex.getFilterSentimentalHashMap();
        Set<Integer> productIds = filterSentimentalHashMap.keySet();
        File writeDirectory = getWriteDirectory();
        String[] keysForOutput = new String[]{"review_id", "count", "url", "title", "creation_time", "images", "sentences"};
        for (Integer productId : productIds) {
            if (productId == 0) continue;
            File productDirectory = FileIO.getDirectory(writeDirectory.getPath() + "/product/" + productId);
            HashMap<String, HashMap> productHashMap = filterSentimentalHashMap.get(productId);
            Set<String> originAttrs = productHashMap.keySet();
            JSONArray originAttrSentimentCounts = new JSONArray();
            for (String originAttr : originAttrs) {
                JSONObject originAttrSentimentCount = new JSONObject();
                originAttrSentimentCount.put("value", originAttrStrings.indexOf(originAttr));
                if (originAttr.equals("휴대폰 자체 성능")) {
                    originAttrSentimentCount.put("key", "자체 성능");
                } else {
                    originAttrSentimentCount.put("key", originAttr);
                }
                HashMap<Integer, HashMap> originAttrHashMap = productHashMap.get(originAttr);
                Set<Integer> sentimentTypes = originAttrHashMap.keySet();
                JsonObjectStringBuilder originAttrOutputStringBuilder = new JsonObjectStringBuilder();
                File originAttrDirectory = FileIO.getDirectory(productDirectory.getPath() + "/attr");
                int totalCountOfSentences = 0;
                int startingFileIndex = "{".getBytes("UTF-8").length;
                Integer originAttrIndex = originAttrStrings.indexOf(originAttr);
                for (Integer sentimentType : sentimentTypes) {
                    startingFileIndex += ("\"" + sentimentType + "\":").getBytes("UTF-8").length;
                    String sentimentString = null;
                    if (sentimentType == 0) {
                        sentimentString = "positive";
                    } else if (sentimentType == 1) {
                        sentimentString = "negative";
                    } else if (sentimentType == 2) {
                        sentimentString = "neutral";
                    }
                    HashMap<Integer, HashMap> sentimentTypeHashMap = originAttrHashMap.get(sentimentType);
                    JSONObject sentimentTypeOutput = getReviewOutputJsonArray(sentimentTypeHashMap, -1, keysForOutput, originAttr, sentimentType);
                    originAttrOutputStringBuilder.putJson(sentimentType.toString(), sentimentTypeOutput.getString("string"));
                    JSONArray fileIndicesForSentimentType = sentimentTypeOutput.getJSONArray("indices");
                    for (int i = 0; i < fileIndicesForSentimentType.length(); i++) {
                        JSONArray fileIndexForSentimentType = fileIndicesForSentimentType.getJSONArray(i);
                        fileIndexForSentimentType.put(0, fileIndexForSentimentType.getInt(0) + startingFileIndex);
                        fileIndexForSentimentType.put(1, fileIndexForSentimentType.getInt(1) + startingFileIndex);
                    }
                    fileIndices.put(productId + "/attr/" + originAttrIndex + "_sentimental.json/" + sentimentType, fileIndicesForSentimentType);

                    JSONArray fileIndicesForSentimentTypeForDate = getSortedReviewIndicesByDate((Integer[])sentimentTypeOutput.get("sortedReviewIds"), sentimentTypeHashMap, fileIndicesForSentimentType);
                    fileIndices.put(productId + "/attr/" + originAttrIndex+ "_sentimental.json/" + sentimentType + "/date", fileIndicesForSentimentTypeForDate);

                    startingFileIndex += sentimentTypeOutput.getString("string").getBytes("UTF-8").length + ",".getBytes("UTF-8").length;
                    int numberOfSentences = sentimentTypeOutput.getInt("numberOfSentences");

                    originAttrSentimentCount.put(sentimentString, numberOfSentences);
                    totalCountOfSentences += numberOfSentences;
                }
                if (!originAttrSentimentCount.has("positive")) {
                    originAttrSentimentCount.put("positive", 0);
                }
                if (!originAttrSentimentCount.has("negative")) {
                    originAttrSentimentCount.put("negative", 0);
                }
                if (!originAttrSentimentCount.has("neutral")) {
                    originAttrSentimentCount.put("neutral", 0);
                }
                originAttrSentimentCount.put("total", totalCountOfSentences);
                originAttrSentimentCounts.put(originAttrSentimentCount);


                FileIO.writeFile(originAttrDirectory.getPath() + "/" + originAttrIndex + "_sentimental.json", originAttrOutputStringBuilder.toString());
            }

            for (int i = 0; i < originAttrSentimentCounts.length() - 1; i++) {
                for (int j = i + 1; j < originAttrSentimentCounts.length(); j++) {
                    JSONObject originAttrCount1 = originAttrSentimentCounts.getJSONObject(i);
                    JSONObject originAttrCount2 = originAttrSentimentCounts.getJSONObject(j);
                    int positive1 = originAttrCount1.getInt("positive");
                    int negative1 = originAttrCount1.getInt("negative");
                    int positive2 = originAttrCount2.getInt("positive");
                    int negative2 = originAttrCount2.getInt("negative");
                    int count1 = positive1 + negative1;
                    int count2 = positive2 + negative2;
                    if (originAttrCount1.getString("key").equals("기타") ||
                            (!originAttrCount2.getString("key").equals("기타") && count1 < count2 || (count1 == count2 && negative1 < negative2))) {
                        originAttrSentimentCounts.put(i, originAttrCount2);
                        originAttrSentimentCounts.put(j, originAttrCount1);
                    }
                }
            }

            FileIO.writeFile(productDirectory.getPath() + "/origin_attr_count.json", originAttrSentimentCounts.toString());
            saveProductPosNegSentences(productId, originAttrSentimentCounts);
        }
    }

    private Integer[] getSortReviewIdsByNumberOfSentences(HashMap<Integer, HashMap> hashMap) throws JSONException {
        Integer[] reviewIds = hashMap.keySet().toArray(new Integer[hashMap.size()]);
        int[] NUMBERS_OF_SENTENCES = new int[reviewIds.length];
        for (int i = 0; i < reviewIds.length; i++) {
            NUMBERS_OF_SENTENCES[i] = hashMap.get(reviewIds[i]).size();
        }

        HashMap<Integer, Integer> sentencesForReviewId = new HashMap<Integer, Integer>();
        for (int i = 0; i < reviewIds.length; i++) {
            sentencesForReviewId.put(reviewIds[i], NUMBERS_OF_SENTENCES[i]);
        }
        for (int i = 0; i < reviewIds.length - 1; i++) {
            for (int j = i + 1; j < reviewIds.length; j++) {
                int NUMBER_OF_SENTENCE1 = sentencesForReviewId.get(reviewIds[i]);
                int NUMBER_OF_SENTENCE2 = sentencesForReviewId.get(reviewIds[j]);
                if (NUMBER_OF_SENTENCE1 < NUMBER_OF_SENTENCE2) {
                    Integer tmpInteger = reviewIds[i];
                    reviewIds[i] = reviewIds[j];
                    reviewIds[j] = tmpInteger;
                }
            }
        }
        return reviewIds;
    }

    private Integer[] getSortReviewIdsByDate(HashMap<Integer, HashMap> hashMap) throws JSONException {
        Integer[] reviewIds = hashMap.keySet().toArray(new Integer[hashMap.size()]);
        Date[] dates = new Date[reviewIds.length];
        Date minDate = new Date();
        for (int i = 0; i < reviewIds.length; i++) {
            for (CrawledFile crawledFile : crawledFiles) {
                if (reviewIds[i] == crawledFile.getCrawling().getUid().intValue()) {
                    dates[i] = crawledFile.getDate();
                    if (dates[i] != null && minDate.compareTo(dates[i]) > 0) {
                        minDate = dates[i];
                    }
                    break;
                }
            }
        }
        for (int i = 0; i < dates.length; i++) {
            if (dates[i] == null) dates[i] = minDate;
        }

        HashMap<Integer, Date> sentencesForReviewId = new HashMap<Integer, Date>();
        for (int i = 0; i < reviewIds.length; i++) {
            sentencesForReviewId.put(reviewIds[i], dates[i]);
        }
        for (int i = 0; i < reviewIds.length - 1; i++) {
            for (int j = i + 1; j < reviewIds.length; j++) {
                Date date1 = sentencesForReviewId.get(reviewIds[i]);
                Date date2 = sentencesForReviewId.get(reviewIds[j]);
                if (date1.compareTo(date2) < 0) {
                    Integer tmpInteger = reviewIds[i];
                    reviewIds[i] = reviewIds[j];
                    reviewIds[j] = tmpInteger;
                }
            }
        }
        return reviewIds;
    }

    private JSONArray getSortedReviewIndicesByDate(Integer[] sortedReviewIds, HashMap<Integer, HashMap> hashMap, JSONArray reviewIndices) throws JSONException {
        Integer[] sortedReviewIdsByDate = getSortReviewIdsByDate(hashMap);
        JSONArray reviewIndicesForDate = new JSONArray();
        for (int i = 0; i < sortedReviewIdsByDate.length; i++) {
            for (int j = 0; j < sortedReviewIds.length; j++) {
                if (sortedReviewIdsByDate[i].intValue() == sortedReviewIds[j]) {
                    reviewIndicesForDate.put(reviewIndices.getJSONArray(j));
                }
            }
        }
        return reviewIndicesForDate;
    }

    public static void main(String[] args) throws Exception {
        // Todo override analyze, override read files.
        boolean skipReadFile = (args.length > 0 && args[0].equals("true"));
        boolean skipAnalyze = (args.length > 1 && args[1].equals("true"));

        int[] convertTypes = { SentenceConverter.ONLY_PLAIN_SENTENCE, SentenceConverter.ONLY_PLAIN_EOJEOL };

        HighLightingInfraPlant highLightingInfraPlant = new HighLightingInfraPlant(Constants.CRAWLER_JSON_DIRECTORY_PATH,
                Constants.REVIEW_SUMMARY_DIRECTORY_PATH, WorkflowFactory.WORKFLOW_POS_SIMPLE_22, convertTypes, skipReadFile, skipAnalyze);
        highLightingInfraPlant.setSubmitedReviewFileNames();
        highLightingInfraPlant.setPredefinedDataSet();
        highLightingInfraPlant.analyzeFiles(true);
        highLightingInfraPlant.close(true);
        highLightingInfraPlant.saveToFile();
    }
}
