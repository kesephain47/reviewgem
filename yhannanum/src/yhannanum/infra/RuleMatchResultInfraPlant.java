package yhannanum.infra;

import kr.ac.kaist.swrc.jhannanum.comm.Sentence;
import kr.ac.kaist.swrc.jhannanum.hannanum.WorkflowFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import yhannanum.comm.*;
import yhannanum.converter.SentenceConverter;
import yhannanum.db.CrawlingTable;
import yhannanum.db.RuleTable;
import yhannanum.db.SubmitTable;
import yhannanum.share.ArrayUtil;
import yhannanum.share.Constants;
import yhannanum.share.FileIO;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * Created by Arthur on 2014. 5. 5..
 */
public class RuleMatchResultInfraPlant extends InfraPlant {
    public RuleMatchResultInfraPlant(String readDirectoryName, String writeDirectoryName, int workflowFlag, int[] convertTypes) throws Exception {
        super(readDirectoryName, writeDirectoryName, workflowFlag, convertTypes);
    }

    @Override
    public boolean skipReadFile(int fileIndex) { return false; }

    @Override
    public boolean skipAnalyzing(int fileIndex) {
        return false;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T processConvertedSetence(int fileIndex, Sentence sentence, JSONObject convertedSentence) throws Exception {
        List<Integer> attrRuleIds = RuleArgument.getIncludedArgsRuleIds(sentence, attrSet);
        List<Integer> sentiRuleIds = RuleArgument.getIncludedArgsRuleIds(sentence, sentimentSet);

        Set<Integer> mergedSet = ArrayUtil.getMergedSet(attrRuleIds, sentiRuleIds);

        RsfWorkerResult rsfWorkerResult = new RsfWorkerResult(sentence.getDocumentID(), sentence.getSentenceID(), sentence);

        for (Integer ruleId : mergedSet) {
            rsfWorkerResult.checkAndAddRuleResult(sentence, ruleId, ruleArgRelations[ruleId]);
        }

        if (rsfWorkerResult.getRuleIds() != null && rsfWorkerResult.getRuleIds().size() > 0) {
            JSONObject sentenceObject = new JSONObject();
            sentenceObject.put("sentence_id", sentence.getSentenceID());
            sentenceObject.put("sentence", convertedSentence.getString("sentence"));
            sentenceObject.put("morphemes", convertedSentence.getString("morphemes"));
            sentenceObject.put("eojeols", convertedSentence.getString("eojeol"));
            sentenceObject.put("tags", convertedSentence.getString("tags"));

            JSONArray matchedRules = new JSONArray();
            String reviewId = submits[fileIndex].getCrawling_uid().toString();
            String sentenceId = String.valueOf(sentence.getSentenceID());
            for (int i = 0; i < rsfWorkerResult.getRuleIds().size(); i++) {
                JSONObject ruleMatchingResult = null;
                if (ruleMatchingResults.has(reviewId) && ruleMatchingResults.getJSONObject(reviewId).has(sentenceId)) {
                    ruleMatchingResult = ruleMatchingResults.getJSONObject(reviewId).getJSONObject(sentenceId);
                }
                JSONObject matchedRuleJson = rsfWorkerResult.getMatchedRuleJson(i, ruleArgRelations, rules, ruleMatchingResult);
                String matchingResult = matchedRuleJson.getString("compare_with_result");
                if (matchingResult.equals("correct")) CORRECT++;
                else if (matchingResult.equals("wrong")) WRONG++;
                else if (matchingResult.equals("not a rule")) NOT_A_RULE++;
                else if (matchingResult.equals("no result")) NO_RESULT++;
                else if (matchingResult.equals("not this attr")) NOT_THIS_ATTR++;
                matchedRules.put(matchedRuleJson);
            }
            sentenceObject.put("rules", matchedRules);
            return (T)sentenceObject;
        } else {
            return null;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T processConvertedSentences(int fileIndex, JSONObject originalJsonFile, JSONArray convertedSentences) throws Exception {
        if (convertedSentences.length() == 0) return null;
        JSONObject resultObject = new JSONObject();
        JSONObject reviewObject = new JSONObject();
        reviewObject.put("review_id", submits[fileIndex].getCrawling_uid());
        reviewObject.put("file_path", getToReadFilePath(fileIndex));
        reviewObject.put("url", originalJsonFile.getString("url"));
        reviewObject.put("title", originalJsonFile.getString("title"));
        reviewObject.put("content", originalJsonFile.getString("content"));
        resultObject.put("review", reviewObject);
        resultObject.put("sentences", convertedSentences);
        return (T)resultObject;
    }
    @Override
    public boolean writeToFile() {
        return true;
    }

    @Override
    public String setFilePathToWrite(int fileIndex, String fileName) throws IOException {
        return String.valueOf(submits[fileIndex].getCrawling_uid()) + ".json";
    }

    @Override
    public void close(boolean showProgress) {
        super.close(showProgress);
    }

    private Rule[] rules = null;

    private RuleArgument[] targetSet = null;

    private RuleArgument[] attrSet = null;

    private RuleArgument[] sentimentSet = null;

    private RuleArgRelation[] ruleArgRelations = null;

    private Submit[] submits = null;

    private JSONObject ruleMatchingResults = null;

    private int CORRECT = 0;

    private int WRONG = 0;

    private int NOT_A_RULE = 0;

    private int NOT_THIS_ATTR = 0;

    private int NO_RESULT = 0;

    @SuppressWarnings("unchecked")
    public void setPredefinedDataSet() throws Exception {
        RuleTable ruleTable = new RuleTable();
        ruleTable.connect();
        rules = ruleTable.readRules("sentiment is not null");
        ruleMatchingResults = ruleTable.readRuleMatchingResult();
        ruleTable.close();

        Class rClass = Class.forName("yhannanum.comm.Rule");
        Method getTargets = rClass.getMethod("getTargets");
        Method getTargetIndices = rClass.getMethod("getTargetIndices");
        Method getTargetTags = rClass.getMethod("getTargetTags");
        Method getAttrs = rClass.getMethod("getAttrs");
        Method getAttrIndices = rClass.getMethod("getAttrIndices");
        Method getAttrTags = rClass.getMethod("getAttrTags");
        Method getSentiments = rClass.getMethod("getSentiments");
        Method getSentimentIndices = rClass.getMethod("getSentimentIndices");
        Method getSentimentTags = rClass.getMethod("getSentimentTags");
        targetSet = RuleArgument.getRuleArguSet(rules, getTargets, getTargetIndices, getTargetTags, true);
        attrSet = RuleArgument.getRuleArguSet(rules, getAttrs, getAttrIndices, getAttrTags, true);
        sentimentSet = RuleArgument.getRuleArguSet(rules, getSentiments, getSentimentIndices, getSentimentTags, true,
                new String[]{"NC", "NB", "NN", "NP", "NQ", "PV", "PA", "PX", "F", "MA", "MM", "SE", "SP", "SU"});

        ruleArgRelations = RuleArgRelation.getRuleRelations(rules,
                new String[]{"NC", "NB", "NN", "NP", "NQ", "PV", "PA", "PX", "F", "MA", "MM", "SE", "SP", "SU"});
    }

    public String[] getSubmitedReviewFileNames() throws Exception {
        SubmitTable submitTable = new SubmitTable();
        submitTable.connect();
        submits = submitTable.readSubmitTable();
        ArrayList<Integer> submitReviewIds = new ArrayList<Integer>();
        for (Submit submit : submits) {
            submitReviewIds.add(submit.getUid());
        }
        String submitReviewIdString = Arrays.deepToString(submitReviewIds.toArray());
        submitReviewIdString = submitReviewIdString.substring(1, submitReviewIdString.length() - 1);
        CrawlingTable crawlingTable = new CrawlingTable();
        crawlingTable.setConnect(submitTable.getConnect());
        Crawling[] crawlings = crawlingTable.readCrawlingTable("uid in (" + submitReviewIdString + ")");
        String[] names = new String[crawlings.length];
        for (int i = 0; i < crawlings.length; i++) {
            names[i] = FileIO.extractNameFromPath(crawlings[i].getPath());
        }
        crawlingTable.close();
        submitTable.close();

        return names;
    }

    public void printResults() {
        System.out.println("Correct : " + CORRECT + " Wrong : " + WRONG + " Wrong Attr : " + NOT_THIS_ATTR + " NOT A RULE : " + NOT_A_RULE + " NO RESULT : " + NO_RESULT);
    }

    public static void main(String[] args) throws Exception {
        boolean override = (args.length > 0 && args[0].equals("true"));

        int[] convertTypes = { SentenceConverter.ONLY_PLAIN_SENTENCE, SentenceConverter.ONLY_MORPHEME,
                SentenceConverter.ONLY_PLAIN_EOJEOL, SentenceConverter.ONLY_TAG };

        RuleMatchResultInfraPlant ruleMatchResultInfraPlant = new RuleMatchResultInfraPlant(Constants.CRAWLER_JSON_DIRECTORY_PATH, Constants.RULE_MATCH_CHECKER_RESULT_DIRECTORY_PATH, WorkflowFactory.WORKFLOW_POS_SIMPLE_22, convertTypes);
        String[] names = ruleMatchResultInfraPlant.getSubmitedReviewFileNames();
//        reviewSemanticFactory.setToReadFileNames(true);
//        reviewSemanticFactory.setToReadFileNames("tistory_thebetterday_3914.json");
        ruleMatchResultInfraPlant.setToReadFileNames(names);
        ruleMatchResultInfraPlant.setPredefinedDataSet();
        ruleMatchResultInfraPlant.analyzeFiles(true);
        ruleMatchResultInfraPlant.close(true);
        ruleMatchResultInfraPlant.printResults();
    }
}
