package yhannanum.infra;

import kr.ac.kaist.swrc.jhannanum.comm.Sentence;
import kr.ac.kaist.swrc.jhannanum.hannanum.WorkflowFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import yhannanum.converter.SentenceConverter;
import yhannanum.share.Constants;

import java.io.IOException;

/**
 * Created by Arthur on 2014. 4. 23..
 */
public class ErrorCheckerInfraPlant extends InfraPlant {
    public ErrorCheckerInfraPlant(String readDirectoryName, String writeDirectoryName, int workflowFlag, int[] convertTypes, boolean override) throws Exception {
        super(readDirectoryName, writeDirectoryName, workflowFlag, convertTypes, override);
    }

    @Override
    public boolean skipReadFile(int fileIndex) { return false; }

    @Override
    public boolean skipAnalyzing(int fileIndex) {
        return false;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T processConvertedSetence(int fileIndex, Sentence sentence, JSONObject convertedSentence) throws Exception {
        return (T)convertedSentence;
    }

    /**
     * Add only content and analyzing morpheme results.
     * @param originalJsonFile - original json file, which contains contents exactly same with read file
     * @param convertedSentences - converted sentences array
     * @param <T> - type of return.
     * @return - return JSONObject
     * @throws JSONException
     */
    @Override
    @SuppressWarnings("unchecked")
    public <T> T processConvertedSentences(int fileIndex, JSONObject originalJsonFile, JSONArray convertedSentences) throws Exception {
        JSONObject simpleJsonFile = new JSONObject();
        simpleJsonFile.put("content", originalJsonFile.getString("content"));
        simpleJsonFile.put("morphemeAnalyze", convertedSentences);
        return (T)simpleJsonFile;
    }
    @Override
    public boolean writeToFile() {
        return true;
    }

    @Override
    public String setFilePathToWrite(int fileIndex, String fileName) throws IOException {
        return fileName;
    }

    /**
     * add only sentence, morpheme, eojeols
     * @param args whether override or not.
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        boolean override = (args.length > 0 && args[0].equals("true"));
        int[] convertTypes = {SentenceConverter.ONLY_PLAIN_SENTENCE, SentenceConverter.ONLY_MORPHEME, SentenceConverter.ONLY_PLAIN_EOJEOL};
        ErrorCheckerInfraPlant errorCheckerInfraPlant = new ErrorCheckerInfraPlant(Constants.CRAWLER_JSON_DIRECTORY_PATH, Constants.RULE_ERROR_CHECK_SOURCE_DIRECTORY_PATH,
                WorkflowFactory.WORKFLOW_POS_SIMPLE_22, convertTypes, override);
        errorCheckerInfraPlant.analyzeFiles(true);
        errorCheckerInfraPlant.close(true);
    }
}