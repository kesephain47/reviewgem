package yhannanum.infra;

import kr.ac.kaist.swrc.jhannanum.comm.Sentence;
import kr.ac.kaist.swrc.jhannanum.hannanum.Workflow;
import kr.ac.kaist.swrc.jhannanum.hannanum.WorkflowFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import yhannanum.converter.SentenceConverter;
import yhannanum.share.ArrayUtil;
import yhannanum.share.FileIO;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Arthur on 2014. 4. 23..
 *
 * Plant - the assets of a business including land, buildings, machinery and all equipment permanently employed.
 * Factory - building in which products are manufactured; any location which mass-produces one type of product (can be used figuratively)
 */

public abstract class InfraPlant{

    /**
     * analyzing target directory
     */
    private File readDirectory = null;

    /**
     * directory where the result files will be saved in.
     */
    private File writeDirectory = null;

    /**
     * file name strings array.
     * file name is set by User or readDirectory and writeDirectory
     *   1. User can set read file list
     *   2. Program can read only files which are inside of readDirectory
     *   3. Program can read only files which are inside of readDirectory but writeDirectory
     */
    private String[] toReadFileNames = null;

    /**
     * workflow object.
     */
    private Workflow workflow = null;

    /**
     * workflow flag.
     * default flag is WORKFLOW_POS_SIMPLE_22
     */
    private int workflowFlag = WorkflowFactory.WORKFLOW_POS_SIMPLE_22;

    /**
     * sentenceConverter object
     */
    private SentenceConverter sentenceConverter = null;

    /**
     * convert Types.
     * types are defined in SentenceConverter Class
     */
    private int[] convertTypes = null;

    /**
     * Constructor with read directory name and write directory name
     * @param readDirectoryName - analyzing target directory
     * @param writeDirectoryName - directory where the result files will be saved in.
     */
    public InfraPlant(String readDirectoryName, String writeDirectoryName) throws IOException{
        readDirectory = FileIO.getDirectory(readDirectoryName);
        writeDirectory = FileIO.getDirectory(writeDirectoryName);
    }

    /**
     * Constructor with read directory name and write directory name and workflow flag
     * @param readDirectoryName - analyzing target directory
     * @param writeDirectoryName - directory where the result files will be saved in.
     * @param workflowFlag - workflow flag.
     * @throws Exception
     */
    public InfraPlant(String readDirectoryName, String writeDirectoryName, int workflowFlag) throws Exception {
        this(readDirectoryName, writeDirectoryName);
        setWorkflow(workflowFlag);
    }

    /**
     * Constructor with read directory name and write directory name and workflow flag and convertTypes
     * @param readDirectoryName - analyzing target directory
     * @param writeDirectoryName - directory where the result files will be saved in.
     * @param workflowFlag - workflow flag.
     * @param convertTypes - convert types. types are defined in SentenceConverter Class
     * @throws Exception
     */
    public InfraPlant(String readDirectoryName, String writeDirectoryName, int workflowFlag, int[] convertTypes) throws Exception {
        this(readDirectoryName, writeDirectoryName, workflowFlag);
        setConvertTypes(convertTypes);
    }

    /**
     * Constructor with read directory name and write directory name and workflow flag and convertTypes
     * @param readDirectoryName - analyzing target directory
     * @param writeDirectoryName - directory where the result files will be saved in.
     * @param workflowFlag - workflow flag.
     * @param convertTypes - convert types. types are defined in SentenceConverter Class
     * @param override - whether override files into write directory or not.
     * @throws Exception
     */
    public InfraPlant(String readDirectoryName, String writeDirectoryName, int workflowFlag, int[] convertTypes, boolean override) throws Exception {

        this(readDirectoryName, writeDirectoryName, workflowFlag, convertTypes);
        setToReadFileNames(override);
    }

    /**
     * set file name list to read.
     * if wants to override, read all of files in read directory.
     * if not, read files except files which exist inside of write directory.
     * @param override - - whether override files into write directory or not.
     */
    public void setToReadFileNames(boolean override) {
        if (override) {
            toReadFileNames = FileIO.getToReadFileNames(readDirectory);
        } else {
            toReadFileNames = FileIO.getToReadFileNames(readDirectory, writeDirectory);
        }
    }

    /**
     * User can set read file list directly
     * @param fileName - file name which will be read.
     */
    public void setToReadFileNames(String fileName) {
        toReadFileNames = new String[]{fileName};
    }

    /**
     * User can set read files list directly
     * @param fileNames - file names which will be read.
     */
    public void setToReadFileNames(String[] fileNames, boolean override) {
        if (override) {
            toReadFileNames = fileNames;
        } else {
            List<String> fileNameList = ArrayUtil.substractList(fileNames, FileIO.getToReadFileNames(writeDirectory));
            toReadFileNames = fileNameList.toArray(new String[fileNameList.size()]);
        }
    }

    public void setToReadFileNames(String[] fileNames) {
        setToReadFileNames(fileNames, true);
    }

    public String[] getToReadFileNames() { return toReadFileNames; }

    public String getToReadFileName(int index) { return toReadFileNames[index]; }

    public String getToReadFilePath(int index) { return readDirectory + "/" + toReadFileNames[index]; }

    public File getReadDirectory() { return readDirectory; }

    public File getWriteDirectory() { return writeDirectory; }

    /**
     * set workflow along with workflow flag
     * @param workflowFlag - - workflow flag.
     * @throws Exception
     */
    public void setWorkflow(int workflowFlag) throws Exception{
        this.workflowFlag = workflowFlag;
        if (workflow != null) workflow.close();
        workflow = WorkflowFactory.getPredefinedWorkflow(workflowFlag);
        workflow.activateWorkflow(true);
        if (sentenceConverter == null) sentenceConverter = new SentenceConverter();
    }

    /**
     * set convert types. types are defined in SentenceConverter Class
     * @param convertTypes - - convert types. types are defined in SentenceConverter Class
     */
    public void setConvertTypes(int[] convertTypes) {
        this.convertTypes = convertTypes;
    }

    /**
     * read files with toReadFileNames string array.
     *
     * @param showProgress - whether print progress or not.
     * @throws Exception
     */
    public void analyzeFiles(boolean showProgress) throws Exception {
        if (toReadFileNames == null) return;

        if (showProgress) System.out.println("number of files to read : " + toReadFileNames.length);

        for (int i = 0; i < toReadFileNames.length; i++) {
            if (skipReadFile(i)) continue;
            String fileName = toReadFileNames[i];

            if (showProgress) System.out.println((i + 1) + "/" + toReadFileNames.length + " " + fileName);

            String readFilePath = readDirectory.getPath() + "/" + fileName;

            if (FileIO.getExtension(readFilePath).equals("json")) {
                analyze(i, readFilePath, fileName);
            }
        }
    }

    /**
     * actual analyzing for the file.
     * each file sepereted into sentence.
     * after analyze sentence, convert it into json format.
     * with abstract methods, child class can add aditional process
     *
     * @param filePath - file path for analyzing file
     * @param fileName - file name of analyzing file.
     * @throws Exception
     */
    private void analyze(int fileIndex, String filePath, String fileName) throws Exception {
        String jsonFile = FileIO.readFile(filePath);
        if (jsonFile == null) return;
        JSONTokener tokener = new JSONTokener(jsonFile);
        JSONObject jsonObject = new JSONObject(tokener);
        String content = jsonObject.getString("content");

        if (!workflow.isAnalyzable(content)) return;

        workflow.analyze(content);

        JSONArray resultArray = new JSONArray();

        int sentenceId = 0;
        while (!skipAnalyzing(fileIndex)) {
            sentenceId++;
            Sentence s = workflow.getResultOfSentence(new Sentence(0, 0, false));
            s.setDocumentID(fileIndex);
            s.setSentenceID(sentenceId);
            JSONObject resultObject = convertedSentence(s, convertTypes);
            Object processedConvertedSentence = processConvertedSetence(fileIndex, s, resultObject);
            if (processedConvertedSentence != null) resultArray.put(processedConvertedSentence);
            if (s.isEndOfDocument()) {
                break;
            }
        }
        Object processedObject = processConvertedSentences(fileIndex, jsonObject, resultArray);
        if (writeToFile() && processedObject != null) {
            FileIO.writeFile(writeDirectory.getPath() + "/" + setFilePathToWrite(fileIndex, fileName), processedObject.toString());
        }
    }

    /**
     * convert sentence into desired format of json file
     * @param s - sentence to process
     * @param convertTypes - - convert types. types are defined in SentenceConverter Class
     * @return json file of analyzed sentence.
     * @throws Exception
     */
    private JSONObject convertedSentence(Sentence s, int[] convertTypes) throws Exception {
        JSONObject convertedSentence = null;
        for (int convertType : convertTypes) {
            convertedSentence = sentenceConverter.convertSentenceToJSON(convertedSentence, null, s, convertType);
        }
        return convertedSentence;
    }

    public abstract boolean skipReadFile(int fileIndex);

    public abstract boolean skipAnalyzing(int fileIndex);

    /**
     * abstract method which will be defined in child class.
     * with convertedSentence, child class can change each line of sentence
     * @param convertedSentence - converted sentence
     */
    public abstract <T> T processConvertedSetence(int fileIndex, Sentence sentence, JSONObject convertedSentence) throws Exception;

    /**
     * abstract method which will be defined in child class.
     * with orginalJsonFile and convertedSentences Array, child class can change all of sentences
     * @param originalJsonFile - original json file, which contains contents exactly same with read file
     * @param convertedSentences - converted sentences array
     * @param <T> - type of return. return type should has toString
     * @return T type of object
     * @throws JSONException
     */
    public abstract <T> T processConvertedSentences(int fileIndex, JSONObject originalJsonFile, JSONArray convertedSentences) throws Exception;

    /**
     * whether write content to file or not
     * @return boolean return value whether write or not
     */
    public abstract boolean writeToFile();

    public abstract String setFilePathToWrite(int fileIndex, String fileName) throws IOException;

    /**
     * after analyzing Should close workflow.
     *
     * @param showProgress - - whether print progress or not.
     */
    public void close(boolean showProgress) {
        workflow.close();
        if (showProgress) System.out.println("converting done");
    }
}
