package yhannanum.share;

/**
 * Created by Arthur on 2014. 6. 1..
 */
public class JsonArrayStringBuilder {
    private StringBuilder stringBuilder = new StringBuilder();

    public JsonArrayStringBuilder() {
        stringBuilder.append("[");
    }

    public JsonArrayStringBuilder(String originalJsonStringBuilder) {
        stringBuilder.append(originalJsonStringBuilder);
        stringBuilder.delete(stringBuilder.length() - 1, stringBuilder.length());
    }

    public void putValue(Object value) {
        if (stringBuilder.length() > 1) stringBuilder.append(",");
        stringBuilder.append("\"").append(value.toString().replace("\"", "\\\"").replace("\n", "\\n")).append("\"");
    }

    public void putJson(Object value) {
        if (stringBuilder.length() > 1) stringBuilder.append(",");
        stringBuilder.append(value.toString());
    }

    @Override
    public String toString() {
        return stringBuilder.toString() + "]";
    }
}
