package yhannanum.share;

import java.io.File;
import java.io.Reader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;


public class FileIO {
    public static File getDirectory(String directoryPath) throws IOException {
        File directory = new File(directoryPath);
        if (!directory.exists()) {
            directory.mkdir();
        }
        return directory;
    }

	public static String readFile( String filePath ) throws IOException {
        if (!(new File(filePath).exists())) return null;
        Reader reader = new FileReader( filePath );
        StringBuilder sb = new StringBuilder(); 
        char buffer[] = new char[16384];  // read 16k blocks
        int len; // how much content was read? 
        while( ( len = reader.read( buffer ) ) > 0 ){
            sb.append( buffer, 0, len ); 
        }
        reader.close();
        return sb.toString();
    }

    public static void writeFile( String filePath, String content, boolean append ) throws IOException {
        File newTextFile = new File(filePath);
        FileWriter fw = new FileWriter(newTextFile, append);
        fw.write(content);
        fw.close();
    }

    public static void writeFile( String filePath, String content ) throws IOException {
        writeFile(filePath, content, false);
    }

    public static String getExtension(String fileStr) {
        return fileStr.substring(fileStr.lastIndexOf(".")+1,fileStr.length());
    }
    
    public static String removeExtension(String fileStr) {
    	return fileStr.substring(0, fileStr.lastIndexOf("."));
    }

    public static String extractNameFromPath(String path) {
        return path.substring(path.lastIndexOf('/') + 1);
    }

    public static String[] getToReadFileNames(File readDirectory, File writeDirectory) {
        String[] readFileNames = readDirectory.list();
        String[] existFileNames = writeDirectory.list();

        List<String> toReadFileNames = ArrayUtil.substractList(readFileNames, existFileNames);

        return toReadFileNames.toArray(new String[0]);
    }

    public static String[] getToReadFileNames(File readDirectory) {
        return readDirectory.list();
    }
}