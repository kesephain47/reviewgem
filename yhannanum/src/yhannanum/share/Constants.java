package yhannanum.share;

public final class Constants {
	public static final String CRAWLER_JSON_DIRECTORY_PATH = "/var/www/crawler/jsons";
    public static final String NLP_ROOT_DIRECTORY_PATH = "/var/www/nlp";
    public static final String DEMO_STATIC_FILES_DIRECTORY_PATH = "/var/www/static";

    public static final String RULE_COVERAGE_DIRECTORY_PATH = DEMO_STATIC_FILES_DIRECTORY_PATH + "/coverage";
    public static final String RULE_ERROR_CHECK_SOURCE_DIRECTORY_PATH = DEMO_STATIC_FILES_DIRECTORY_PATH + "/error_check_source";
    public static final String RULE_ERROR_CHECK_DIRECTORY_PATH = DEMO_STATIC_FILES_DIRECTORY_PATH + "/error_check";
    public static final String RULE_DICTIONARY_COUNTER_DIRECTORY_PATH = DEMO_STATIC_FILES_DIRECTORY_PATH + "/dictionary_counter";
    public static final String RULE_MATCH_CHECKER_RESULT_DIRECTORY_PATH = DEMO_STATIC_FILES_DIRECTORY_PATH + "/match_check_result";
    public static final String RULE_TAG_MARKER_SOURCE_DIRECTORY = DEMO_STATIC_FILES_DIRECTORY_PATH + "/tag_marker_source";

    public static final String SERVICE_DIRECTORY_PATH = "/root/soma_2nd";
    public static final String SERVICE_STATIC_DIRECTORY_PATH = SERVICE_DIRECTORY_PATH + "/static";

    public static final String REVIEW_SUMMARY_DIRECTORY_PATH = SERVICE_STATIC_DIRECTORY_PATH + "/review_summary";
    public static final String REVIEW_INDEX_DIRECTORY_PATH = SERVICE_STATIC_DIRECTORY_PATH + "/review_index";
}
