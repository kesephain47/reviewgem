package yhannanum.share;

import java.util.*;

public class ArrayUtil {
	public static <T> List<T> substractList(T[] list1, T[] list2) {
		List<T> newList = new ArrayList<T>(Arrays.asList(list1));

		for (T list2Element : list2) {
			if (newList.contains(list2Element)) {
				newList.remove(list2Element);
			}
		}

		return newList;
	}

    public static <T> T[] concat(T[] first, T[] second) {
        if (first == null || first.length == 0) return second;
        if (second == null || second.length == 0) return first;
        T[] result = Arrays.copyOf(first, first.length + second.length);
        System.arraycopy(second, 0, result, first.length, second.length);
        return result;
    }

    public static <T> Set<T> getMergedSet(List<T> list1, List<T> list2) {
        Set<T> mergedSet = new HashSet<T>();
        mergedSet.addAll(list1);
        mergedSet.addAll(list2);
        return mergedSet;
    }

    public static <T> String listStringWithoutParenthesis(T[] list) {
        if (list == null || list.length == 0) return null;
        return Arrays.deepToString(list).replaceAll("[\\[\\]]", "");
    }

}