package yhannanum.share;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.HashMap;

public class JSONUtil {
    /**
     * Convert JsonString to Json Array.
     * In Usual case, Use new JSONArray(sourceString) instead of this.
     * If Strings contained in JsonArray has ':', '/', '|', ...(special characters), JSONArray consider those characters as seperator(,).
     * To avoid consider those characters as seperator, use this method When it comes to those situations.
     *
     * @param sourceString - original string of json objects
     * @return JsonArray of Strings
     */
    public static JSONArray convertJsonStringToJsonArray(String sourceString) {
        String[] stringArray = sourceString.replaceAll("[\\[\\] ]", "").split(",");
        JSONArray jsonArray = new JSONArray();
        if (stringArray.length == 1 && stringArray[0].length() < 1) return jsonArray;
        for (String element : stringArray) {
            jsonArray.put(element);
        }
        return jsonArray;
    }

    /**
     * Convert JsonString to Json Array.
     * In Usual case, Use new JSONArray(sourceString) and convert it to String array instead of this.
     * If Strings contained in JsonArray has ':', '/', '|', ...(special characters), JSONArray consider those characters as seperator(,).
     * To avoid consider those characters as seperator, use this method When it comes to those situations.
     *
     * @param jsonString - original string of json objects
     * @return String array of Strings
     */
    public static String[] convertJsonStringToStrings(String jsonString) {
        String[] stringArray = jsonString.replaceAll("[\\[\\] ]", "").split(",");
        if (stringArray.length == 1 && stringArray[0].length() < 1) return new String[]{};
        return stringArray;
    }

    /**
     * insert new object at index of JSONArray
     *
     * @param index - index that object should be inserted
     * @param object - object to be inserted
     * @param restObjects - objects that original JSONArray has
     * @return - new JSONArray that has object and restObjects
     * @throws JSONException
     */
    public static JSONArray insertAtIndex(int index, Object object, JSONArray restObjects) throws JSONException {
        JSONArray newObjects = new JSONArray();
        int indexBeforeInsert = (index >= restObjects.length()?restObjects.length():index);
        for (int i = 0; i < indexBeforeInsert; i++) {
            newObjects.put(restObjects.get(i));
        }
        newObjects.put(object);
        for (int i = indexBeforeInsert; i < restObjects.length(); i++) {
            newObjects.put(restObjects.get(i));
        }
        return newObjects;
    }

    public static JSONArray insertAtIndex(int index, int intValue, JSONArray restObjects) throws JSONException {
        return insertAtIndex(index, new Integer(intValue), restObjects);
    }

    /**
     * It return the index Of string in the jsonArray.
     * If jsonArray has same strings but different indices, it returns the first index of string in the jsonArray.
     *
     * @param string - target string to be found
     * @param jsonArray - jsonArray that has strings as element
     * @return index of string in the jsonArray
     * @throws Exception
     */
    public static int indexOfString(String string, JSONArray jsonArray) throws JSONException {
        for (int i = 0; i < jsonArray.length(); i++) {
            Object objClass = jsonArray.get(i).getClass();
            if (JSONArray.class.equals(objClass)) {
                int index = indexOfString(string, jsonArray.getJSONArray(i));
                if (index != -1) return index;
            } else if (jsonArray.getString(i).equals(string)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * It return the indices of string in the jsonArray.
     * If jsonArray has same strings but different indices, it returns all of indices as JSONArray.
     *
     * @param string - target string to be found
     * @param jsonArray - jsonArray that has strings as element
     * @return indices JSONArray
     * @throws JSONException
     */
    public static JSONArray indicesOfString(String string, JSONArray jsonArray) throws JSONException {
        boolean isString = false;
        JSONArray indices = new JSONArray();
        for (int i = 0; i < jsonArray.length(); i++) {
            Object objClass = jsonArray.get(i).getClass();
            if (JSONArray.class.equals(objClass)) {
                JSONArray indicesOfSubArray = indicesOfString(string, jsonArray.getJSONArray(i));
                if (indicesOfSubArray.length() > 0) {
                    if (indicesOfSubArray.get(0).getClass().equals(Integer.class)) {
                        indices.put(insertAtIndex(0, i, indicesOfSubArray));
                    } else {
                        for (int j = 0; j < indicesOfSubArray.length(); j++) {
                            indices.put(insertAtIndex(0, i, indicesOfSubArray.getJSONArray(j)));
                        }
                    }
                }
            } else if (jsonArray.getString(i).equals(string)) {
                isString = true;
                indices.put(i);
            }
        }

        if (indices.length() > 1 && isString) {
            JSONArray indicesArray = new JSONArray();
            for (int i = 0; i < indices.length(); i++) {
                indicesArray.put(new JSONArray(new int[]{indices.getInt(i)}));
            }
            indices = indicesArray;
        }

        return indices;
    }

    /**
     *
     * jsonArray should has all of object of targetArray.
     * run containAllElement to examine whether all object of targetArray is in the jsonArray
     *
     * @param targetArray - targetArray to be found
     * @param jsonArray - jsonArray that has strings as element
     * @return indices of Objects of targetArray as JSONArray
     * @throws JSONException
     */
    public static JSONArray indicesOfElement(JSONArray targetArray, JSONArray jsonArray) throws JSONException {
        JSONArray targetIndices = new JSONArray();
        for (int i = 0; i < targetArray.length(); i++) {
            Object objClass = targetArray.get(i).getClass();
            if (String.class.equals(objClass)) {
                targetIndices.put(indicesOfString(targetArray.getString(i), jsonArray));
            } else {
                System.out.println("NO String!");
                return null;
            }
        }
        return targetIndices;
    }

    /**
     * It return whether jsonArray contains all Objects of targetArray or not.
     *
     * @param targetArray - target array of elements. elements could be any object
     * @param jsonArray - jsonArray that has elements to be examined
     * @return true when jsonArray contains all elements of targetArray
     * @throws JSONException
     */
    public static boolean containAllElement(JSONArray targetArray, JSONArray jsonArray) throws JSONException {
        for (int i = 0; i < targetArray.length(); i++) {
            Object objClass = targetArray.get(i).getClass();
            if (String.class.equals(objClass)) {
                if (indexOfString(targetArray.getString(i), jsonArray) == -1) {
                    return false;
                }
            } else {
                System.out.println("NO string!");
                return false;
            }
        }
        return true;
    }

    public static <T> String makeHashMapToJsonString(HashMap<String, T> hashMap) {
        String[] keys = hashMap.keySet().toArray(new String[hashMap.size()]);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("{");
        boolean addComma = false;
        for (String key : keys) {
            if (addComma) stringBuilder.append(",");
            else addComma = true;
            stringBuilder.append("\"").append(key).append("\":\"").append(hashMap.remove(key)).append("\"");
        }
        stringBuilder.append("}");

        return stringBuilder.toString();
    }

    @SuppressWarnings("unchecked")
    public static <T> HashMap<String, T> makeJsonStringToHashMap(String jsonString) {
        HashMap<String, T> hashMap = new HashMap<String, T>();
        if (jsonString == null || jsonString.length() < 2) return hashMap;
        jsonString = jsonString.substring(1, jsonString.length() - 1);
        String[] keyValues = jsonString.split(",");
        for (String keyValue : keyValues) {
            String key = keyValue.substring(1, keyValue.lastIndexOf(":") - 1);
            T value = (T)keyValue.substring(keyValue.lastIndexOf(":") + 2, keyValue.length() - 1);
            hashMap.put(key, value);
        }

        return hashMap;
    }

    public static JSONArray mergeJsonArrays(JSONArray jsonArray1, JSONArray jsonArray2) throws JSONException {
        JSONArray newJsonArray = new JSONArray();
        if (jsonArray1 != null) {
            for (int i = 0; i < jsonArray1.length(); i++) {
                newJsonArray.put(jsonArray1.get(i));
            }
        }
        if (jsonArray2 != null) {
            for (int i = 0; i < jsonArray2.length(); i++) {
                newJsonArray.put(jsonArray2.get(i));
            }
        }
        return newJsonArray;
    }
}