package yhannanum.share;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Arthur on 2014. 5. 7..
 */
public class DateUtil {
    public static String reviewCreationDateString(Date originalDate) {
        SimpleDateFormat reviewCreationDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return reviewCreationDateFormat.format(originalDate);
    }

    public static Date dateFromString(String dateString) throws ParseException {
        try {
            if (dateString == null) return new Date();
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
            return dateFormat.parse(dateString);
        } catch (ParseException e) {
            return new Date();
        }
    }

    public static Date dateFromDateString(String dateString) throws ParseException {
        try {
            if (dateString == null) return new Date();
            DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
            return dateFormat.parse(dateString);
        } catch (ParseException e) {
            return new Date();
        }
    }
}
