package yhannanum.share;

public class StringUtil {
    public static String spaceCharacterRegex = "\\p{Zs}" + String.valueOf((char)160) + String.valueOf((char)8192) +
        String.valueOf((char)8193) + String.valueOf((char)8194) + String.valueOf((char)8195) + String.valueOf((char)8196) +
        String.valueOf((char)8197) + String.valueOf((char)8198) + String.valueOf((char)8199) + String.valueOf((char)8200) +
        String.valueOf((char)8201) + String.valueOf((char)8202) + String.valueOf((char)8203) + String.valueOf((char)8239) +
        String.valueOf((char)8287) + String.valueOf((char)65279);
    public static String letterCharacterRegex = "\\p{L}_";
    public static String digitCharacterRegex = "\\d";
    public static String whiteSpaceCharacterRegex = "\\s";
    public static String punctuationCharacterRegex = "\\p{P}";
    public static String symbolCharacterRegex = "\\p{S}";
    public static String multipleDotRegex = "^\\.{2,}$|([^.]+)\\.{2,}$|^\\.{2,}([^.]+)|([^.]+)\\.{2,}([^.]+)";

	public static String concatenateStrings(String initialString, String initialSeperator, String[] strings, String seperator) {
		String concatenatedString = initialString;
		String newConcatenatedString = null;
		for (String string : strings) {
			if (newConcatenatedString == null) {
				newConcatenatedString = string;
			} else {
				newConcatenatedString += seperator + string;
			}
		}
		if (concatenatedString == null) {
			return newConcatenatedString;
		}
		if (newConcatenatedString != null) {
			concatenatedString += initialSeperator + newConcatenatedString; 
		}
		return concatenatedString;
	}
	
	public static String[] combinationOfStrings(String[] stringArray1, String seperator, String[] stringArray2) {
		if (stringArray1.length != stringArray2.length){
			System.out.println("StringUtil\ncombinationOfStrings\n\ttwo array has different length!");
			return null;
		}
		String[] combinationStringArray = new String[stringArray1.length];
		for (int indexOfString = 0; indexOfString < stringArray1.length; indexOfString++) {
			combinationStringArray[indexOfString] = stringArray1[indexOfString] + seperator + stringArray2[indexOfString];
		}
		
		return combinationStringArray;
	}
}
