package yhannanum.share;

/**
 * Created by Arthur on 2014. 6. 1..
 */
public class JsonObjectStringBuilder {
    private StringBuilder stringBuilder = new StringBuilder();

    public JsonObjectStringBuilder() {
        stringBuilder.append("{");
    }

    public JsonObjectStringBuilder(String originalJsonStringBuilder) {
        stringBuilder.append(originalJsonStringBuilder);
        stringBuilder.delete(stringBuilder.length() - 1, stringBuilder.length());
    }

    public void putValue(String key, Object value) {
        if (stringBuilder.length() > 1) stringBuilder.append(",");
        stringBuilder.append("\"").append(key).append("\":\"").append(value.toString().replace("\"", "\\\"").replace("\n", "\\n")).append("\"");
    }

    public void putJson(String key, Object value) {
        if (stringBuilder.length() > 1) stringBuilder.append(",");
        stringBuilder.append("\"").append(key).append("\":").append(value.toString());
    }

    @Override
    public String toString() {
        return stringBuilder.toString() + "}";
    }
}
