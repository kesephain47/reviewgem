package yhannanum.worker;

import java.lang.Exception;
import java.util.Calendar;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.io.File;

import yhannanum.share.Constants;
import yhannanum.share.FileIO;
import yhannanum.db.MysqlConnection;

import org.json.JSONTokener;
import org.json.JSONArray;
import org.json.JSONObject;

public class dictionaryCounter {
    public String todayString(int daysAfter) {
        Calendar cal = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        cal.add(Calendar.DATE, daysAfter);
        return dateFormat.format(cal.getTime());
    }

    public String dateString(String dateString) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(dateString);
    }

    public String baseFileName(boolean isTotal) { return (isTotal ? "total_" : "today_"); }

    public String queryAddedWhere(String baseQuery, boolean isTotal, int daysAfter) {
        if (isTotal) {
            return baseQuery + " where date < adddate(current_date, " + (daysAfter + 1) + ")";
        } else {
            return baseQuery + " where date > adddate(current_date, " + (daysAfter) + ") AND date < adddate(current_date, " + (1 + daysAfter) + ")";
        }
    }

    public JSONArray jsonArrayForQuery(String query) throws Exception {
        MysqlConnection mysql = new MysqlConnection();
        mysql.connect();

        mysql.executeQuery(query);
        JSONArray jsonArray = mysql.getResultRows();

        mysql.close();

        return jsonArray;
    }

    public <T> void saveAsFile(String fileName, String todayString, T object) throws Exception {
        File directory = FileIO.getDirectory(Constants.RULE_DICTIONARY_COUNTER_DIRECTORY_PATH);
        String[] fileList = directory.list();

        List<String> fileStringList = Arrays.asList(fileList);

        JSONObject originalJSONObject = null;
        if (fileStringList.contains(fileName + ".json")) {
            String jsonFile = FileIO.readFile(directory.getPath() + "/" + fileName + ".json");
            JSONTokener tokener = new JSONTokener(jsonFile);
            originalJSONObject = new JSONObject(tokener);
        } else {
            originalJSONObject = new JSONObject();
        }

        originalJSONObject.put(todayString, object);

        FileIO.writeFile(directory.getPath() + "/" + fileName + ".json", originalJSONObject.toString());
    }

    public int countFromTable(String tableName, int daysAfter, boolean isTotal) throws Exception {
        String query = queryAddedWhere("select count(*) as count from " + tableName, isTotal, daysAfter);
        String fileName = baseFileName(isTotal) + tableName + "_count";

        JSONArray jsonResultArray = jsonArrayForQuery(query);

        JSONObject jsonObject = (JSONObject)jsonResultArray.get(0);
        int count = jsonObject.getInt("count");
        saveAsFile(fileName, todayString(daysAfter), count);

        return count;
    }

    public JSONObject submitCountByUser(int daysAfter, boolean isTotal) throws  Exception {
        String query = queryAddedWhere("select user, count(*) as count from submit", isTotal, daysAfter) + "group by user";
        String fileName = baseFileName(isTotal) + "submit_count_by_user";

        JSONArray jsonResultArray = jsonArrayForQuery(query);

        JSONObject totalJsonObject = new JSONObject();
        for (int i = 0; i < jsonResultArray.length(); i++) {
            JSONObject jsonObject = (JSONObject) jsonResultArray.get(i);
            String userName = jsonObject.getString("user");
            int count = jsonObject.getInt("count");
            totalJsonObject.put(userName, count);
        }
        saveAsFile(fileName, todayString(daysAfter), totalJsonObject);

        return totalJsonObject;
    }

    public JSONObject countByUser(String tableName, int daysAfter, boolean isTotal) throws  Exception {
        String query = "select user, crawling_uid from submit";
        String fileName = baseFileName(isTotal) + tableName + "_count_by_user";

        JSONArray jsonResultArray = jsonArrayForQuery(query);

        if (jsonResultArray.length() == 0) {
            saveAsFile(fileName, todayString(daysAfter), "");
            return new JSONObject();
        }

        String reviewIDs = "";
        for (int i = 0; i < jsonResultArray.length(); i++) {
            JSONObject jsonObject = (JSONObject)jsonResultArray.get(i);
            String reviewID = jsonObject.getString("crawling_uid");
            if (i != jsonResultArray.length() - 1) {
                reviewIDs += reviewID + ", ";
            } else {
                reviewIDs += reviewID;
            }
        }

        query = queryAddedWhere("select review_id, count(*) as count from " + tableName, isTotal, daysAfter) + " and review_id in (" + reviewIDs + ") group by review_id";
        JSONArray jsonRuleResultArray = jsonArrayForQuery(query);
        JSONObject ruleCountByReviewID = new JSONObject();
        for (int i = 0; i < jsonRuleResultArray.length(); i++) {
            JSONObject jsonObject = (JSONObject)jsonRuleResultArray.get(i);
            String reviewID = jsonObject.getString("review_id");
            int count = jsonObject.getInt("count");
            ruleCountByReviewID.put(reviewID, count);
        }

        JSONObject totalJsonObject = new JSONObject();
        for (int i = 0; i < jsonResultArray.length(); i++) {
            JSONObject jsonObject = (JSONObject)jsonResultArray.get(i);
            String userName = jsonObject.getString("user");
            String reviewID = jsonObject.getString("crawling_uid");
            int count;
            if (ruleCountByReviewID.has(reviewID)) {
                count = ruleCountByReviewID.getInt(reviewID);
            } else {
                count = 0;
            }
            int beforeCount;
            if (totalJsonObject.has(userName)) {
                beforeCount = totalJsonObject.getInt(userName);
            } else {
                beforeCount = 0;
            }
            totalJsonObject.put(userName, count + beforeCount);
        }

        saveAsFile(fileName, todayString(daysAfter), totalJsonObject);

        return totalJsonObject;
    }

    public static void main(String[] args) throws Exception {
        boolean total = false;
        boolean today = false;
        int beforeDays = 0;
        dictionaryCounter dc = new dictionaryCounter();
        String[] defaultTableNames = {"submit", "rule", "dic_target", "dic_attr", "dic_sentiment"};
        String[] tableNames;

        if (args.length > 0) {
            if (args[0].equals("today")) {
                today = true;
            } else if (args[0].equals("total")) {
                total = true;
            } else {
                today = total = true;
            }
        } else {
            today = total = true;
            beforeDays = 3;
            tableNames = defaultTableNames;
        }
        if (args.length > 1) {
            if (args[1].equals("today")) {
                beforeDays = 0;
            } else if (args[1].equals("yesterday")) {
                beforeDays = 1;
            } else if (args[1].equals("all")) {
                // TO D
                beforeDays = 2;
            } else {
                beforeDays = Integer.parseInt(args[1]);
            }
        } else {
            beforeDays = 0;
            tableNames = defaultTableNames;
        }
        if (args.length > 2) {
            tableNames = new String[args.length - 2];
            for (int i = 2; i < args.length; i++) {
                tableNames[i - 2] = args[i];
            }
        } else {
            tableNames = defaultTableNames;
        }

        for (int day = beforeDays * -1; day <= 0; day++) {
            for (int i = 0; i < tableNames.length; i++) {
                if (total) {
                    dc.countFromTable(tableNames[i], day, true);
                    if (tableNames[i].equals("submit")) {
                        dc.submitCountByUser(day, true);
                    } else {
                        dc.countByUser(tableNames[i], day, true);
                    }
                }
                if (today) {
                    dc.countFromTable(tableNames[i], day, false);
                    if (tableNames[i].equals("submit")) {
                        dc.submitCountByUser(day, false);
                    } else {
                        dc.countByUser(tableNames[i], day, false);
                    }
                }
            }
        }
    }
}