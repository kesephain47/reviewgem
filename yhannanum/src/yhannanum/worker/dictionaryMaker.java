package yhannanum.worker;

import kr.ac.kaist.swrc.jhannanum.comm.Sentence;
import kr.ac.kaist.swrc.jhannanum.hannanum.Workflow;
import kr.ac.kaist.swrc.jhannanum.hannanum.WorkflowFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import yhannanum.comm.Rule;
import yhannanum.converter.SentenceConverter;
import yhannanum.db.RuleTable;
import yhannanum.share.JSONUtil;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Reader;
import java.util.Arrays;

/**
 * Created by Arthur on 2014. 6. 6..
 */
public class dictionaryMaker {
    private JSONObject sentimentInputJsonObject = new JSONObject();
    private JSONObject resultSentimentJsonObject = new JSONObject();
    private Workflow workflow = null;
    private SentenceConverter sentenceConverter = null;
    private static final String filePath = "/var/sent.xml";
    private static RuleTable rt = null;

    private JSONArray getSplitEojeolJsonArray(JSONArray jsonArray) throws Exception {
        return getSplitEojeolJsonArray(jsonArray, false);
    }

    private JSONArray getSplitEojeolJsonArray(JSONArray jsonArray, boolean checkHas) throws Exception {
        JSONArray splitEojeolJsonArray = new JSONArray();
        for (int i = 0; i < jsonArray.length(); i++) {
            String eojeolString = jsonArray.getString(i);
            String[] eojeols = eojeolString.split(" ");
            for (String eojeol : eojeols) {
                if (eojeol.equals(".")) continue;
                if (eojeol.length() > 0) {
                    if (checkHas) {
                        boolean has = false;
                        for (int j = 0; j < splitEojeolJsonArray.length(); j++) {
                            if (splitEojeolJsonArray.getString(j).equals(eojeol)) {
                                has = true;
                            }
                        }
                        if (!has) {
                            splitEojeolJsonArray.put(eojeol);
                        }
                    } else {
                        splitEojeolJsonArray.put(eojeol);
                    }
                }
            }
        }
        return splitEojeolJsonArray;
    }

    public static void updateTableWithRule(JSONObject rule) throws Exception {
        if (rt == null) {
            rt = new RuleTable();
            rt.connect();
        }
        String query = "INSERT INTO rule (review_id, sentence_id, sentence, origin_attr, sentiment_type, date, target, attr, sentiment, " +
                "target_eojeol, attr_eojeol, sentiment_eojeol, target_tag, attr_tag, sentiment_tag, target_index, attr_index, sentiment_index) VALUES " +
                "( -1, -1, \"" + rule.getString("sentence") + "\", \"" + rule.getString("origin_attr") + "\", " + rule.getInt("sentiment_type") +
                ", now(), \"" + rule.getString("target") + "\", \"" + rule.getString("attr") + "\", \"" + rule.getString("sentiment") +
                "\", \"" + rule.getString("target_eojeol") + "\", \"" + rule.getString("attr_eojeol") + "\", \"" + rule.getString("sentiment_eojeol") +
                "\", \"" + rule.getString("target_tag") + "\", \"" + rule.getString("attr_tag") + "\", \"" + rule.getString("sentiment_tag") +
                "\", \"" + rule.getString("target_index") + "\", \"" + rule.getString("attr_index") + "\", \"" + rule.getString("sentiment_index") +
                "\")";
        rt.executeUpdate(query);
    }

    public dictionaryMaker() throws Exception {
        this.workflow = WorkflowFactory.getPredefinedWorkflow(WorkflowFactory.WORKFLOW_POS_SIMPLE_22);
        workflow.activateWorkflow(true);
        sentenceConverter = new SentenceConverter();
        readSentimentInputFile();
        setSentimentTags();

        int count = 0;

        String[] keys = JSONObject.getNames(resultSentimentJsonObject);
        for (String key : keys) {
            JSONArray keyJsonArray = resultSentimentJsonObject.getJSONArray(key);
            for (int i = 0; i < keyJsonArray.length(); i++) {
                JSONObject convertedSentence = keyJsonArray.getJSONObject(i);
                String originalSentence = convertedSentence.getString("originalSentence");
                String sentence = convertedSentence.getString("sentence");
                String modifiedSentence = convertedSentence.getString("modifiedSentence");
                JSONArray targetEojeolJsonArray = convertedSentence.getJSONArray("target_eojeols");
                JSONArray attrEojeolJsonArray = convertedSentence.getJSONArray("attr_eojeols");
                JSONArray sentimentEojeolJsonArray = convertedSentence.getJSONArray("sentiment_eojeols");
                int sentimentType = convertedSentence.getInt("sentimentType");
                JSONArray morphemes = convertedSentence.getJSONArray("morphemes");
                JSONArray eojeols = convertedSentence.getJSONArray("eojeols");
                JSONArray tags = convertedSentence.getJSONArray("tags");

                JSONArray targetSplitEojeolJsonArray = getSplitEojeolJsonArray(targetEojeolJsonArray, true);
                JSONArray attrSplitEojeolJsonArray = getSplitEojeolJsonArray(attrEojeolJsonArray, true);
                JSONArray sentimentSplitEojeolJsonArray = getSplitEojeolJsonArray(sentimentEojeolJsonArray);

                JSONArray targetEojeolsIndices = JSONUtil.indicesOfElement(targetSplitEojeolJsonArray, eojeols);
                JSONArray attrEojeolIndices = JSONUtil.indicesOfElement(attrSplitEojeolJsonArray, eojeols);
                JSONArray sentimentEojeolIndices = JSONUtil.indicesOfElement(sentimentSplitEojeolJsonArray, eojeols);

                JSONArray targetEojeolNotClearedIndices = fillMissingFieldsOfRuleTable.simpleAmbiguityModificationOfEojeolIndex(targetEojeolsIndices);
                JSONArray attrEojeolNotClearedIndices = fillMissingFieldsOfRuleTable.simpleAmbiguityModificationOfEojeolIndex(attrEojeolIndices);
                JSONArray sentimentEojeolNotClearedIndices = fillMissingFieldsOfRuleTable.simpleAmbiguityModificationOfEojeolIndex(sentimentEojeolIndices);

                fillMissingFieldsOfRuleTable.modifyAmbiguityWithOtherSections(attrEojeolIndices, sentimentEojeolIndices, targetEojeolsIndices, attrEojeolNotClearedIndices);
                fillMissingFieldsOfRuleTable.modifyAmbiguityWithOtherSections(sentimentEojeolIndices, attrEojeolIndices, targetEojeolsIndices, sentimentEojeolNotClearedIndices);
                fillMissingFieldsOfRuleTable.modifyAmbiguityWithOtherSections(targetEojeolsIndices, attrEojeolIndices, sentimentEojeolIndices, targetEojeolNotClearedIndices);
                if (!(targetEojeolNotClearedIndices.length() + attrEojeolNotClearedIndices.length() + sentimentEojeolNotClearedIndices.length() == 0)) {
                    System.out.println("Error");
                } else {
                    count++;
                    JSONArray target = fillMissingFieldsOfRuleTable.morphemesFromEojeols(targetEojeolsIndices, morphemes);
                    JSONArray attr = fillMissingFieldsOfRuleTable.morphemesFromEojeols(attrEojeolIndices, morphemes);
                    JSONArray sentiment = fillMissingFieldsOfRuleTable.morphemesFromEojeols(sentimentEojeolIndices, morphemes);

                    JSONArray target_indices = fillMissingFieldsOfRuleTable.morphemeIndicesFromEojeolIndices(targetEojeolsIndices, morphemes);
                    JSONArray attr_indices = fillMissingFieldsOfRuleTable.morphemeIndicesFromEojeolIndices(attrEojeolIndices, morphemes);
                    JSONArray sentiment_indices = fillMissingFieldsOfRuleTable.morphemeIndicesFromEojeolIndices(sentimentEojeolIndices, morphemes);

                    JSONArray target_eojeol = fillMissingFieldsOfRuleTable.eojeolsFromMorphemes(target_indices, eojeols);
                    JSONArray attr_eojeol = fillMissingFieldsOfRuleTable.eojeolsFromMorphemes(attr_indices, eojeols);
                    JSONArray sentiment_eojeol = fillMissingFieldsOfRuleTable.eojeolsFromMorphemes(sentiment_indices, eojeols);

                    JSONArray target_tag = fillMissingFieldsOfRuleTable.tagsFromMorphemes(target_indices, tags);
                    JSONArray attr_tag = fillMissingFieldsOfRuleTable.tagsFromMorphemes(attr_indices, tags);
                    JSONArray sentiment_tag = fillMissingFieldsOfRuleTable.tagsFromMorphemes(sentiment_indices, tags);
                    if (target.length() != target_indices.length() || target.length() != target_tag.length() || target_indices.length() != target_tag.length()) {
                        System.out.println(count + " " + target + " " + target_indices + " " + target_tag + " " + targetEojeolJsonArray + " " + targetSplitEojeolJsonArray + " " + targetEojeolsIndices);
                    }
                    if (attr.length() != attr_indices.length() || attr.length() != attr_tag.length() || attr_indices.length() != attr_tag.length()) {
                        System.out.println(count + " " + attr + " " + attr_indices + " " + attr_tag + " " + attrEojeolJsonArray + " " + attrSplitEojeolJsonArray + " " + attrEojeolIndices);
                    }
                    if (sentiment.length() != sentiment_indices.length() || sentiment.length() != sentiment_tag.length() || sentiment_indices.length() != sentiment_tag.length()) {
                        System.out.println(count + " " + sentiment + " " + sentiment_indices + " " + sentiment_tag + " " + sentimentEojeolJsonArray + " " + sentimentSplitEojeolJsonArray + " " + sentimentEojeolIndices);
                    }
                    JSONObject rule = new JSONObject();
                    rule.put("sentence", sentence);
                    rule.put("target_index", target_indices.toString().replaceAll("\"", ""));
                    rule.put("attr_index", attr_indices.toString().replaceAll("\"", ""));
                    rule.put("sentiment_index", sentiment_indices.toString().replaceAll("\"", ""));
                    rule.put("target_eojeol", target_eojeol.toString().replaceAll("\"", ""));
                    rule.put("attr_eojeol", attr_eojeol.toString().replaceAll("\"", ""));
                    rule.put("sentiment_eojeol", sentiment_eojeol.toString().replaceAll("\"", ""));
                    rule.put("target", target.toString().replaceAll("\"", ""));
                    rule.put("attr", attr.toString().replaceAll("\"", ""));
                    rule.put("sentiment", sentiment.toString().replaceAll("\"", ""));
                    rule.put("target_tag", target_tag.toString().replaceAll("\"", ""));
                    rule.put("attr_tag", attr_tag.toString().replaceAll("\"", ""));
                    rule.put("sentiment_tag", sentiment_tag.toString().replaceAll("\"", ""));
                    rule.put("sentiment_type", sentimentType);
                    rule.put("origin_attr", key);
                    updateTableWithRule(rule);

                }
            }
        }
    }

    private void readSentimentInputFile() throws Exception {
        Reader reader = new FileReader(filePath);
        BufferedReader bufferedReader = new BufferedReader(reader);
        String line;
        String key = null;
        boolean isWorksheetOn = false;
        JSONArray sentimentInputJsonArray = null;
        boolean isRowOn = false;
        while((line = bufferedReader.readLine()) != null) {
            if (line.contains("<Worksheet ")) {
                isWorksheetOn = true;
                int firstIndex = line.indexOf("Name=\"") + "Name=\"".length();
                int lastIndex = line.lastIndexOf("\">");
                key = line.substring(firstIndex, lastIndex);
            }
            if (isWorksheetOn && line.contains("</Worksheet ")) {
                isWorksheetOn = false;
                key = null;
            }
            if (isWorksheetOn && (line.contains("<Row ") || line.contains("<Row>"))) {
                isRowOn = true;
                sentimentInputJsonArray = new JSONArray();
            }
            if (isWorksheetOn && isRowOn && line.contains("</Row>")) {
                isRowOn = false;
                if (sentimentInputJsonArray.length() > 0) {
                    if (!sentimentInputJsonObject.has(key)) {
                        sentimentInputJsonObject.put(key, new JSONArray());
                    }
                    JSONArray rowSentimentJsonArray = sentimentInputJsonObject.getJSONArray(key);
                    rowSentimentJsonArray.put(sentimentInputJsonArray);
                }
                sentimentInputJsonArray = null;
            }
            if (isWorksheetOn && isRowOn && (line.contains("<Cell ") || line.contains("<Cell>"))) {
                line = line.substring(line.indexOf(">") + 1, line.length());
                if (line.length() <= 0) continue;
                line = line.substring(line.indexOf(">") + 1, line.length());
                line = line.substring(0, line.indexOf("<"));
                sentimentInputJsonArray.put(line);
            }
        }
        bufferedReader.close();
        reader.close();
        System.out.println("Finish read file");
    }

    private JSONArray setRuleArgumentJsonArray(JSONObject resultJsonObject, String startCharacter, String endCharacter, int numberOfLastOffset) throws Exception {
        JSONArray ruleArgumentJsonArray = new JSONArray();
        int firstIndex = -1;
        while((firstIndex = resultJsonObject.getString("sentence").indexOf(startCharacter)) >= 0) {
            int lastIndex = resultJsonObject.getString("sentence").indexOf(endCharacter, firstIndex);
            String ruleArugment = resultJsonObject.getString("sentence").substring(firstIndex + startCharacter.length(), lastIndex - numberOfLastOffset);
            String originalRuleArgument = resultJsonObject.getString("sentence").substring(firstIndex, lastIndex + endCharacter.length());
            boolean doNotHave = true;
            for (int i = 0; i < ruleArgumentJsonArray.length(); i++) {
                if (ruleArgumentJsonArray.getString(i).equals(ruleArugment)) {
                    doNotHave = false;
                    break;
                }
            }
            ruleArugment = ruleArugment.replaceAll("([-/.*!%+])", " $1 ").replaceAll("[(),:?~]", "");
            if (doNotHave) {
                ruleArgumentJsonArray.put(ruleArugment);
            }
            resultJsonObject.put("sentence", resultJsonObject.getString("sentence").replace(originalRuleArgument, " " + ruleArugment + " "));
        }
        return ruleArgumentJsonArray;
    }

    private int getSentimentType(String sentimentTypeString) {
        int sentimentType = -1;
        if (sentimentTypeString.equals("긍정")) {
            sentimentType = 0;
        } else if (sentimentTypeString.equals("부정")) {
            sentimentType = 1;
        } else if (sentimentTypeString.equals("중립")) {
            sentimentType = 2;
        }
        return sentimentType;
    }

    private JSONObject analyze(String sentence) throws Exception {
        JSONObject convertedSentence = new JSONObject();
        while (true) {
            Sentence s = workflow.getResultOfSentence(new Sentence(0, 0, false));
            JSONArray morphemes = sentenceConverter.convertSentenceToJSON(null, "morphemes", s, SentenceConverter.ONLY_MORPHEME).getJSONArray("morphemes");
            String plainSentence = sentenceConverter.convertSentenceToJSON(null, "sentence", s, SentenceConverter.ONLY_PLAIN_SENTENCE).getString("sentence");
            JSONArray eojeols = sentenceConverter.convertSentenceToJSON(null, "eojeols", s, SentenceConverter.ONLY_PLAIN_EOJEOL).getJSONArray("eojeols");
            JSONArray tags = sentenceConverter.convertSentenceToJSON(null, "tags", s, SentenceConverter.ONLY_TAG).getJSONArray("tags");

            if (!convertedSentence.has("morphemes")) {
                convertedSentence.put("morphemes", new JSONArray());
            }
            JSONArray convertedSentenceMorphemes = convertedSentence.getJSONArray("morphemes");
            for (int j = 0; j < morphemes.length(); j++) {
                convertedSentenceMorphemes.put(morphemes.get(j));
            }

            if (!convertedSentence.has("eojeols")) {
                convertedSentence.put("eojeols", new JSONArray());
            }
            JSONArray convertedSentenceEojeols = convertedSentence.getJSONArray("eojeols");
            for (int j = 0; j < eojeols.length(); j++) {
                convertedSentenceEojeols.put(eojeols.get(j));
            }

            if (!convertedSentence.has("tags")) {
                convertedSentence.put("tags", new JSONArray());
            }
            JSONArray convertedSentenceTags = convertedSentence.getJSONArray("tags");
            for (int j = 0; j < tags.length(); j++) {
                convertedSentenceTags.put(tags.get(j));
            }

            if (convertedSentence.has("sentence")) {
                convertedSentence.put("sentence", convertedSentence.getString("sentence") + "\n" + plainSentence);
            } else {
                convertedSentence.put("sentence", plainSentence);
            }
            if (s.isEndOfDocument()) {
                break;
            }
        }
        return convertedSentence;
    }

    private void setSentimentTags() throws Exception {
        String[] keys = JSONObject.getNames(sentimentInputJsonObject);
        for (String key : keys) {
            if (key.equals("Total")) continue;
            resultSentimentJsonObject.put(key, new JSONArray());
            JSONArray rowSentimentJsonArray = sentimentInputJsonObject.getJSONArray(key);
            JSONArray resultSentimentJsonArray = resultSentimentJsonObject.getJSONArray(key);
            for (int i = 0; i < rowSentimentJsonArray.length(); i++) {
                JSONArray rowSentimentInfos = rowSentimentJsonArray.getJSONArray(i);
                JSONObject rowSentimentResult = new JSONObject();

                String sentence = rowSentimentInfos.getString(3).replaceAll("&#13;", "\n").replaceAll("&#45;", "-");
                rowSentimentResult.put("originalSentence", sentence);
                rowSentimentResult.put("sentence", sentence);

                JSONArray targetJsonArray = setRuleArgumentJsonArray(rowSentimentResult, "{", "}", 0);
                JSONArray attrJsonArray = setRuleArgumentJsonArray(rowSentimentResult, "[", "]", 0);
                JSONArray sentimentJsonArray = setRuleArgumentJsonArray(rowSentimentResult, "&lt;", "&gt;", 2);
                rowSentimentResult.put("target_eojeols", targetJsonArray);
                rowSentimentResult.put("attr_eojeols", attrJsonArray);
                rowSentimentResult.put("sentiment_eojeols", sentimentJsonArray);

                int sentimentType = getSentimentType(rowSentimentInfos.getString(4));
                rowSentimentResult.put("sentimentType", sentimentType);

                if (!workflow.isAnalyzable(rowSentimentResult.getString("sentence"))) continue;

                workflow.analyze(rowSentimentResult.getString("sentence"));

                JSONObject convertedSentence = analyze(rowSentimentResult.getString("sentence"));

                rowSentimentResult.put("morphemes", convertedSentence.getJSONArray("morphemes"));
                rowSentimentResult.put("eojeols", convertedSentence.getJSONArray("eojeols"));
                rowSentimentResult.put("tags", convertedSentence.getJSONArray("tags"));
                rowSentimentResult.put("modifiedSentence", convertedSentence.getString("sentence"));

                resultSentimentJsonArray.put(rowSentimentResult);
            }
            resultSentimentJsonObject.put(key, resultSentimentJsonArray);
        }
        workflow.close();
        System.out.println("Finish make tags");
    }

    public static void main(String[] args) throws Exception {
        dictionaryMaker dictionaryMaker = new dictionaryMaker();
    }
}
