package yhannanum.worker;

/**
 * Created by Arthur on 2014. 4. 18..
 */

import kr.ac.kaist.swrc.jhannanum.hannanum.Workflow;
import kr.ac.kaist.swrc.jhannanum.hannanum.WorkflowFactory;
import kr.ac.kaist.swrc.jhannanum.comm.Sentence;

import org.json.JSONException;
import org.json.JSONTokener;
import org.json.JSONArray;
import org.json.JSONObject;

import yhannanum.comm.Crawling;
import yhannanum.db.CrawlingTable;
import yhannanum.db.RuleTable;
import yhannanum.converter.SentenceConverter;
import yhannanum.share.ArrayUtil;
import yhannanum.share.FileIO;
import yhannanum.share.JSONUtil;

public class fillMissingFieldsOfRuleTable {

    public static int totalNumberOfError = 0;
    public static RuleTable rt = null;

    // TO DO : Can Get ids as parameter
    public static void clearMissingFieldsOfRuleTable() throws Exception {
        RuleTable rt = new RuleTable();
        rt.connect();
        rt.executeQuery("UPDATE rule SET target_eojeol = NULL, attr_eojeol = NULL, sentiment = NULL, " +
                "target_tag = NULL, attr_tag = NULL, sentiment_tag = NULL, " +
                "target_index = NULL, attr_index = NULL, sentiment_index = NULL " +
                "WHERE (target_eojeol IS NOT NULL OR attr_eojeol IS NOT NULL OR sentiment IS NOT NULL OR " +
                "target_tag IS NOT NULL OR attr_tag IS NOT NULL OR sentiment_tag IS NOT NULL OR " +
                "target_index IS NOT NULL OR attr_index IS NOT NULL OR sentiment_index IS NOT NULL)");
        rt.close();
    }

    public static JSONObject targetRules(int indexOfFile, Integer[] uids, JSONObject ruleTableResultSet) throws Exception {
        JSONObject targetRules = new JSONObject();
        int reviewID = uids[indexOfFile];
        JSONArray idJSONArray = ruleTableResultSet.getJSONArray("id");
        JSONArray review_idJSONArray = ruleTableResultSet.getJSONArray("review_id");
        JSONArray sentence_idJSONArray = ruleTableResultSet.getJSONArray("sentence_id");
        JSONArray sentenceJSONArray = ruleTableResultSet.getJSONArray("sentence");
        JSONArray origin_attrJSONArray = ruleTableResultSet.getJSONArray("origin_attr");
        JSONArray targetJSONArray = ruleTableResultSet.getJSONArray("target");
        JSONArray attrJSONArray = ruleTableResultSet.getJSONArray("attr");
        JSONArray sentiment_eojeolJSONArray = ruleTableResultSet.getJSONArray("sentiment_eojeol");
        JSONArray sentiment_typeJSONArray = ruleTableResultSet.getJSONArray("sentiment_type");
        JSONArray parsed_targetJSONArray = ruleTableResultSet.getJSONArray("parsed_target");
        JSONArray parsed_attrJSONArray = ruleTableResultSet.getJSONArray("parsed_attr");
        JSONArray parsed_sentiment_eojeolJSONArray = ruleTableResultSet.getJSONArray("parsed_sentiment_eojeol");


        for (int i = 0; i < review_idJSONArray.length(); i++) {
            int ruleReviewID = review_idJSONArray.getInt(i);
            if (ruleReviewID == reviewID) {
                JSONObject targetRule = new JSONObject();
                targetRule.put("id", idJSONArray.getInt(i));
                targetRule.put("review_id", review_idJSONArray.getInt(i));
                targetRule.put("sentence_id", sentence_idJSONArray.getInt(i));
                targetRule.put("sentence", sentenceJSONArray.getString(i));
                targetRule.put("origin_attr", origin_attrJSONArray.getString(i));
                targetRule.put("target", targetJSONArray.getString(i));
                targetRule.put("attr", attrJSONArray.getString(i));
                targetRule.put("sentiment_eojeol", sentiment_eojeolJSONArray.getString(i));
                targetRule.put("sentiment_type", sentiment_typeJSONArray.getInt(i));
                targetRule.put("parsed_target", parsed_targetJSONArray.getJSONArray(i));
                targetRule.put("parsed_attr", parsed_attrJSONArray.getJSONArray(i));
                targetRule.put("parsed_sentiment_eojeol", parsed_sentiment_eojeolJSONArray.getJSONArray(i));
                String sentenceID = sentence_idJSONArray.getString(i);
                if (!targetRules.has(sentenceID)) {
                    targetRules.put(sentenceID, new JSONArray());
                }
                JSONArray targetSentenceRules = targetRules.getJSONArray(sentenceID);
                targetSentenceRules.put(targetRule);
            }
        }

        return targetRules;
    }

    public static boolean isRightRule(JSONObject rule, JSONObject analyzedResult) throws Exception {
        JSONArray target = rule.getJSONArray("parsed_target");
        JSONArray attr = rule.getJSONArray("parsed_attr");
        JSONArray sentiment_eojeol = rule.getJSONArray("parsed_sentiment_eojeol");
        JSONArray morphemes = analyzedResult.getJSONArray("morphemes");
        JSONArray eojeols = analyzedResult.getJSONArray("eojeol");

//        if (!JSONUtil.containAllElement(target, morphemes)) {
//            System.out.println("target is not right");
//            System.out.println(target + " \n" + morphemes);
//        }
//
//        if (!JSONUtil.containAllElement(attr, morphemes)) {
//            System.out.println("attr is not right");
//            System.out.println(attr + " \n" + morphemes);
//        }
//
//        if (!JSONUtil.containAllElement(sentiment_eojeol, eojeols)) {
//            System.out.println("sentiment_eojeol is not right");
//            System.out.println(sentiment_eojeol + " \n" + morphemes);
//        }

        return (JSONUtil.containAllElement(target, morphemes) &&
                JSONUtil.containAllElement(attr, morphemes) &&
                JSONUtil.containAllElement(sentiment_eojeol, eojeols));
    }

    public static JSONObject offsetBetweenEojeol(int targetIndex, int sideIndex, JSONArray targetMorphemeIndex, JSONArray sideMorphemeIndex, boolean reverse, boolean withDirection) throws JSONException {
        JSONObject offset = new JSONObject();
        offset.put("targetIndex", targetIndex);
        offset.put("sideIndex", sideIndex);
        offset.put("eojeolOffset", Math.abs(targetMorphemeIndex.getInt(0) - sideMorphemeIndex.getInt(0)));
        if (targetMorphemeIndex.length() > 1 && sideMorphemeIndex.length() > 1) {
            offset.put("morphemeOffset", Math.abs(targetMorphemeIndex.getInt(1) - sideMorphemeIndex.getInt(1)));
        } else {
            offset.put("morphemeOffset", 0);
        }
        if (withDirection) {
            offset.put("forwardDirectionEojeol", (reverse ? (targetMorphemeIndex.getInt(0) - sideMorphemeIndex.getInt(0) >= 0) : (sideMorphemeIndex.getInt(0) - targetMorphemeIndex.getInt(0) >= 0)));
            if (targetMorphemeIndex.length() > 1 && sideMorphemeIndex.length() > 1) {
                offset.put("forwardDirectionMorpheme", (reverse ? (targetMorphemeIndex.getInt(1) - sideMorphemeIndex.getInt(1) >= 0) : (sideMorphemeIndex.getInt(1) - targetMorphemeIndex.getInt(1) >= 0)));
            } else {
                offset.put("forwardDirectionMorpheme", true);
            }
        } else {
            offset.put("forwardDirectionEojeol", true);
            offset.put("forwardDirectionMorpheme", true);
        }

        return offset;
    }

    public static JSONArray offsetBetweenEojeols(JSONArray targetEojeolIndices, JSONArray sideEojeolIndices) throws  JSONException {
        return offsetBetweenEojeols(targetEojeolIndices, sideEojeolIndices, false, true);
    }

    public static JSONArray offsetBetweenEojeols(JSONArray targetEojeolIndices, JSONArray sideEojeolIndices, boolean reverse) throws JSONException {
        return offsetBetweenEojeols(targetEojeolIndices, sideEojeolIndices, reverse, true);
    }

    public static JSONArray offsetBetweenEojeols(JSONArray targetEojeolIndices, JSONArray sideEojeolIndices, boolean reverse, boolean withDirection) throws  JSONException {
        JSONArray offsets = new JSONArray();

        for (int i = 0; i < targetEojeolIndices.length(); i++) {
            int targetIndex;
            JSONArray targetEojeolIndex;
            if (targetEojeolIndices.get(i).getClass().equals(Integer.class)) {
                targetEojeolIndex = targetEojeolIndices;
                targetIndex = -1;
            } else {
                targetEojeolIndex = targetEojeolIndices.getJSONArray(i);
                targetIndex = i;
            }
            for (int j = 0; j < sideEojeolIndices.length(); j++) {
                int sideIndex;
                JSONArray sideEojeolIndex;
                if (sideEojeolIndices.get(j).getClass().equals(Integer.class)) {
                    sideEojeolIndex = sideEojeolIndices;
                    sideIndex = -1;
                } else {
                    sideEojeolIndex = sideEojeolIndices.getJSONArray(j);
                    sideIndex = j;
                }
                offsets.put(offsetBetweenEojeol(targetIndex, sideIndex, targetEojeolIndex, sideEojeolIndex, reverse, withDirection));
            }
        }

        return offsets;
    }

    public static JSONObject bestOffsetForOnlyOneSide(JSONArray offsets) throws JSONException{
        int smallestEojeolOffset = 9999;
        int smallestMorphemeOffset = 9999;
        boolean forwardDirectionEojeol = false;
        boolean forwardDirectionMorpheme = false;
        int bestOffsetIndex = -1;
        for (int j = 0; j < offsets.length(); j++) {
            JSONObject offset = offsets.getJSONObject(j);
            if (smallestEojeolOffset > offset.getInt("eojeolOffset")) {
                smallestEojeolOffset = offset.getInt("eojeolOffset");
                forwardDirectionEojeol = offset.getBoolean("forwardDirectionEojeol");
                smallestMorphemeOffset = offset.getInt("morphemeOffset");
                forwardDirectionMorpheme = offset.getBoolean("forwardDirectionMorpheme");
                bestOffsetIndex = j;
            } else if (smallestEojeolOffset == offset.getInt("eojeolOffset")) {
                if (!forwardDirectionEojeol) {
                    forwardDirectionEojeol = offset.getBoolean("forwardDirectionEojeol");
                    smallestMorphemeOffset = offset.getInt("morphemeOffset");
                    forwardDirectionMorpheme = offset.getBoolean("forwardDirectionMorpheme");
                    bestOffsetIndex = j;
                } else if (smallestMorphemeOffset > offset.getInt("morphemeOffset")) {
                    smallestMorphemeOffset = offset.getInt("morphemeOffset");
                    forwardDirectionMorpheme = offset.getBoolean("forwardDirectionMorpheme");
                    bestOffsetIndex = j;
                } else if (smallestMorphemeOffset == offset.getInt("morphemeOffset") && !forwardDirectionMorpheme) {
                    forwardDirectionMorpheme = offset.getBoolean("forwardDirectionMorpheme");
                    bestOffsetIndex = j;
                }
            }
        }

        return offsets.getJSONObject(bestOffsetIndex);
    }

    public static JSONObject chooseBestOffset(JSONObject leftBestOffset, JSONObject rightBestOffset) throws JSONException {

        if (leftBestOffset.getInt("targetIndex") == rightBestOffset.getInt("targetIndex")) {
            return leftBestOffset;
        }

        int leftEojeolOffset = leftBestOffset.getInt("eojeolOffset");
        int leftMorphemeOffset = leftBestOffset.getInt("morphemeOffset");
        boolean leftEojeolDirection = leftBestOffset.getBoolean("forwardDirectionEojeol");
        boolean leftMorphemeDirection = leftBestOffset.getBoolean("forwardDirectionMorpheme");

        int rightEojeolOffset = rightBestOffset.getInt("eojeolOffset");
        int rightMorphemeOffset = rightBestOffset.getInt("morphemeOffset");
        boolean rightEojeolDirection = rightBestOffset.getBoolean("forwardDirectionEojeol");
        boolean rightMorphemeDirection = rightBestOffset.getBoolean("forwardDirectionMorpheme");

        if (leftEojeolDirection && !rightEojeolDirection) {
            return leftBestOffset;
        } else if (!leftEojeolDirection && rightEojeolDirection) {
            return rightBestOffset;
        } else {
            if (leftEojeolOffset < rightEojeolOffset) {
                return leftBestOffset;
            } else  if (leftEojeolOffset > rightEojeolOffset) {
                return rightBestOffset;
            } else {
                if (leftMorphemeDirection && !rightMorphemeDirection) {
                    return leftBestOffset;
                } else if (!leftMorphemeDirection && rightMorphemeDirection) {
                    return rightBestOffset;
                } else {
                    return (leftMorphemeOffset > rightMorphemeOffset ? rightBestOffset : leftBestOffset);
                }
            }
        }
    }

    public static void setAdjacentEojeolIndex(JSONArray eojeolIndices, JSONArray notClearedIndices) throws  JSONException {
        for (int i = 0; i < notClearedIndices.length(); i++) {
            int notClearedIndex = notClearedIndices.getInt(i);
            JSONArray leftOffsets = null;
            JSONArray rightOffsets = null;
            JSONObject bestOffset = null;
            if (notClearedIndex == 0) {
                rightOffsets = offsetBetweenEojeols(eojeolIndices.getJSONArray(notClearedIndex), eojeolIndices.getJSONArray(notClearedIndex + 1));
                bestOffset = bestOffsetForOnlyOneSide(rightOffsets);
            } else if (notClearedIndex > 0 && notClearedIndex < eojeolIndices.length() - 1) {
                leftOffsets = offsetBetweenEojeols(eojeolIndices.getJSONArray(notClearedIndex), eojeolIndices.getJSONArray(notClearedIndex - 1), true);
                rightOffsets = offsetBetweenEojeols(eojeolIndices.getJSONArray(notClearedIndex), eojeolIndices.getJSONArray(notClearedIndex + 1));
                JSONObject bestOffsetForLeft = bestOffsetForOnlyOneSide(leftOffsets);
                JSONObject bestOffsetForRight = bestOffsetForOnlyOneSide(rightOffsets);
                bestOffset = chooseBestOffset(bestOffsetForLeft, bestOffsetForRight);
            } else {
                leftOffsets = offsetBetweenEojeols(eojeolIndices.getJSONArray(notClearedIndex), eojeolIndices.getJSONArray(notClearedIndex - 1), true);
                bestOffset = bestOffsetForOnlyOneSide(leftOffsets);
            }
            JSONArray notClearedMorphemeIndices = eojeolIndices.getJSONArray(notClearedIndex);
            int bestIndex = bestOffset.getInt("targetIndex");
            JSONArray bestMorphemeIndex = notClearedMorphemeIndices.getJSONArray(bestIndex);
            eojeolIndices.put(notClearedIndex, bestMorphemeIndex);
            notClearedIndices.remove(i);
        }
    }

    public static JSONArray simpleAmbiguityModificationOfEojeolIndex(JSONArray eojeolIndices) throws JSONException {
        JSONArray notClearedIndices = new JSONArray();

        for (int i = 0; i < eojeolIndices.length(); i++) {
            JSONArray eojeolIndex = eojeolIndices.getJSONArray(i);
            if (eojeolIndex.length() == 1) {
                if (eojeolIndex.get(0).getClass().equals(Integer.class)) {
                    eojeolIndices.put(i, eojeolIndex);
                } else {
                    eojeolIndices.put(i, eojeolIndex.getJSONArray(0));
                }
            } else {
                notClearedIndices.put(i);
            }
        }

        while(notClearedIndices.length() > 0 && eojeolIndices.length() > 1) {
            setAdjacentEojeolIndex(eojeolIndices, notClearedIndices);
        }

        return notClearedIndices;
    }

    public static void modifyAmbiguityWithOtherSections(JSONArray targetIndices, JSONArray otherIndices1, JSONArray otherIndices2, JSONArray targetNotClearedIndices) throws JSONException {
        for (int i = 0; i < targetNotClearedIndices.length();) {
            int notClearedIndex = targetNotClearedIndices.getInt(i);
            JSONObject bestOffsetForOther1 = null;
            for (int j = 0; j < otherIndices1.length(); j++) {
                JSONArray offsetWithOther1 = offsetBetweenEojeols(targetIndices.getJSONArray(notClearedIndex), otherIndices1.getJSONArray(j), false, false);
                JSONObject betterOffsetForOther1 = bestOffsetForOnlyOneSide(offsetWithOther1);
                if (betterOffsetForOther1.getInt("eojeolOffset") == 0) continue;
                if (bestOffsetForOther1 == null) bestOffsetForOther1 = betterOffsetForOther1;
                else {
                    bestOffsetForOther1 = chooseBestOffset(bestOffsetForOther1, betterOffsetForOther1);
                }
            }
            JSONObject bestOffsetForOther2 = null;
            for (int j = 0; j < otherIndices2.length(); j++) {
                JSONArray offsetWithOther2 = offsetBetweenEojeols(targetIndices.getJSONArray(notClearedIndex), otherIndices2.getJSONArray(j), false, false);
                JSONObject betterOffsetForOther2 = bestOffsetForOnlyOneSide(offsetWithOther2);
                if (betterOffsetForOther2.getInt("eojeolOffset") == 0) continue;
                if (bestOffsetForOther2 == null) bestOffsetForOther2 = betterOffsetForOther2;
                else {
                    bestOffsetForOther2 = chooseBestOffset(bestOffsetForOther2, betterOffsetForOther2);
                }
            }

            JSONArray notClearedMorphemeIndices = targetIndices.getJSONArray(notClearedIndex);
            JSONArray bestMorphemeIndex = null;
            int bestIndex = 0;
            if (bestOffsetForOther1 == null && bestOffsetForOther2 == null) {
                bestIndex = 0;
            } else if (bestOffsetForOther1 != null && bestOffsetForOther2 == null) {
                bestIndex = bestOffsetForOther1.getInt("targetIndex");
            } else if (bestOffsetForOther1 == null && bestOffsetForOther2 != null) {
                bestIndex = bestOffsetForOther2.getInt("targetIndex");
            } else {
                int bestIndexForOther1 = bestOffsetForOther1.getInt("targetIndex");
                int bestIndexForOther2 = bestOffsetForOther2.getInt("targetIndex");
                if (bestIndexForOther1 == bestIndexForOther2) {
                    bestIndex = bestIndexForOther1;
                } else {
                    int eojeolOffsetForOther1 = bestOffsetForOther1.getInt("eojeolOffset");
                    int eojeolOffsetForOther2 = bestOffsetForOther2.getInt("eojeolOffset");
                    if (eojeolOffsetForOther1 <= eojeolOffsetForOther2 || eojeolOffsetForOther1 < 3 || Math.abs(eojeolOffsetForOther1 - eojeolOffsetForOther2) < 3) {
                        bestIndex = bestIndexForOther1;
                    } else {
                        bestIndex = bestIndexForOther2;
                    }
                }
            }

            bestMorphemeIndex = notClearedMorphemeIndices.getJSONArray(bestIndex);
            targetIndices.put(notClearedIndex, bestMorphemeIndex);
            targetNotClearedIndices.remove(i);
        }
    }

    public static boolean removeAmbiguityOfIndex(JSONArray targetIndices, JSONArray attrIndices, JSONArray sentimentEojeolIndices) throws JSONException {
        JSONArray targetNotClearedIndices = simpleAmbiguityModificationOfEojeolIndex(targetIndices);
        JSONArray attrNotClearedIndices = simpleAmbiguityModificationOfEojeolIndex(attrIndices);
        JSONArray sentimentEojeolNotClearedIndices = simpleAmbiguityModificationOfEojeolIndex(sentimentEojeolIndices);

        modifyAmbiguityWithOtherSections(attrIndices, sentimentEojeolIndices, targetIndices, attrNotClearedIndices);
        modifyAmbiguityWithOtherSections(sentimentEojeolIndices, attrIndices, targetIndices, sentimentEojeolNotClearedIndices);
        modifyAmbiguityWithOtherSections(targetIndices, attrIndices, sentimentEojeolIndices, targetNotClearedIndices);

        return ((targetNotClearedIndices.length() + attrNotClearedIndices.length() + sentimentEojeolNotClearedIndices.length()) == 0);
    }

    public static JSONArray morphemeIndicesFromEojeolIndices(JSONArray targetEojeolIndices, JSONArray morphemes) throws JSONException {
        JSONArray morphemeIndices = new JSONArray();
        for (int i = 0; i < targetEojeolIndices.length(); i++) {
            int targetEojeolIndex = targetEojeolIndices.getJSONArray(i).getInt(0);
            JSONArray morphemesForEojeolIndex = morphemes.getJSONArray(targetEojeolIndex);
            for (int j = 0; j < morphemesForEojeolIndex.length(); j++) {
                morphemeIndices.put(new JSONArray(new int[]{targetEojeolIndex, j}));
            }
        }

        return morphemeIndices;
    }

    public static JSONArray eojeolsFromMorphemes(JSONArray targetMorphemesIndices, JSONArray eojeols) throws JSONException {
        JSONArray eojeolsFromMorphemes = new JSONArray();
        JSONArray eojeolsIndices = new JSONArray();
        for (int i = 0; i < targetMorphemesIndices.length(); i++) {
            int targetEojeolIndex = targetMorphemesIndices.getJSONArray(i).getInt(0);
            boolean addNewEojeol = true;
            for (int j = 0; j < eojeolsIndices.length(); j++) {
                if (eojeolsIndices.getInt(j) == targetEojeolIndex) {
                    addNewEojeol = false;
                }
            }
            if (addNewEojeol) {
                eojeolsIndices.put(targetEojeolIndex);
                eojeolsFromMorphemes.put(eojeols.getString(targetEojeolIndex));
            }
        }

        return eojeolsFromMorphemes;
    }

    public static JSONArray tagsFromMorphemes(JSONArray targetMorphemesIndices, JSONArray tags) throws JSONException {
        JSONArray tagsFromMorphemes = new JSONArray();
        for (int i = 0; i < targetMorphemesIndices.length(); i++) {
            int targetEojeolIndex = targetMorphemesIndices.getJSONArray(i).getInt(0);
            if (targetMorphemesIndices.getJSONArray(i).length() > 1) {
                int targetMorphemeIndex = targetMorphemesIndices.getJSONArray(i).getInt(1);
                tagsFromMorphemes.put(tags.getJSONArray(targetEojeolIndex).getString(targetMorphemeIndex));
            } else {
                JSONArray tagsForEojeolIndex = tags.getJSONArray(targetEojeolIndex);
                for (int j = 0; j < tagsForEojeolIndex.length(); j++) {
                    tagsFromMorphemes.put(tagsForEojeolIndex.getString(j));
                }
            }
        }

        return tagsFromMorphemes;
    }

    public static JSONArray morphemesFromEojeols(JSONArray targetEojeolsIndices, JSONArray morphemes) throws JSONException {
        JSONArray morphemesFromEojeol = new JSONArray();
        JSONArray eojeolIndices = new JSONArray();
        for (int i = 0; i < targetEojeolsIndices.length(); i++) {
            int targetEojeolIndex = targetEojeolsIndices.getJSONArray(i).getInt(0);
            boolean addNewEojeol = true;
            for (int j = 0; j < eojeolIndices.length(); j++) {
                if (eojeolIndices.getInt(j) == targetEojeolIndex) {
                    addNewEojeol = false;
                }
            }
            if (addNewEojeol) {
                eojeolIndices.put(targetEojeolIndex);
                JSONArray morphemesForEojeolIndex = morphemes.getJSONArray(targetEojeolIndex);
                for (int j = 0; j < morphemesForEojeolIndex.length(); j++) {
                    morphemesFromEojeol.put(morphemesForEojeolIndex.getString(j));
                }
            }
        }

        return morphemesFromEojeol;
    }

    public static void updateTableWithRule(JSONObject rule) throws Exception {
        if (rt == null) {
            rt = new RuleTable();
            rt.connect();
        }
        String query = "UPDATE rule SET " +
                "target_eojeol = \"" + rule.getString("target_eojeol") + "\", attr_eojeol =\"" + rule.getString("attr_eojeol") + "\", sentiment = \"" + rule.getString("sentiment") +
                "\", target_tag = \"" + rule.getString("target_tag") + "\", attr_tag = \"" + rule.getString("attr_tag") + "\", sentiment_tag = \"" + rule.getString("sentiment_tag") +
                "\", target_index = \"" + rule.getString("target_index") + "\", attr_index = \"" + rule.getString("attr_index") + "\", sentiment_index = \"" + rule.getString("sentiment_index") +
                "\", sentence = \"" + rule.getString("sentence") +
                "\" WHERE id = " + rule.getInt("id");
        rt.executeUpdate(query);
    }

    public static boolean fillMissingFields(JSONObject rule, JSONObject analyzedResult) throws Exception {
        JSONArray target = rule.getJSONArray("parsed_target");
        JSONArray attr = rule.getJSONArray("parsed_attr");
        JSONArray sentiment_eojeol = rule.getJSONArray("parsed_sentiment_eojeol");
        JSONArray morphemes = analyzedResult.getJSONArray("morphemes");
        JSONArray eojeols = analyzedResult.getJSONArray("eojeol");
        JSONArray tags = analyzedResult.getJSONArray("tags");

        JSONArray target_indices = JSONUtil.indicesOfElement(target, morphemes);
        JSONArray attr_indices = JSONUtil.indicesOfElement(attr, morphemes);
        JSONArray sentiment_eojeol_indices = JSONUtil.indicesOfElement(sentiment_eojeol, eojeols);

        if (!removeAmbiguityOfIndex(target_indices, attr_indices, sentiment_eojeol_indices)) {
//            System.out.println("rule id : " + rule.getInt("id"));
//            System.out.println("target : \t" + target + " : " + target_indices + "\tattr : \t" + attr + " : " + attr_indices + "\tsentiment_eojeol : \t" + sentiment_eojeol + " : " + sentiment_eojeol_indices);
//            System.out.println("morphemes : \t" + morphemes + "\teojeols : \t" + eojeols);
//            System.out.println("\n");
            totalNumberOfError++;
            return false;
        } else {
            JSONArray target_eojeol = eojeolsFromMorphemes(target_indices, eojeols);
            JSONArray attr_eojeol = eojeolsFromMorphemes(attr_indices, eojeols);
            JSONArray sentiment = morphemesFromEojeols(sentiment_eojeol_indices, morphemes);
            JSONArray target_tag = tagsFromMorphemes(target_indices, tags);
            JSONArray attr_tag = tagsFromMorphemes(attr_indices, tags);
            JSONArray sentiment_tag = tagsFromMorphemes(sentiment_eojeol_indices, tags);
            JSONArray sentiment_indices = morphemeIndicesFromEojeolIndices(sentiment_eojeol_indices, morphemes);
            rule.put("target_index", target_indices.toString().replaceAll("\"", ""));
            rule.put("attr_index", attr_indices.toString().replaceAll("\"", ""));
            rule.put("sentiment_index", sentiment_indices.toString().replaceAll("\"", ""));
            rule.put("target_eojeol", target_eojeol.toString().replaceAll("\"", ""));
            rule.put("attr_eojeol", attr_eojeol.toString().replaceAll("\"", ""));
            rule.put("sentiment", sentiment.toString().replaceAll("\"", ""));
            rule.put("target_tag", target_tag.toString().replaceAll("\"", ""));
            rule.put("attr_tag", attr_tag.toString().replaceAll("\"", ""));
            rule.put("sentiment_tag", sentiment_tag.toString().replaceAll("\"", ""));
            updateTableWithRule(rule);
            return true;
        }
    }

    public static void main(String[] args) throws Exception {
        try {
            SentenceConverter sc = new SentenceConverter();
            Workflow workflow = WorkflowFactory.getPredefinedWorkflow(WorkflowFactory.WORKFLOW_POS_SIMPLE_22);
            workflow.activateWorkflow(true);

            RuleTable rt = new RuleTable();
            CrawlingTable ct = new CrawlingTable();
            rt.connect();
            ct.setConnect(rt.getConnect());
            Integer[] uids = rt.readReviewIds("sentiment is null");
            Crawling[] crawlings = ct.readCrawlingTable("uid in (" + ArrayUtil.listStringWithoutParenthesis(uids) + ")");
            JSONObject rules = rt.ruleTableResultSetJSONObject("sentiment is null");

            for (int i = 0; i < crawlings.length; i++) {
                String filePath = crawlings[i].getPath();
                int reviewId = uids[i];
//                System.out.println("\n" + reviewId + filePath);

                if (FileIO.getExtension(filePath).equals("json")) {
                    String jsonFile = FileIO.readFile(filePath);
                    JSONTokener tokener = new JSONTokener(jsonFile);
                    JSONObject object = new JSONObject(tokener);
                    String content = (String)object.get("content");

                    workflow.analyze(content);

                    JSONObject targetRules = targetRules(i, uids, rules);

                    int sentenceID = 0;
                    while (true) {
                        sentenceID++;
                        Sentence s = workflow.getResultOfSentence(new Sentence(0, 0, false));
                        JSONObject resultObject = sc.convertSentenceToJSON(null, null, s, SentenceConverter.ONLY_PLAIN_SENTENCE);
                        resultObject = sc.convertSentenceToJSON(resultObject, null, s, SentenceConverter.ONLY_MORPHEME);
                        resultObject = sc.convertSentenceToJSON(resultObject, null, s, SentenceConverter.ONLY_TAG);
                        resultObject = sc.convertSentenceToJSON(resultObject, null, s, SentenceConverter.ONLY_PLAIN_EOJEOL);
                        String sentenceIDString = String.valueOf(sentenceID);
                        if (targetRules.has(sentenceIDString)) {
                            JSONArray targetSentenceRules = targetRules.getJSONArray(sentenceIDString);
                            for (int j = 0; j < targetSentenceRules.length(); j++) {
                                JSONObject targetSentenceRule = targetSentenceRules.getJSONObject(j);
                                targetSentenceRule.put("sentence", resultObject.getString("sentence"));
                                if (isRightRule(targetSentenceRule, resultObject)) {
                                    if (fillMissingFields(targetSentenceRule, resultObject));
                                } else {
//                                    System.out.println(reviewId + " - " + sentenceID);
//                                    System.out.println(targetSentenceRule + "\n" + resultObject);
                                }
                            }
                        }
                        if (s.isEndOfDocument()) {
                            break;
                        }
                    }
                }
            }
            rt.close();
            workflow.close();
//            System.out.println(totalNumberOfError);
        } catch ( Exception e ) {
            throw e;
        }
    }
}
