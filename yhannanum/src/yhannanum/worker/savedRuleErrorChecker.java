package yhannanum.worker;

// TO DO : Renames as easy to understand

import java.io.File;
import java.lang.Exception;
import java.util.HashMap;

import yhannanum.comm.Crawling;
import yhannanum.db.CrawlingTable;
import yhannanum.db.RuleTable;
import yhannanum.db.SubmitTable;
import yhannanum.share.ArrayUtil;
import yhannanum.share.Constants;
import yhannanum.share.FileIO;

import org.json.JSONTokener;
import org.json.JSONArray;
import org.json.JSONObject;

public class savedRuleErrorChecker {
    private JSONObject ruleTableResultSet = null;
    private Crawling[] crawlings = null;
    private Integer[] reviewIDList = null;
    private String[] fileNameList = null;
    private HashMap<Integer, String> userForReviewID = null;
    private JSONObject errorDetailsJSONObject = null;
    private RuleTable rt = null;
    private CrawlingTable ct = null;
    private SubmitTable st = null;

    savedRuleErrorChecker() throws Exception {
        rt = new RuleTable();
        ct = new CrawlingTable();
        st = new SubmitTable();
    }

    private void dbConnect() throws Exception {
        rt.connect();
        ct.setConnect(rt.getConnect());
        st.setConnect(rt.getConnect());
    }

    private void dbClose() throws Exception {
        st.close();
        ct.close();
        rt.close();
    }

    public void readData() throws Exception {
        dbConnect();
        if (rt != null) {
            ruleTableResultSet = rt.ruleTableResultSetJSONObject(" review_id > 0 ");
            reviewIDList = rt.readReviewIds();
        }
        if (ct != null && reviewIDList != null) {
            crawlings = ct.readCrawlingTable("uid in (" + ArrayUtil.listStringWithoutParenthesis(reviewIDList) + ")");
            fileNameList = new String[crawlings.length];
            for (int i = 0; i < crawlings.length; i++) {
                fileNameList[i] = FileIO.extractNameFromPath(crawlings[i].getPath());
            }
        }
        if (st != null) {
            userForReviewID = st.readReviewIdUserDictionary();
        }
        dbClose();
    }

    private JSONObject resultOfCheckJSONFileError(String fileName, String filePath, int reviewID, String content, JSONArray ruleJSONArray, JSONObject resultOfCheckJSONFileError) throws Exception {
        JSONObject resultOfReview = new JSONObject();
        resultOfReview.put("file_name", fileName);
        resultOfReview.put("file_path", filePath);
        resultOfReview.put("review_id", reviewID);
        resultOfReview.put("content", content);
        resultOfReview.put("result", ruleJSONArray);

        String userName = userForReviewID.get(reviewID);
        if (userName == null) {
            userName = String.valueOf(reviewID);
        }


        if (!resultOfCheckJSONFileError.has(userName)) {
            resultOfCheckJSONFileError.put(userName, new JSONObject());
        }

        JSONObject reviewResultOfCheckJSONFileError = resultOfCheckJSONFileError.getJSONObject(userName);
        reviewResultOfCheckJSONFileError.put(String.valueOf(reviewID), resultOfReview);

        return resultOfCheckJSONFileError;
    }

    public JSONObject checkError() throws Exception {
        if (ruleTableResultSet == null || fileNameList == null || userForReviewID == null) return null;
        errorDetailsJSONObject = new JSONObject();
        File readDirectory = new File(Constants.RULE_ERROR_CHECK_SOURCE_DIRECTORY_PATH);
//        System.out.println("number of files : " + fileNameList.length());
//        System.out.println("number of rules : " + ruleTableResultSet.getInt("numberOfRows"));
        JSONObject resultOfCheckJSONFileError = new JSONObject();
        JSONObject resultOfCheckJSONFileErrorIncludingRight = new JSONObject();

        for (int i = 0; i < fileNameList.length; i++) {
            String fileName = fileNameList[i];
            String filePath = readDirectory.getPath() + "/" + fileName;
            int reviewID = reviewIDList[i];
            if (FileIO.getExtension(filePath).equals("json")) {
                String jsonString = FileIO.readFile(filePath);
                JSONTokener tokener = new JSONTokener(jsonString);
                JSONObject jsonObject = new JSONObject(tokener);
                String content = jsonObject.getString("content");
                JSONArray morphemeAnalyzeJSONArray = jsonObject.getJSONArray("morphemeAnalyze");
                JSONArray targetRuleJSONArray = ruleErrorsInReview(morphemeAnalyzeJSONArray, targetRuleJSONArray(i), true);
                JSONArray targetRuleJSONArrayIncludingRight = ruleErrorsInReview(morphemeAnalyzeJSONArray, targetRuleJSONArray(i), false);

                if (targetRuleJSONArray.length() > 0) {
                    resultOfCheckJSONFileError = resultOfCheckJSONFileError(fileName, filePath, reviewID, content, targetRuleJSONArray, resultOfCheckJSONFileError);
                }
                if (targetRuleJSONArrayIncludingRight.length() > 0) {
                    resultOfCheckJSONFileErrorIncludingRight = resultOfCheckJSONFileError(fileName, filePath, reviewID, content, targetRuleJSONArrayIncludingRight, resultOfCheckJSONFileErrorIncludingRight);
                }
            }
        }

        JSONObject results = new JSONObject();
        results.put("only_error", resultOfCheckJSONFileError);
        results.put("all", resultOfCheckJSONFileErrorIncludingRight);
        return results;
    }

    private JSONArray targetRuleJSONArray(int indexOfFile) throws Exception {
        JSONArray targetRuleJSONArray = new JSONArray();
        int reviewID = reviewIDList[indexOfFile];
        JSONArray idJSONArray = ruleTableResultSet.getJSONArray("id");
        JSONArray review_idJSONArray = ruleTableResultSet.getJSONArray("review_id");
        JSONArray sentence_idJSONArray = ruleTableResultSet.getJSONArray("sentence_id");
        JSONArray sentenceJSONArray = ruleTableResultSet.getJSONArray("sentence");
        JSONArray origin_attrJSONArray = ruleTableResultSet.getJSONArray("origin_attr");
        JSONArray targetJSONArray = ruleTableResultSet.getJSONArray("target");
        JSONArray attrJSONArray = ruleTableResultSet.getJSONArray("attr");
        JSONArray sentiment_eojeolJSONArray = ruleTableResultSet.getJSONArray("sentiment_eojeol");
        JSONArray sentiment_typeJSONArray = ruleTableResultSet.getJSONArray("sentiment_type");
        JSONArray parsed_targetJSONArray = ruleTableResultSet.getJSONArray("parsed_target");
        JSONArray parsed_attrJSONArray = ruleTableResultSet.getJSONArray("parsed_attr");
        JSONArray parsed_sentiment_eojeolJSONArray = ruleTableResultSet.getJSONArray("parsed_sentiment_eojeol");


        for (int i = 0; i < review_idJSONArray.length(); i++) {
            int ruleReviewID = review_idJSONArray.getInt(i);
            if (ruleReviewID == reviewID) {
                JSONObject targetRule = new JSONObject();
                targetRule.put("id", idJSONArray.getInt(i));
                targetRule.put("review_id", review_idJSONArray.getInt(i));
                targetRule.put("sentence_id", sentence_idJSONArray.getInt(i));
                targetRule.put("sentence", sentenceJSONArray.getString(i));
                targetRule.put("origin_attr", origin_attrJSONArray.getString(i));
                targetRule.put("target", targetJSONArray.getString(i));
                targetRule.put("attr", attrJSONArray.getString(i));
                targetRule.put("sentiment_eojeol", sentiment_eojeolJSONArray.getString(i));
                targetRule.put("sentiment_type", sentiment_typeJSONArray.getInt(i));
                targetRule.put("parsed_target", parsed_targetJSONArray.getJSONArray(i));
                targetRule.put("parsed_attr", parsed_attrJSONArray.getJSONArray(i));
                targetRule.put("parsed_sentiment_eojeol", parsed_sentiment_eojeolJSONArray.getJSONArray(i));
                targetRuleJSONArray.put(targetRule);
            }
        }

        return targetRuleJSONArray;
    }

    private JSONObject stringContainsStatus(JSONArray targetJSONArray, JSONArray setJSONArray) throws Exception {
        int countOfContains = 0;
        JSONArray notFound = new JSONArray();
        for (int i = 0; i < targetJSONArray.length(); i++) {
            boolean contains = false;
            for (int j = 0; j < setJSONArray.length(); j++) {
                Object setClass = setJSONArray.get(j).getClass();
                if (JSONArray.class.equals(setClass)) {
                    JSONArray set = setJSONArray.getJSONArray(j);
                    for (int k = 0; k < set.length(); k++) {
                        if (targetJSONArray.getString(i).equals(set.getString(k))) {
                            countOfContains++;
                            contains = true;
                            break;
                        }
                    }
                } else {
                    String set = setJSONArray.getString(j);
                    if (targetJSONArray.getString(i).equals(set)) {
                        countOfContains++;
                        contains = true;
                        break;
                    }
                }
                if (contains) break;
            }
            if (!contains) {
                notFound.put(targetJSONArray.getString(i));
            }
        }

        JSONObject stringContainsStatus = new JSONObject();
        stringContainsStatus.put("length", targetJSONArray.length());
        stringContainsStatus.put("count_of_contains", countOfContains);
        stringContainsStatus.put("not_found_list", notFound);
        return stringContainsStatus;
    }

    private JSONObject comparedRuleErrorInSentence(JSONObject targetRuleJSONObject, JSONObject morphemeAnalyzeJSONObject) throws Exception {
        return comparedRuleErrorInSentence(targetRuleJSONObject, morphemeAnalyzeJSONObject, new JSONObject());
    }

    private JSONObject comparedRuleErrorInSentence(JSONObject savedRuleForSentence, JSONObject analyzedMorphemesForSentence, JSONObject resultObject) throws Exception {
        JSONArray rule_parsed_target = savedRuleForSentence.getJSONArray("parsed_target");
        JSONArray rule_parsed_attr = savedRuleForSentence.getJSONArray("parsed_attr");
        JSONArray rule_parsed_sentiment_eojeol = savedRuleForSentence.getJSONArray("parsed_sentiment_eojeol");

        JSONArray review_morpheme = analyzedMorphemesForSentence.getJSONArray("morphemes");
        JSONArray review_eojeol = analyzedMorphemesForSentence.getJSONArray("eojeol");

        JSONObject targetContainsStatus = stringContainsStatus(rule_parsed_target, review_morpheme);
        JSONObject attrContainsStatus = stringContainsStatus(rule_parsed_attr, review_morpheme);
        JSONObject sentimentEojeolContainsStatus = stringContainsStatus(rule_parsed_sentiment_eojeol, review_eojeol);

        resultObject.put("review_morpheme", review_morpheme);
        resultObject.put("review_eojeol", review_eojeol);

        resultObject.put("target_result", targetContainsStatus);
        resultObject.put("attr_result", attrContainsStatus);
        resultObject.put("sentiment_eojeol_result", sentimentEojeolContainsStatus);
        double accuracy = 0;
        if (targetContainsStatus.getInt("length") + attrContainsStatus.getInt("length") + sentimentEojeolContainsStatus.getInt("length") > 0) {
            accuracy = (double)(targetContainsStatus.getInt("count_of_contains") + attrContainsStatus.getInt("count_of_contains") + sentimentEojeolContainsStatus.getInt("count_of_contains")) /
                    ((targetContainsStatus.getInt("length") + attrContainsStatus.getInt("length") + sentimentEojeolContainsStatus.getInt("length")));
        }
        resultObject.put("accuracy", accuracy);

        return resultObject;
    }

    private JSONArray ruleErrorsInReview(JSONArray analyzedMorphemesForReview, JSONArray savedRulesForReview, boolean onlyError) throws Exception {
        for (int i = 0; i < savedRulesForReview.length(); i++) {
            JSONObject savedRuleForSentence = savedRulesForReview.getJSONObject(i);

            int sentence_id = savedRuleForSentence.getInt("sentence_id");
            int sentence_index = sentence_id - 1;
            JSONObject analyzedMorphemesForSentence;
            if (sentence_index >= analyzedMorphemesForReview.length()) {
                analyzedMorphemesForSentence = analyzedMorphemesForReview.getJSONObject(analyzedMorphemesForReview.length() - 1);
            } else {
                analyzedMorphemesForSentence = analyzedMorphemesForReview.getJSONObject(sentence_index);
            }
            comparedRuleErrorInSentence(savedRuleForSentence, analyzedMorphemesForSentence, savedRuleForSentence);
        }

        if (onlyError) {
            for (int i = 0; i < savedRulesForReview.length();) {
                JSONObject savedRuleForSentence = savedRulesForReview.getJSONObject(i);
                if (savedRuleForSentence.getDouble("accuracy") == 1) {
                    savedRulesForReview.remove(i);
                } else {
                    i++;
                }
            }
        }

        for (int i = 0; i < savedRulesForReview.length(); i++) {
            JSONObject targetRuleJSONObject = savedRulesForReview.getJSONObject(i);

            double accuracy = targetRuleJSONObject.getDouble("accuracy");

            int sentence_id = targetRuleJSONObject.getInt("sentence_id");
            int sentence_index = sentence_id - 1;

            for (int j = 0; j < analyzedMorphemesForReview.length(); j++) {
                if (j == sentence_index) continue;
                JSONObject morphemeAnalyzeJSONObject = analyzedMorphemesForReview.getJSONObject(j);

                JSONObject resultOfCompare = comparedRuleErrorInSentence(targetRuleJSONObject, morphemeAnalyzeJSONObject);

                double resultOfAccuracy = resultOfCompare.getDouble("accuracy");
                if (resultOfAccuracy > accuracy || resultOfAccuracy == accuracy && j == sentence_index) {
                    if (!targetRuleJSONObject.has("other_results")) {
                        targetRuleJSONObject.put("other_results", new JSONArray());
                    }
                    JSONArray otherResults = targetRuleJSONObject.getJSONArray("other_results");
                    resultOfCompare.put("sentence_id", j + 1);
                    resultOfCompare.put("sentence", morphemeAnalyzeJSONObject.getString("sentence"));
                    otherResults.put(resultOfCompare);
                }
            }

            if (targetRuleJSONObject.has("other_results")) {
                JSONArray otherResults = targetRuleJSONObject.getJSONArray("other_results");
                double maxAccuracy = 0;
                for (int j = 0; j < otherResults.length(); j++) {
                    double resultAccuracy = otherResults.getJSONObject(j).getDouble("accuracy");
                    if (maxAccuracy < resultAccuracy) maxAccuracy = resultAccuracy;
                }
                for (int j = 0; j < otherResults.length();) {
                    double resultAccuracy = otherResults.getJSONObject(j).getDouble("accuracy");
                    if (resultAccuracy < maxAccuracy) {
                        otherResults.remove(j);
                    } else {
                        j++;
                    }
                }
            }
        }
        return savedRulesForReview;
    }

    public void saveRuleErrorCheck(JSONObject resultObject) throws Exception {
        JSONObject onlyError = resultObject.getJSONObject("only_error");
        JSONObject all = resultObject.getJSONObject("all");

        if (onlyError.length() == 0 && all.length() == 0) return;
        File directory = FileIO.getDirectory(Constants.RULE_ERROR_CHECK_DIRECTORY_PATH);
        for (String fileName : directory.list()) {
            if (fileName.contains("savedRuleErrorChecking")) {
                File existFile = new File(directory.getPath() + "/" + fileName);
                existFile.delete();
            }
        }
        if (onlyError != null && onlyError.length() > 0) {
            for (String name : JSONObject.getNames(onlyError)) {
                FileIO.writeFile(directory.getPath() + "/" + "savedRuleErrorChecking_only_error_" + name + ".json", onlyError.getJSONObject(name).toString());
            }
        }
        if (all != null && all.length() > 0) {
            for (String name : JSONObject.getNames(all)) {
                FileIO.writeFile(directory.getPath() + "/" + "savedRuleErrorChecking_all_" + name + ".json", all.getJSONObject(name).toString());
            }
        }
    }

    public static void main(String[] args) throws Exception {
        try {
            savedRuleErrorChecker srec = new savedRuleErrorChecker();
            srec.readData();
            JSONObject result = srec.checkError();
            srec.saveRuleErrorCheck(result);
        } catch ( Exception e ) {
            throw e;
        }
    }
}