package yhannanum.worker;

import java.io.*;
import java.net.ConnectException;
import java.net.Socket;
import java.util.Arrays;

/**
 * Created by Arthur on 2014. 5. 25..
 */
public class ReviewSearchClient {
    public static void main(String[] args) throws Exception {
        String serverIP = "127.0.0.1";

        Socket socket = new Socket(serverIP, 5000);

        OutputStream out = socket.getOutputStream();
        DataOutputStream dos = new DataOutputStream(out);
        dos.writeUTF(args[0]);

        InputStream in = socket.getInputStream();
        DataInputStream dis = new DataInputStream(in);
        System.out.println(dis.readUTF());

        socket.close();
    }
}
