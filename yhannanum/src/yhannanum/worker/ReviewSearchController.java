package yhannanum.worker;

import kr.ac.kaist.swrc.jhannanum.comm.Sentence;
import kr.ac.kaist.swrc.jhannanum.hannanum.Workflow;
import kr.ac.kaist.swrc.jhannanum.hannanum.WorkflowFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import yhannanum.converter.SentenceConverter;
import yhannanum.share.Constants;
import yhannanum.share.DateUtil;
import yhannanum.share.FileIO;
import yhannanum.share.JSONUtil;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Arthur on 2014. 5. 15..
 */
public class ReviewSearchController {
    private HashMap<String, Integer> wordMap = new HashMap<String, Integer>();
    private Workflow workflow = WorkflowFactory.getPredefinedWorkflow(WorkflowFactory.WORKFLOW_POS_SIMPLE_22);
    private File readDirectory = new File(Constants.REVIEW_INDEX_DIRECTORY_PATH);
    private String reviewSentencesDirectoryPath = Constants.REVIEW_SUMMARY_DIRECTORY_PATH + "/review/search";

    public ReviewSearchController() throws Exception {
        wordMap = JSONUtil.makeJsonStringToHashMap(FileIO.readFile(Constants.REVIEW_INDEX_DIRECTORY_PATH + "/index.json"));
        workflow.activateWorkflow(true);
        File reviewSentencesDirectory = new File(reviewSentencesDirectoryPath);
        if (!reviewSentencesDirectory.exists()) {
            reviewSentencesDirectory.mkdir();
        }
    }

    private Integer[] readIdsForWord(String word) { return readIdsForWord(word, null); }

    private Integer[] readIdsForWord(String word, Integer reviewId) {
        if (!wordMap.containsKey(word)) return new Integer[0];
        File wordDirectory = new File(readDirectory.getPath() + "/" + wordMap.get(word) + "/" + word + (reviewId != null ? "/" + reviewId : ""));
        String[] idStrings = wordDirectory.list();
        ArrayList<Integer> ids = new ArrayList<Integer>();
        for (String idString : idStrings) {
            if (idString.equals(".") || idString.equals("..")) continue;
            ids.add(Integer.valueOf(idString));
        }
        return ids.toArray(new Integer[ids.size()]);
    }

    private Integer[] readEojeolIndices(String word, Integer reviewId, Integer sentenceId) throws IOException, JSONException {
        if (!wordMap.containsKey(word)) return new Integer[0];
        String eojeolIndexInfoString = FileIO.readFile(readDirectory.getPath() + "/" + wordMap.get(word) + "/" + word + "/" + reviewId + "/" + sentenceId);
        eojeolIndexInfoString = eojeolIndexInfoString.replaceAll("[\\[\\]]", "");
        String[] eojeolIndexStrings = eojeolIndexInfoString.split(",");
        Integer[] eojeolIndices = new Integer[eojeolIndexStrings.length];
        for (int i = 0; i < eojeolIndexStrings.length; i++) {
            eojeolIndices[i] = Integer.valueOf(eojeolIndexStrings[i]);
        }
        return eojeolIndices;
    }

    private void addIdsToHashMap(Integer[] Ids, HashMap<Integer, Integer> countForId,
                                 ArrayList<ArrayList<String>> idListForCount) {
        if (idListForCount.size() == 0) idListForCount.add(new ArrayList<String>());
        for (Integer id : Ids) {
            if (countForId.containsKey(id)) {
                ArrayList<String> beforeCountIdList = idListForCount.get(countForId.get(id));
                beforeCountIdList.remove(id.toString());
                countForId.put(id, countForId.get(id) + 1);
            } else {
                countForId.put(id, 1);
            }
            if (idListForCount.size() <= countForId.get(id)) {
                idListForCount.add(new ArrayList<String>());
            }
            ArrayList<String> afterCountIdList = idListForCount.get(countForId.get(id));
            afterCountIdList.add(id.toString());
        }
    }

    private boolean doesIntegerArrayHasValue(Integer[] array, Integer value) {
        for (Integer valueInArray : array) {
            if (valueInArray.intValue() == value.intValue()) return true;
        }
        return false;
    }

    private ArrayList<ArrayList<Integer>> duplicationIndicesCombination(ArrayList<ArrayList<Integer>> indicesCombination, int times) {
        if (times == 1) return indicesCombination;
        ArrayList<ArrayList<Integer>> newIndicesCombination = new ArrayList<ArrayList<Integer>>();
        for (int i = 0; i < times; i++) {
            for (ArrayList<Integer> indices : indicesCombination) {
                ArrayList<Integer> newIndices = new ArrayList<Integer>(indices);
                newIndicesCombination.add(newIndices);
            }
        }
        return newIndicesCombination;
    }

    private ArrayList<ArrayList<Integer>> makeIndicesCombinationWithArray(ArrayList<ArrayList<Integer>> indicesCombination,
                                                                          Integer[] newList) {
        if (indicesCombination.size() == 0) {
            for (int i = 0; i < newList.length; i++) {
                indicesCombination.add(new ArrayList<Integer>());
                indicesCombination.get(i).add(newList[i]);
            }
            return indicesCombination;
        }
        int NUMBER_OF_COMBINATION = indicesCombination.size();
        ArrayList<ArrayList<Integer>> newIndicesCombination = duplicationIndicesCombination(indicesCombination, newList.length);
        int indexOfCombination = 0;
        for (Integer index : newList) {
            for (int i = 0; i < NUMBER_OF_COMBINATION; i++) {
                newIndicesCombination.get(indexOfCombination++).add(index);
            }
        }
        return newIndicesCombination;
    }

    private boolean doesLeftHasMoreQualifiedResults(List<Integer[]> sentenceCounts1, List<Integer[]> sentenceCounts2) {
        int NUMBER_OF_SENTENCE_COUNT_1 = sentenceCounts1.size();
        int NUMBER_OF_SENTENCE_COUNT_2 = sentenceCounts2.size();
        int i = 0;
        int j = 0;
        while(i < NUMBER_OF_SENTENCE_COUNT_1 && j < NUMBER_OF_SENTENCE_COUNT_2) {
            Integer[] sentenceCount1 = sentenceCounts1.get(i);
            Integer[] sentenceCount2 = sentenceCounts2.get(j);
            if (sentenceCount1[0] > sentenceCount2[0]) {
                return true;
            } else if (sentenceCount1[0].intValue() == sentenceCount2[0].intValue()) {
                if (sentenceCount1[1] > sentenceCount2[1]) {
                    return true;
                } else if (sentenceCount1[1].intValue() == sentenceCount2[1].intValue()) {
                    i++;
                    j++;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }

        return (!(i >= NUMBER_OF_SENTENCE_COUNT_1 && j < NUMBER_OF_SENTENCE_COUNT_2));
    }

    private void setReviewIdsForWord(JSONArray words, ArrayList<Integer[]> idsForWords, ArrayList<ArrayList<String>> idListForWordCount) throws JSONException {
        HashMap<Integer, Integer> wordCountForId = new HashMap<Integer, Integer>();
        for (int i = 0; i < words.length(); i++) {
            Integer[] ids = readIdsForWord(words.getString(i));
            idsForWords.add(ids);
            addIdsToHashMap(ids, wordCountForId, idListForWordCount);
        }
    }

    private void setSentenceIdsForWord(JSONArray words, ArrayList<Integer[]> reviewIdsForWords, Integer reviewId,
                                       ArrayList<Integer[]> idsForWords, ArrayList<ArrayList<String>> idListForWordCount) throws JSONException {
        HashMap<Integer, Integer> wordCountForId = new HashMap<Integer, Integer>();
        for (int i = 0; i < words.length(); i++) {
            if (doesIntegerArrayHasValue(reviewIdsForWords.get(i), reviewId)) {
                Integer[] ids = readIdsForWord(words.getString(i), reviewId);
                idsForWords.add(ids);
                addIdsToHashMap(ids, wordCountForId, idListForWordCount);
            } else {
                idsForWords.add(new Integer[0]);
            }
        }
    }

    private void setEojeolIndicesForWord(JSONArray words, Integer reviewId, ArrayList<Integer[]> sentenceIdsForWords, Integer sentenceId,
                                         ArrayList<ArrayList<Integer>> eojeolIndicesCombination, ArrayList<Boolean> wordsContainedInSentence) throws JSONException, IOException {
        for (int i = 0; i < words.length(); i++) {
            if (doesIntegerArrayHasValue(sentenceIdsForWords.get(i), sentenceId)) {
                Integer[] indices = readEojeolIndices(words.getString(i), reviewId, sentenceId);
                eojeolIndicesCombination = makeIndicesCombinationWithArray(eojeolIndicesCombination, indices);
                wordsContainedInSentence.add(true);
            } else {
                wordsContainedInSentence.add(false);
            }
        }
    }

    private float getSmallestAverageDistance(ArrayList<ArrayList<Integer>> eojeolIndicesCombination) {
        float smallestAverageDistance = -1;
        for (ArrayList<Integer> eojeolIndices : eojeolIndicesCombination) {
            float average = 0.0f;

            if (eojeolIndices.size() >= 2) {
                int distances = 0;

                Collections.sort(eojeolIndices);

                for (int i = 0; i < eojeolIndices.size() - 1; i++) {
                    distances += eojeolIndices.get(i + 1) - eojeolIndices.get(i);
                }

                average = (float)distances / (eojeolIndices.size() - 1);
            }

            if (smallestAverageDistance < 0 || smallestAverageDistance > average) smallestAverageDistance = average;
        }
        return smallestAverageDistance;
    }

    private void searchForMorphemes(JSONArray morphemes, JSONArray tags) throws JSONException {
//        System.out.print(morphemes + " - " + tags);

//        ArrayList<ArrayList<String>> reviewIdListForEojeolCount = new ArrayList<ArrayList<String>>();
    }

    private void sortSentenceIdWithAvgDistance(String[] sentenceIds, Float[] avgDistances) {
        for (int k = 0; k < avgDistances.length; k++) {
            boolean swapped = false;
            for (int l = k + 1; l < avgDistances.length; l++) {
                if (avgDistances[k] > avgDistances[l]) {
                    Float tmpAvgDistance = avgDistances[k];
                    avgDistances[k] = avgDistances[l];
                    avgDistances[l] = tmpAvgDistance;
                    String tmpSentenceId = sentenceIds[k];
                    sentenceIds[k] = sentenceIds[l];
                    sentenceIds[l] = tmpSentenceId;
                    swapped = true;
                }
            }
            if (!swapped) break;
        }
    }

    private void sortReviewIdsWithSentenceCounts(String[] reviewIds, List<Integer[]>[] sentenceCounts, JSONObject[] reviewResults) {
        for (int i = 0; i < reviewIds.length; i++) {
            boolean swapped = false;
            for (int j = i + 1; j < reviewIds.length; j++) {
                if (!doesLeftHasMoreQualifiedResults(sentenceCounts[i], sentenceCounts[j])) {
                    String tmpReviewId = reviewIds[i];
                    reviewIds[i] = reviewIds[j];
                    reviewIds[j] = tmpReviewId;
                    List<Integer[]> tmpSentenceCount = sentenceCounts[i];
                    sentenceCounts[i] = sentenceCounts[j];
                    sentenceCounts[j] = tmpSentenceCount;
                    JSONObject tmpReviewResult = reviewResults[i];
                    reviewResults[i] = reviewResults[j];
                    reviewResults[j] = tmpReviewResult;
                    swapped = true;
                }
            }
            if (!swapped) break;
        }
    }

    private JSONObject getSentenceInfoForReview(Integer reviewId, ArrayList<String> sentencesInReview) throws Exception {
        String reviewString = FileIO.readFile(reviewSentencesDirectoryPath + "/" + reviewId + "_sentences.json");
        JSONObject originalReviewJsonObject = new JSONObject(reviewString);
        JSONArray originalReviewSentences = originalReviewJsonObject.getJSONArray("sentences");

        JSONArray selectedSentences = new JSONArray();
        for (int i = 0; i < (sentencesInReview.size() >= 5 ? 5 : sentencesInReview.size()); i++) {
            selectedSentences.put(originalReviewSentences.get(Integer.valueOf(sentencesInReview.get(i)) - 1));
        }
        String fileName = originalReviewJsonObject.getString("file_name");
        String crawledReviewString = FileIO.readFile(Constants.CRAWLER_JSON_DIRECTORY_PATH + "/" + fileName);
        JSONObject crawledReviewJsonObject = new JSONObject(crawledReviewString);

        JSONObject sentenceInfo = new JSONObject();
        sentenceInfo.put("sentences", selectedSentences);
        Date creationDate = null;
        if (crawledReviewJsonObject.has("date") && crawledReviewJsonObject.getString("date").length() > 0) {
            creationDate = DateUtil.dateFromString(crawledReviewJsonObject.getString("date"));
        } else {
            creationDate = new Date();
        }
        sentenceInfo.put("creation_time", DateUtil.reviewCreationDateString(creationDate));
        if (crawledReviewJsonObject.has("images")) {
            sentenceInfo.put("images", crawledReviewJsonObject.getJSONArray("images"));
        } else {
            sentenceInfo.put("images", "");
        }
        sentenceInfo.put("review_id", reviewId);
        sentenceInfo.put("title", crawledReviewJsonObject.getString("title"));
        sentenceInfo.put("url", crawledReviewJsonObject.getString("url"));

        return sentenceInfo;
    }

    @SuppressWarnings("unchecked")
    private String searchForEojeols(JSONArray eojeols) throws Exception {

        ArrayList<ArrayList<String>> reviewIdListForEojeolCount = new ArrayList<ArrayList<String>>();
        ArrayList<Integer[]> reviewIdsForEojeols = new ArrayList<Integer[]>();
        setReviewIdsForWord(eojeols, reviewIdsForEojeols, reviewIdListForEojeolCount);

        int total_count = 0;

        JSONArray searchResults = new JSONArray();

        // for [1 : [reviewId, ...], ...]
        for (int i = reviewIdListForEojeolCount.size() - 1; i > 0; i--) {
            ArrayList<String> reviewList = reviewIdListForEojeolCount.get(i);
            List<Integer[]>[] sentenceCountsForEojeolCount = new ArrayList[reviewList.size()];
            JSONObject[] resultReviewInfoList = new JSONObject[reviewList.size()];
            for (int j = 0; j < reviewList.size(); j++) {
                String reviewIdString = reviewList.get(j);
                Integer reviewId = Integer.valueOf(reviewIdString);

                ArrayList<ArrayList<String>> sentenceIdListForEojeolCount = new ArrayList<ArrayList<String>>();
                ArrayList<Integer[]> sentenceIdsForEojeols = new ArrayList<Integer[]>();
                setSentenceIdsForWord(eojeols, reviewIdsForEojeols, reviewId, sentenceIdsForEojeols, sentenceIdListForEojeolCount);

                ArrayList<Integer[]> sentenceCountForEojeolCount = new ArrayList<Integer[]>();
                ArrayList<Float> distanceValuesInReview = new ArrayList<Float>();
                ArrayList<String> sentencesInReview = new ArrayList<String>();
                // for [1 : [sentenceId, ...], ...] -> [smallest_avg_dist, ...]
                for (int k = sentenceIdListForEojeolCount.size() - 1; k > 0; k--) {
                    ArrayList<String> sentenceList = sentenceIdListForEojeolCount.get(k);

                    ArrayList<Float> smallestAvgDistanceForSentence = new ArrayList<Float>();
                    for (String sentenceIdString : sentenceList) {
                        Integer sentenceId = Integer.valueOf(sentenceIdString);

                        // -> count, average_distance, [contained, not contained, ...]
                        ArrayList<ArrayList<Integer>> eojeolIndicesCombination = new ArrayList<ArrayList<Integer>>();
                        ArrayList<Boolean> eojeolsContainedInSentence = new ArrayList<Boolean>();
                        setEojeolIndicesForWord(eojeols, reviewId, sentenceIdsForEojeols, sentenceId, eojeolIndicesCombination, eojeolsContainedInSentence);

                        float smallestAverageDistance = getSmallestAverageDistance(eojeolIndicesCombination);
                        smallestAvgDistanceForSentence.add(smallestAverageDistance);
                    }

                    // sort by avg_distance
                    String[] sentenceIds = sentenceList.toArray(new String[sentenceList.size()]);
                    Float[] avgDistances = smallestAvgDistanceForSentence.toArray(new Float[smallestAvgDistanceForSentence.size()]);
                    sortSentenceIdWithAvgDistance(sentenceIds, avgDistances);

                    sentencesInReview.addAll(Arrays.asList(sentenceIds));
                    distanceValuesInReview.addAll(Arrays.asList(avgDistances));
                    sentenceCountForEojeolCount.add(new Integer[]{k, sentenceIds.length});
                }
                sentenceCountsForEojeolCount[j] = sentenceCountForEojeolCount;
                resultReviewInfoList[j] = getSentenceInfoForReview(reviewId, sentencesInReview);
            }

            String[] reviewIds = reviewList.toArray(reviewList.toArray(new String[reviewList.size()]));
            sortReviewIdsWithSentenceCounts(reviewIds, sentenceCountsForEojeolCount, resultReviewInfoList);
            total_count += reviewIds.length;

            int lastIndexOfSource = (total_count >= 25 ? reviewIds.length - (total_count - 25) : reviewIds.length);
            for (int j = 0; j < lastIndexOfSource; j++) {
                searchResults.put(resultReviewInfoList[j]);
            }

            if (total_count >= 25) break;
            // Queue처럼 현재는 1단계에서 25개가 넘은경우 검사 안하도록 진행.
            // But if, 어절, 형태소, substring의 조합 intersaction에서 25개가 안넘을 경우 다른 부분도 진행해야 함.
        }

        return searchResults.toString();
    }

    public String search(String searchString) throws Exception {
        if (!workflow.isAnalyzable(searchString)) return "";
        workflow.analyze(searchString);

        JSONArray morphemes = null;
        JSONArray tags = null;
        JSONArray eojeols = null;
        while (true) {
            Sentence s = workflow.getResultOfSentence(new Sentence(0, 0, false));

            SentenceConverter sentenceConverter = new SentenceConverter();
            morphemes = JSONUtil.mergeJsonArrays(morphemes, sentenceConverter.convertSentenceToJSON(null, null, s, SentenceConverter.ONLY_MORPHEME).getJSONArray("morphemes"));
            tags = JSONUtil.mergeJsonArrays(tags, sentenceConverter.convertSentenceToJSON(null, null, s, SentenceConverter.ONLY_TAG).getJSONArray("tags"));
            eojeols = JSONUtil.mergeJsonArrays(eojeols, sentenceConverter.convertSentenceToJSON(null, null, s, SentenceConverter.ONLY_PLAIN_EOJEOL).getJSONArray("eojeol"));

            if (s.isEndOfDocument()) {
                break;
            }
        }

        return searchForEojeols(eojeols);
//        searchForMorphemes(morphemes, tags);
    }

    public void close() {
        workflow.close();
    }

    public static void main(String[] args) throws Exception {
        ServerSocket serverSocket = serverSocket = new ServerSocket(5000);
        ReviewSearchController reviewSearchController = new ReviewSearchController();
        try {
            while(true) {
                Socket socket = serverSocket.accept();

                InputStream in = socket.getInputStream();
                DataInputStream dis = new DataInputStream(in);

                String inputString = dis.readUTF();
                System.out.println(inputString);

                String results = reviewSearchController.search(inputString);

                OutputStream out = socket.getOutputStream();
                DataOutputStream dos = new DataOutputStream(out);
                dos.writeUTF(results);

                socket.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            reviewSearchController.close();
        }
    }
}
