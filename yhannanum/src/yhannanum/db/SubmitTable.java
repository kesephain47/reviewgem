package yhannanum.db;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import yhannanum.comm.Submit;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SubmitTable extends MysqlConnection {
    public Submit[] readSubmitTable() throws SQLException, JSONException { return readSubmitTable(null, null); }

    public Submit[] readSubmitTable(String withWhere) throws SQLException, JSONException { return readSubmitTable(withWhere, null); }

    public Submit[] readSubmitTable(String withWhere, String withGroupBy) throws SQLException, JSONException {
        String query = "select * from submit";
        if (withWhere != null) query += " where (" + withWhere + ")";
        if (withGroupBy != null) query += " group by (" + withGroupBy + ")";
        executeQuery(query);
        JSONArray rows = getResultRows();
        Submit[] submits = new Submit[rows.length()];
        for (int i = 0; i < rows.length(); i++) {
            JSONObject row = rows.getJSONObject(i);
            submits[i] = new Submit(row);
        }
        return submits;
    }

    public int readMaxSubmitedReviewId() throws SQLException, JSONException {
        executeQuery("select MAX(crawling_uid) as max_review_id from submit");
        return getResultRows().getJSONObject(0).getInt("max_review_id");
    }

    /**
     * Read 'review_id' and corresponding 'user'.
     * In submit table, 'User' submit review which has 'review_id' as id field.
     * This returns 'review_id' - 'user' pair as HashMap
     *
     * @return 'review_id' - 'user' pair of HashMap
     * @throws SQLException
     * @throws JSONException
     */
    public HashMap<Integer, String> readReviewIdUserDictionary() throws SQLException, JSONException {
        try {
            executeQuery("select user, crawling_uid from submit");
            JSONArray resultRows = getResultRows();

            HashMap<Integer, String> reviewIdUserPair = new HashMap<Integer, String>();
            for (int i = 0; i < resultRows.length(); i++) {
                JSONObject row = resultRows.getJSONObject(i);
                Integer crawling_uid = row.getInt("crawling_uid");
                String user = row.getString("user");
                reviewIdUserPair.put(crawling_uid, user);
            }
            return reviewIdUserPair;
        } catch (SQLException e) {
            throw e;
        } catch (JSONException e) {
            throw e;
        }
    }

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        SubmitTable st = new SubmitTable();
        st.connect();
        st.close();
    }
}