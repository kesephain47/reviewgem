package yhannanum.db;

import org.json.JSONArray;
import org.json.JSONException;

import java.sql.SQLException;

/**
 * Created by Arthur on 2014. 6. 11..
 */
public class ProsConsTable extends MysqlConnection {
    private final String TABLE_NAME_PROS = "soma_2nd_pros";
    private final String TABLE_NAME_CONS = "soma_2nd_cons";

    @Override
    public void connect() throws ClassNotFoundException, SQLException {
        super.connect("ReviewGem");
    }

    public JSONArray readProsConsTable(boolean isPros, String withWhere) throws SQLException, JSONException {
        String query = "select * from ";
        if (isPros) query += TABLE_NAME_PROS;
        else query += TABLE_NAME_CONS;
        if (withWhere != null) query += " where (" + withWhere + ")";
        executeQuery(query);
        return getResultRows();
    }

    public int getProductIdCount(boolean isPros, String withWhere) throws SQLException, JSONException {
        JSONArray rows = readProsConsTable(isPros, withWhere);
        return rows.length();
    }

    public void insertProsConsSentences(boolean isPros, int productId, JSONArray sentences) throws SQLException, JSONException {
        if (sentences == null) return;
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("insert into ");
        if (isPros) queryBuilder.append(TABLE_NAME_PROS);
        else queryBuilder.append(TABLE_NAME_CONS);
        queryBuilder.append("(product_id, option1, option2, option3, option4) values(").append(productId);
        int lastIndex = (sentences.length() >= 4 ? 4 : sentences.length());
        for (int i = 0; i < lastIndex; i++) {
            queryBuilder.append(", \"").append(sentences.getString(i)).append("\"");
        }
        for (int i = lastIndex; i < 4; i++) {
            queryBuilder.append(", \"\"");
        }
        queryBuilder.append(")");

        executeUpdate(queryBuilder.toString());
    }

    public void updateProsConsSentences(boolean isPros, int productId, JSONArray sentences) throws SQLException, JSONException {
        if (sentences == null) return;
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("update ");
        if (isPros) queryBuilder.append(TABLE_NAME_PROS);
        else queryBuilder.append(TABLE_NAME_CONS);
        queryBuilder.append(" set ");
        int lastIndex = (sentences.length() >= 4 ? 4 : sentences.length());
        for (int i = 0; i < lastIndex; i++) {
            if (i != 0) queryBuilder.append(", ");
            queryBuilder.append("option").append(i + 1).append("=").append("\"").append(sentences.getString(i)).append("\"");
        }
        for (int i = lastIndex; i < 4; i++) {
            if (i != 0) queryBuilder.append(", ");
            queryBuilder.append("option").append(i + 1).append("=").append("\"\"");
        }
        queryBuilder.append(" where product_id = ").append(productId);

        executeUpdate(queryBuilder.toString());
    }

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        ProsConsTable prosConsTable = new ProsConsTable();
        prosConsTable.connect();
        prosConsTable.close();
    }
}