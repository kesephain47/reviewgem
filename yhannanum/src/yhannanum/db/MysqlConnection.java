package yhannanum.db;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

// TO DO : throw exception messages to Logger

public class MysqlConnection {
    private Connection connect = null;
    protected Statement statement = null;
    private PreparedStatement preparedStatement = null;
    protected ResultSet resultSet = null;
    protected String currentQuery = null;

    /**
     * Set MysqlConnection connect object as parameter
     *
     * @param connect - connect object to replace
     * @throws SQLException
     */
    public void setConnect(Connection connect) throws SQLException {
        try {
            this.connect = connect;
            if (statement != null) statement.close();
            statement = connect.createStatement();
        } catch (SQLException e) {
            throw e;
        }
    }

    /**
     * return MysqlConnection's connect object
     *
     * @return connect object
     */
    public Connection getConnect() {
        return connect;
    }

    /**
     * Connect new Connection of MysqlConnection Object
     *
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public void connect() throws SQLException, ClassNotFoundException {
        connect(null);
    }

    public void connect(String databaseName) throws SQLException, ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String database = "jdbc:mysql://58.229.6.137/";
            if (databaseName == null) {
                database += "soma_demo";
            } else {
                database += databaseName;
            }

            connect = DriverManager.getConnection(database, "root", "qwer1234");
            statement = connect.createStatement();
        } catch (SQLException e) {
            throw e;
        } catch (ClassNotFoundException e) {
            throw e;
        }
    }

    /**
     * Close all connections related to MysqlConnection
     *
     * @throws SQLException
     */
    public void close() throws SQLException {
        try {
            if (resultSet != null) resultSet.close();
            if (statement != null) statement.close();
            if (connect != null) connect.close();
        } catch (SQLException e) {
            throw e;
        }
    }

    /**
     * ExecuteQuery. Query should be select.
     * If you want to Update or Delete, reference executeUpdate(String query) method.
     * When executeQuery, if the query is already executed, do not execute same query.
     * Save the query as currentQuery to avoid execute same query twice.
     *
     * @param query - query that should be executed
     * @throws SQLException
     */
    public void executeQuery(String query) throws SQLException {
        try {
            if (currentQuery == null || !currentQuery.equals(query)) {
                currentQuery = query;
                resultSet = statement.executeQuery(currentQuery);
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    /**
     * Update query. If you want to select from table, reference executeQuery(String query)
     *
     * @param query - query that should be executed
     * @return Number of rows affected by the query
     * @throws SQLException
     */
    public int executeUpdate(String query) throws SQLException {
        try {
            return statement.executeUpdate(query);
        } catch (SQLException e) {
            throw e;
        }
    }

    /**
     * Return Rows that are the result of the query.
     * Each row is JSONObject and that has Columns as Object.
     * Each row has 'column name' as the keys and 'corresponding object' as the value.
     *
     * @return Rows that are the result of the query which is executed in executeQuery method
     * @throws SQLException
     * @throws JSONException
     */
    public JSONArray getResultRows() throws SQLException, JSONException {
        try {
            final int NUMBER_OF_COLUMNS = resultSet.getMetaData().getColumnCount();
            String[] columnLabels = new String[NUMBER_OF_COLUMNS];
            String[] columnTypeNames = new String[NUMBER_OF_COLUMNS];
            for (int i = 1; i <= NUMBER_OF_COLUMNS; i++) {
                columnLabels[i - 1] = resultSet.getMetaData().getColumnLabel(i);
                columnTypeNames[i - 1] = resultSet.getMetaData().getColumnTypeName(i);
            }

            JSONArray resultRows = new JSONArray();
            while (resultSet.next()) {
                JSONObject row = new JSONObject();
                for (int i = 0; i < NUMBER_OF_COLUMNS; i++) {
                    if (columnTypeNames[i].equals("VARCHAR") || columnTypeNames[i].equals("CHAR")) {
                        String resultString = resultSet.getString(columnLabels[i]);
                        row.put(columnLabels[i], resultString);
                    } else if (columnTypeNames[i].equals("INT") || columnTypeNames[i].equals("BIGINT")) {
                        int resultInt = resultSet.getInt(columnLabels[i]);
                        row.put(columnLabels[i], resultInt);
                    } else if (columnTypeNames[i].equals("DATETIME")) {
                        Date resultDate = resultSet.getTimestamp(columnLabels[i]);
                        row.put(columnLabels[i], resultDate);
                    }
                }
                resultRows.put(row);
            }
            return resultRows;
        } catch (SQLException e) {
            throw e;
        } catch (JSONException e) {
            throw e;
        }
    }

    /**
     * Return Columns that are the result of the query.
     * Each column is JSONArray and that has Rows as Object.
     * Each column has objects as the value corresponding rows.
     *
     * @return Columns that are the result of the query which is executed in executeQuery method
     * @throws SQLException
     * @throws JSONException
     */
    public JSONObject getResultColumns() throws SQLException, JSONException {
        try {
            JSONObject resultColumns = new JSONObject();
            final int NUMBER_OF_COLUMNS = resultSet.getMetaData().getColumnCount();
            String[] columnLabels = new String[NUMBER_OF_COLUMNS];
            String[] columnTypeNames = new String[NUMBER_OF_COLUMNS];
            for (int i = 1; i <= NUMBER_OF_COLUMNS; i++) {
                columnLabels[i - 1] = resultSet.getMetaData().getColumnLabel(i);
                columnTypeNames[i - 1] = resultSet.getMetaData().getColumnTypeName(i);
                resultColumns.put(columnLabels[i - 1], new JSONArray());
            }

            int numberOfRows = 0;
            while (resultSet.next()) {
                for (int i = 0; i < NUMBER_OF_COLUMNS; i++) {
                    JSONArray column = resultColumns.getJSONArray(columnLabels[i]);
                    if (columnTypeNames[i].equals("VARCHAR") || columnTypeNames[i].equals("CHAR")) {
                        String resultString = resultSet.getString(columnLabels[i]);
                        column.put(resultString);
                    } else if (columnTypeNames[i].equals("INT") || columnTypeNames[i].equals("BIGINT")) {
                        int resultInt = resultSet.getInt(columnLabels[i]);
                        column.put(resultInt);
                    } else if (columnTypeNames[i].equals("DATETIME")) {
                        Date resultDate = resultSet.getTimestamp(columnLabels[i]);
                        column.put(resultDate);
                    }
                }
                numberOfRows++;
            }
            resultColumns.put("numberOfRows", numberOfRows);
            return resultColumns;
        } catch (SQLException e) {
            throw e;
        } catch (JSONException e) {
            throw e;
        }
    }

    public static void main(String[] args) throws Exception {
        MysqlConnection doo = new MysqlConnection();
        doo.connect("ReviewGem");

        doo.executeQuery("select id from soma_2nd_pros where product_id = 1");
        System.out.println(doo.getResultRows());

        doo.close();
    }
}

/*

      // preparedStatements can use variables and are more efficient
      preparedStatement = connect
          .prepareStatement("insert into  FEEDBACK.COMMENTS values (default, ?, ?, ?, ? , ?, ?)");
      // "myuser, webpage, datum, summary, COMMENTS from FEEDBACK.COMMENTS");
      // parameters start with 1
      preparedStatement.setString(1, "Test");
      preparedStatement.setString(2, "TestEmail");
      preparedStatement.setString(3, "TestWebpage");
      preparedStatement.setDate(4, new java.sql.Date(2009, 12, 11));
      preparedStatement.setString(5, "TestSummary");
      preparedStatement.setString(6, "TestComment");
      preparedStatement.executeUpdate();

      preparedStatement = connect
          .prepareStatement("SELECT myuser, webpage, datum, summary, COMMENTS from FEEDBACK.COMMENTS");
      resultSet = preparedStatement.executeQuery();
      writeResultSet(resultSet);

      // remove again the insert comment
      preparedStatement = connect
      .prepareStatement("delete from FEEDBACK.COMMENTS where myuser= ? ; ");
      preparedStatement.setString(1, "Test");
      preparedStatement.executeUpdate();

      resultSet = statement
      .executeQuery("select * from FEEDBACK.COMMENTS");
      writeMetaData(resultSet);
*/