package yhannanum.db;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import yhannanum.comm.MorphemeIndex;
import yhannanum.comm.RuleArgRelation;
import yhannanum.comm.RuleArgument;
import yhannanum.share.JSONUtil;
import yhannanum.comm.Rule;

import java.lang.Exception;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Arthur on 2014. 4. 12..
 */
public class RuleTable extends  MysqlConnection {
    // TODO delete
    public void readFromRuleTable(String withWhere) throws Exception {
        String query = "select * from rule";
        if (withWhere != null) query += " where ( " + withWhere + ")";
        super.executeQuery(query);
    }

    public JSONObject ruleTableResultSetJSONObject() throws Exception {
        return ruleTableResultSetJSONObject(null);
    }

    // TODO delete
    public JSONObject ruleTableResultSetJSONObject(String withWhere) throws Exception {
        try {
            this.readFromRuleTable(withWhere);
            JSONObject resultSetJSONObject = this.getResultColumns();

            JSONArray parsed_target = new JSONArray();
            JSONArray parsed_attr = new JSONArray();
            JSONArray parsed_sentiment_eojeol = new JSONArray();
            int numberOfRows = resultSetJSONObject.getInt("numberOfRows");
            JSONArray targetJSONArray = resultSetJSONObject.getJSONArray("target");
            JSONArray attrJSONArray = resultSetJSONObject.getJSONArray("attr");
            JSONArray sentiment_eojeolJSONArray = resultSetJSONObject.getJSONArray("sentiment_eojeol");

            for (int i = 0; i < numberOfRows; i++) {
                String target = targetJSONArray.getString(i);
                String attr = attrJSONArray.getString(i);
                String sentiment_eojeol = sentiment_eojeolJSONArray.getString(i);
                parsed_target.put(JSONUtil.convertJsonStringToJsonArray(target));
                parsed_attr.put(JSONUtil.convertJsonStringToJsonArray(attr));
                parsed_sentiment_eojeol.put(JSONUtil.convertJsonStringToJsonArray(sentiment_eojeol));
            }
            resultSetJSONObject.put("parsed_target", parsed_target);
            resultSetJSONObject.put("parsed_attr", parsed_attr);
            resultSetJSONObject.put("parsed_sentiment_eojeol", parsed_sentiment_eojeol);

            return resultSetJSONObject;
        } catch (Exception e) {
            throw e;
        }
    }

    public Rule[] readRules() throws SQLException, JSONException {
        return readRules(null);
    }

    /**
     * Read rule table and convert into Rule Objects
     * About Rule Object, reference Rule Class in yhannanum.comm package
     *
     * @return Array of Rule Objects
     * @throws SQLException
     * @throws JSONException
     */
    public Rule[] readRules(String withWhere) throws SQLException, JSONException {
        try {
            String query = "select * from rule";
            if (withWhere != null) query += " where (" + withWhere + ")";
            this.executeQuery(query);
            JSONArray resultRows = this.getResultRows();
            Rule[] rules = new Rule[resultRows.length()];
            for (int i = 0; i < resultRows.length(); i++) {
                rules[i] = new Rule(resultRows.getJSONObject(i));
            }
            return rules;
        } catch (SQLException e) {
            throw e;
        } catch (JSONException e) {
            throw e;
        }
    }

    public Integer[] readReviewIds() throws SQLException, JSONException {
        return readReviewIds(null);
    }

    /**
     * Read list of 'review_id' as Integer array
     * Read 'review_id' from rule table as distinct rows.
     * Return Integer array of 'review_id'
     *
     * @return Integer array of 'review_id'
     * @throws SQLException
     * @throws JSONException
     */
    public Integer[] readReviewIds(String withWhere) throws SQLException, JSONException {
        try {
            String query = "select review_id from rule";
            if (withWhere != null) query += " where ( " + withWhere + " ) ";
            query += " group by review_id";
            executeQuery(query);
            JSONArray resultRows = this.getResultColumns().getJSONArray("review_id");
            Integer[] reviewIds = new Integer[resultRows.length()];
            for (int i = 0; i < resultRows.length(); i++) {
                reviewIds[i] = resultRows.getInt(i);
            }
            return reviewIds;
        } catch (SQLException e) {
            throw e;
        } catch (JSONException e) {
            throw e;
        }
    }

    public JSONObject readRuleMatchingResult() throws SQLException, JSONException {
        this.executeQuery("select * from rule_matching_result");
        JSONArray resultRows = getResultRows();
        JSONObject ruleMatchingResult = new JSONObject();
        for (int i = 0; i < resultRows.length(); i++) {
            JSONObject row = resultRows.getJSONObject(i);
            int review_id = row.getInt("review_id");
            int sentence_id = row.getInt("sentence_id");
            String origin_attr = null;
            if (row.has("origin_attr")) {
                origin_attr = row.getString("origin_attr");
            }
            int sentiment_type = row.getInt("sentiment_type");
            if (!ruleMatchingResult.has(String.valueOf(review_id))) {
                ruleMatchingResult.put(String.valueOf(review_id), new JSONObject());
            }
            JSONObject review = ruleMatchingResult.getJSONObject(String.valueOf(review_id));
            if (!review.has(String.valueOf(sentence_id))) {
                review.put(String.valueOf(sentence_id), new JSONObject());
            }
            JSONObject sentence = review.getJSONObject(String.valueOf(sentence_id));
            if (sentiment_type == -1) {
                sentence.put("not a rule", sentiment_type);
            } else {
                if (!sentence.has(origin_attr)) {
                    sentence.put(origin_attr, new JSONObject());
                }
                JSONObject attr = sentence.getJSONObject(origin_attr);
                if (!attr.has(String.valueOf(sentiment_type))) {
                    attr.put(String.valueOf(sentiment_type), true);
                }
            }
        }
        return ruleMatchingResult;
    }

    @SuppressWarnings("unchecked")
    public static void main(String[] args) throws Exception {
        RuleTable rt = new RuleTable();
        rt.connect();
        System.out.println(Charset.defaultCharset());
        rt.close();
    }
}