package yhannanum.db;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import yhannanum.comm.Crawling;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Arthur on 2014. 4. 12..
 */
public class CrawlingTable extends MysqlConnection {
    public Crawling[] readCrawlingTable() throws SQLException, JSONException {
        return readCrawlingTable(null, null);
    }

    public Crawling[] readCrawlingTable(String withWhere) throws SQLException, JSONException {
        return readCrawlingTable(withWhere, null);
    }

    public Crawling[] readCrawlingTable(String withWhere, String withGroupBy) throws SQLException, JSONException {
        String query = "select * from crawling";
        if (withWhere != null) query += " where (" + withWhere + ")";
        if (withGroupBy != null) query += " group by (" + withGroupBy + ")";
        executeQuery(query);
        JSONArray rows = getResultRows();
        Crawling[] crawlings = new Crawling[rows.length()];
        for (int i = 0; i < rows.length(); i++) {
            JSONObject row = rows.getJSONObject(i);
            crawlings[i] = new Crawling(row);
        }
        return crawlings;
    }


    public int readMinCrawledReviewId(String withWhere) throws SQLException, JSONException {
        String query = "select MIN(uid) as min_review_id from crawling";
        if (withWhere != null) query += " where (" + withWhere + ")";
        executeQuery(query);
        return getResultRows().getJSONObject(0).getInt("min_review_id");
    }

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        CrawlingTable ct = new CrawlingTable();
        ct.connect();
        ct.close();
    }
}
