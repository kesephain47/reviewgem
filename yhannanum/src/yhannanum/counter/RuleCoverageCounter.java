package yhannanum.counter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import yhannanum.comm.Rule;
import yhannanum.comm.Submit;
import yhannanum.share.Constants;
import yhannanum.share.DateUtil;
import yhannanum.share.FileIO;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;

/**
 * Created by Arthur on 2014. 5. 13..
 */
public class RuleCoverageCounter {
    private HashMap<Integer, Integer> countForRuleId = null;
    private HashMap<Integer, Rule> rulesForRuleId = null;
    private HashMap<Integer, Submit> submitForCrawlUid = null;
    private JSONObject ruleCoverageForUser = null;


    public RuleCoverageCounter(HashMap<Integer, Submit> submitForCrawlUid, Rule[] rules, HashMap<Integer, Integer> countForRuleId) {
        this.submitForCrawlUid = submitForCrawlUid;
        this.countForRuleId = countForRuleId;
        rulesForRuleId = new HashMap<Integer, Rule>();
        for (Rule rule : rules) {
            rulesForRuleId.put(rule.getId(), rule);
        }
    }

    private String getUserNameFromRuleId(Integer ruleId) {
        Integer reviewId = rulesForRuleId.get(ruleId).getReviewId();
        Submit submit = submitForCrawlUid.get(reviewId);
        if (submit == null) return null;
        else return submit.getUser();
    }

    private JSONObject getRuleObjectForId(Integer ruleId) throws JSONException {
        JSONObject ruleObject = new JSONObject();
        Rule ruleForRuleId = rulesForRuleId.get(ruleId);
        ruleObject.put("id", ruleId);
        ruleObject.put("sentiment_type", ruleForRuleId.getSentimentType());
        ruleObject.put("origin_attr", ruleForRuleId.getOriginAttr());
        ruleObject.put("sentence", ruleForRuleId.getSentence());
        ruleObject.put("target", Arrays.deepToString(ruleForRuleId.getTargets()));
        ruleObject.put("attr", Arrays.deepToString(ruleForRuleId.getAttrs()));
        ruleObject.put("sentiment_eojeol", Arrays.deepToString(ruleForRuleId.getSentimentEojeols()));
        ruleObject.put("count", countForRuleId.get(ruleId));
        ruleObject.put("date", DateUtil.reviewCreationDateString(ruleForRuleId.getDate()));
        return ruleObject;
    }

    private JSONObject getRuleCoverageForUser() throws JSONException {
        if (ruleCoverageForUser != null) return ruleCoverageForUser;
        Set<Integer> ruleIdSet = countForRuleId.keySet();
        ruleCoverageForUser = new JSONObject();
        for (Integer ruleId : ruleIdSet) {
            String userName = getUserNameFromRuleId(ruleId);
            if (userName == null) userName = "미지정";
            if (!ruleCoverageForUser.has(userName)) {
                ruleCoverageForUser.put(userName, new JSONArray());
            }
            JSONArray ruleCoveragesForUser = ruleCoverageForUser.getJSONArray(userName);
            JSONObject ruleObject = getRuleObjectForId(ruleId);
            ruleCoveragesForUser.put(ruleObject);
        }
        return ruleCoverageForUser;
    }

    public void saveForDate() throws JSONException, IOException {
        if (countForRuleId == null) return;
        JSONObject ruleCoverageForEachRuleAndUser = getRuleCoverageForUser();

        FileIO.getDirectory(Constants.RULE_COVERAGE_DIRECTORY_PATH);
        String[] userNames = JSONObject.getNames(ruleCoverageForEachRuleAndUser);
        for (String userName : userNames) {
            JSONArray ruleCoveragesForUser = ruleCoverageForEachRuleAndUser.getJSONArray(userName);
            JSONObject ruleCoveragesForDate = new JSONObject();
            int totalCount = 0;
            for (int i = 0; i < ruleCoveragesForUser.length(); i++) {
                for (int j = i + 1; j < ruleCoveragesForUser.length(); j++) {
                    String date1 = ruleCoveragesForUser.getJSONObject(i).getString("date");
                    String date2 = ruleCoveragesForUser.getJSONObject(j).getString("date");
                    if (date1.compareTo(date2) < 0) {
                        JSONObject tmp = ruleCoveragesForUser.getJSONObject(i);
                        ruleCoveragesForUser.put(i, ruleCoveragesForUser.getJSONObject(j));
                        ruleCoveragesForUser.put(j, tmp);
                    }
                }
                JSONObject ruleCoverage = ruleCoveragesForUser.getJSONObject(i);
                String dateString = ruleCoverage.getString("date");
                dateString = dateString.split(" ")[0];
                if (!ruleCoveragesForDate.has(dateString)) {
                    ruleCoveragesForDate.put(dateString, 0);
                }
                Integer countForDate = ruleCoveragesForDate.getInt(dateString);
                countForDate += ruleCoverage.getInt("count");
                ruleCoveragesForDate.put(dateString, countForDate);
                totalCount += ruleCoverage.getInt("count");
            }
            JSONObject userJsonObject = new JSONObject();
            userJsonObject.put("rule_coverages", ruleCoveragesForUser);
            userJsonObject.put("count_for_date", ruleCoveragesForDate);
            userJsonObject.put("total_count", totalCount);
            FileIO.writeFile(Constants.RULE_COVERAGE_DIRECTORY_PATH + "/coverage_for_date_" + userName + ".json", userJsonObject.toString());
        }
    }

    public void saveForRuleId() throws JSONException, IOException {
        if (countForRuleId == null) return;
        JSONObject ruleCoverageForEachRuleAndUser = getRuleCoverageForUser();

        FileIO.getDirectory(Constants.RULE_COVERAGE_DIRECTORY_PATH);
        String[] userNames = JSONObject.getNames(ruleCoverageForEachRuleAndUser);
        for (String userName : userNames) {
            JSONArray ruleCoveragesForUser = ruleCoverageForEachRuleAndUser.getJSONArray(userName);
            JSONArray countsForRuleIdForUser = new JSONArray();
            int totalCount = 0;
            for (int i = 0; i < ruleCoveragesForUser.length(); i++) {
                for (int j = i + 1; j < ruleCoveragesForUser.length(); j++) {
                    int count1 = ruleCoveragesForUser.getJSONObject(i).getInt("count");
                    int count2 = ruleCoveragesForUser.getJSONObject(j).getInt("count");
                    if (count1 < count2) {
                        JSONObject tmp = ruleCoveragesForUser.getJSONObject(i);
                        ruleCoveragesForUser.put(i, ruleCoveragesForUser.getJSONObject(j));
                        ruleCoveragesForUser.put(j, tmp);
                    }
                }
                JSONObject ruleCoverage = ruleCoveragesForUser.getJSONObject(i);
                totalCount += ruleCoverage.getInt("count");
                JSONObject countForRuleIdForUser = new JSONObject();
                countForRuleIdForUser.put("id", ruleCoverage.getInt("id"));
                countForRuleIdForUser.put("count", ruleCoverage.getInt("count"));
                countsForRuleIdForUser.put(countForRuleIdForUser);
            }
            JSONObject userJsonObject = new JSONObject();
            userJsonObject.put("rule_coverages", ruleCoveragesForUser);
            userJsonObject.put("count_for_ruleid", countsForRuleIdForUser);
            userJsonObject.put("total_count", totalCount);
            FileIO.writeFile(Constants.RULE_COVERAGE_DIRECTORY_PATH + "/coverage_for_ruleid_" + userName + ".json", userJsonObject.toString());
        }
    }
}
