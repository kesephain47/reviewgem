package yhannanum.comm;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Set;

/**
 * Created by Arthur on 2014. 6. 11..
 */
public class ProsConsForProduct {
    private HashMap<Integer, ProsConsForOriginAttr> prosConsSentences = new HashMap<Integer, ProsConsForOriginAttr>();

    public void addSentence(int productId, int sentimentType, String originAttr, int ruleId, String sentence) {
        if (!prosConsSentences.containsKey(productId)) {
            prosConsSentences.put(productId, new ProsConsForOriginAttr());
        }
        ProsConsForOriginAttr prosConsForOriginAttr = prosConsSentences.get(productId);
        prosConsForOriginAttr.addSentence(sentimentType, originAttr, ruleId, sentence);
    }

    public void setSentence(JSONObject prosConsJsonObject) throws JSONException {
        int productId = prosConsJsonObject.getInt("productId");
        int sentimentType = prosConsJsonObject.getInt("sentimentType");
        String originAttr = prosConsJsonObject.getString("originAttr");
        int ruleId = prosConsJsonObject.getInt("ruleId");
        String sentence = null;
        if (prosConsJsonObject.has("sentence")) sentence = prosConsJsonObject.getString("sentence");
        int count = prosConsJsonObject.getInt("count");
        setSentence(productId, sentimentType, originAttr, ruleId, sentence, count);
    }

    public void setSentence(int productId, int sentimentType, String originAttr, int ruleId, String sentence, int count) {
        if (!prosConsSentences.containsKey(productId)) {
            prosConsSentences.put(productId, new ProsConsForOriginAttr());
        }
        ProsConsForOriginAttr prosConsForOriginAttr = prosConsSentences.get(productId);
        prosConsForOriginAttr.setSentence(sentimentType, originAttr, ruleId, sentence, count);
    }

    public JSONArray getSentenceJsonArray(int productId, int sentimentType, String[] sortedKeys) throws JSONException {
        if (sortedKeys == null || sortedKeys.length == 0) return null;

        ProsConsForOriginAttr prosConsForOriginAttr = prosConsSentences.get(productId);
        JSONArray sentenceJsonArray = new JSONArray();
        for (String originAttr : sortedKeys) {
            String sentenceForKey = prosConsForOriginAttr.getSentence(sentimentType, originAttr);
            if (sentenceForKey == null) continue;
            boolean hasSameSentence = false;
            for (int i = 0; i < sentenceJsonArray.length(); i++) {
                if (sentenceJsonArray.getString(i).equals(sentenceForKey)) {
                    hasSameSentence = true;
                    break;
                }
            }
            if (!hasSameSentence) sentenceJsonArray.put(sentenceForKey);
        }

        return sentenceJsonArray;
    }

    public JSONArray toJsonArray() throws JSONException {
        JSONArray prosConsJsonArray = new JSONArray();
        Set<Integer> productIdSet = prosConsSentences.keySet();
        for (int productId : productIdSet) {
            ProsConsForOriginAttr prosConsForOriginAttr = prosConsSentences.get(productId);
            JSONArray prosConsForOriginAttrJsonArray = prosConsForOriginAttr.toJsonArray();
            for (int i = 0; i < prosConsForOriginAttrJsonArray.length(); i++) {
                JSONObject prosConsForOriginAttrJsonObject = prosConsForOriginAttrJsonArray.getJSONObject(i);
                prosConsForOriginAttrJsonObject.put("productId", productId);
                prosConsJsonArray.put(prosConsForOriginAttrJsonObject);
            }
        }
        return prosConsJsonArray;
    }
}