package yhannanum.comm;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;

/**
 * Created by Arthur on 2014. 5. 6..
 */
public class Crawling {
    private Integer uid = null;

    private String path = null;

    private String url = null;

    private int disuse = -1;

    private Integer productId = null;

    private String query = null;

    public Crawling(JSONObject crawlingData) throws JSONException {
        if (crawlingData.has("uid")) uid = crawlingData.getInt("uid");
        if (crawlingData.has("path")) path = crawlingData.getString("path");
        if (crawlingData.has("url")) url = crawlingData.getString("url");
        if (crawlingData.has("disuse")) disuse = crawlingData.getInt("disuse");
        if (crawlingData.has("product_id")) productId = crawlingData.getInt("product_id");
        if (crawlingData.has("query")) query = crawlingData.getString("query");
    }

    public Integer getUid() { return uid; }

    public String getPath() { return path; }

    public String getFileName() { return path.substring(path.lastIndexOf("/") + 1); }

    public String getUrl() { return url; }

    public int getDisuse() { return disuse; }

    public Integer getProductId() { return productId; }

    public String getQuery() { return query; }

    public static Integer[] getUids(Crawling[] crawlings) {
        Integer[] uids = new Integer[crawlings.length];
        for (int i = 0; i < crawlings.length; i++) {
            uids[i] = crawlings[i].getUid();
        }
        return uids;
    }

    public static String[] getPaths(Crawling[] crawlings) {
        String[] paths = new String[crawlings.length];
        for (int i = 0; i < crawlings.length; i++) {
            paths[i] = crawlings[i].getPath();
        }
        return paths;
    }

    public static HashMap<Integer, String> getPathForUid(Crawling[] crawlings) {
        HashMap<Integer, String> pathForUid = new HashMap<Integer, String>();
        for (Crawling crawling : crawlings) {
            pathForUid.put(crawling.getUid(), crawling.getPath());
        }
        return pathForUid;
    }

    public static HashMap<Integer, String> getFileNameForUid(Crawling[] crawlings) {
        HashMap<Integer, String> pathForUid = new HashMap<Integer, String>();
        for (Crawling crawling : crawlings) {
            pathForUid.put(crawling.getUid(), crawling.getFileName());
        }
        return pathForUid;
    }

    public static String[] getFileNames(Crawling[] crawlings) {
        String[] fileNames = new String[crawlings.length];
        for (int i = 0; i < crawlings.length; i++) {
            fileNames[i] = crawlings[i].getFileName();
        }
        return fileNames;
    }
}
