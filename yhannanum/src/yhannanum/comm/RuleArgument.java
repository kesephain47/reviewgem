package yhannanum.comm;

import kr.ac.kaist.swrc.jhannanum.comm.Eojeol;
import kr.ac.kaist.swrc.jhannanum.comm.Sentence;

import java.lang.reflect.Method;
import java.util.*;

/**
 * Created by Arthur on 2014. 4. 24..
 */

public class RuleArgument {

    private String[] morphemes = null;

    private MorphemeIndex[] indices = null;

    private String[] tags = null;

    private List<Integer> ruleIds = null;

    public RuleArgument() {}

    public RuleArgument(String[] morphemes, MorphemeIndex[] indices, String[] tags) {
        this(morphemes, indices, tags, true);
    }

    public RuleArgument(String[] morphemes, MorphemeIndex[] indices, String[] tags, boolean deepCopy) {
        if (deepCopy) {
            if (morphemes != null) this.morphemes = morphemes.clone();
            if (indices != null) {
                this.indices = new MorphemeIndex[indices.length];
                for (int i = 0; i < this.indices.length; i++) {
                    this.indices[i] = indices[i].clone();
                    if (this.indices[i].getEojeol() < 0)
                        System.out.println("error");
                }
            }
            if (tags != null) this.tags = tags.clone();
        } else {
            this.morphemes = morphemes;
            this.indices = indices;
            this.tags = tags;
        }
    }

    public static RuleArgument[] getRuleArguSet(Rule[] rules, Method getMorphemes, Method getIndices, Method getTags) throws Exception {
        return getRuleArguSet(rules, getMorphemes, getIndices, getTags, false, null);
    }

    public static RuleArgument[] getRuleArguSet(Rule[] rules, Method getMorphemes, Method getIndices, Method getTags, boolean setIndicesToAbsCord) throws Exception {
        return getRuleArguSet(rules, getMorphemes, getIndices, getTags, setIndicesToAbsCord, null);
    }

    public static RuleArgument[] getRuleArguSet(Rule[] rules, Method getMorphemes, Method getIndices, Method getTags, boolean setIndicesToAbsCord, String[] roleTags) throws Exception {
        List<RuleArgument> targetRuleArguSet = new ArrayList<RuleArgument>();
        for (int i = 0; i < rules.length; i++) {
            RuleArgument ruleArgument = new RuleArgument((String[])getMorphemes.invoke(rules[i]), (MorphemeIndex[])getIndices.invoke(rules[i]), (String[])getTags.invoke(rules[i]));
            if (roleTags != null) ruleArgument.deleteExtraMorphemes(roleTags);
            if (setIndicesToAbsCord) ruleArgument.setIndicesToAbsCord();
            boolean exist = false;
            for (RuleArgument targetRuleArgu : targetRuleArguSet) {
                if (targetRuleArgu.equals(ruleArgument)) {
//                    targetRuleArgu.addRuleId(rules[i].getId());
                    targetRuleArgu.addRuleId(i);
                    exist = true;
                    break;
                }
            }
            if (!exist && ruleArgument.getMorphemes() != null && ruleArgument.getMorphemes().length > 0) {
//                ruleArgument.addRuleId(rules[i].getId());
                ruleArgument.addRuleId(i);
                targetRuleArguSet.add(ruleArgument);
            }
        }

        return targetRuleArguSet.toArray(new RuleArgument[targetRuleArguSet.size()]);
    }

    @Override
    public String toString() {
        String str = "";
        if (morphemes != null) str += " morphemes : " + Arrays.deepToString(morphemes);
        if (indices != null) str += " indices : " + Arrays.deepToString(indices);
        if (tags != null) str += " tags : " + Arrays.deepToString(tags);
        if (ruleIds != null) str += " rule_ids : " + ruleIds.toString();

        return str;
    }

    public boolean equals(RuleArgument ruleArgument) { return equals(ruleArgument, false); }

    public boolean equals(RuleArgument ruleArgument, boolean includeRuleIds) {
        if (!Arrays.deepEquals(morphemes, ruleArgument.getMorphemes())) return false;
        if ((indices == null && ruleArgument.getIndices() != null) ||
                (indices != null && ruleArgument.getIndices() == null) ||
                (indices != null && ruleArgument.getIndices() != null && indices.length != ruleArgument.getIndices().length)) {
            return false;
        } else if(indices != null && ruleArgument.getIndices() != null) {
            MorphemeIndex[] ruleArgIndices = ruleArgument.getIndices();
            for (int i = 0; i < indices.length; i++) {
                if (!indices[i].equals(ruleArgIndices[i])) return false;
            }
        }
        if (!Arrays.deepEquals(tags, ruleArgument.getTags())) return false;
        if (includeRuleIds && !Arrays.deepEquals(ruleIds.toArray(), ruleArgument.getRuleIds().toArray())) return false;
        return true;
    }

    public String[] getMorphemes() { return morphemes; }

    public MorphemeIndex[] getIndices() { return indices; }

    public String[] getTags() { return tags; }

    public List<Integer> getRuleIds() { return ruleIds; }

    public void setMorphemes(String[] morphemes) { this.morphemes = morphemes; }

    public void setIndices(MorphemeIndex[] indices) { this.indices = indices; }

    public void setTags(String[] tags) { this.tags = tags; }

    public void setRuleIds(List<Integer> ruleIds) { this.ruleIds = ruleIds; }

    public void addRuleId(Integer ruleId) {
        if (ruleIds == null) ruleIds = new ArrayList<Integer>();
        ruleIds.add(ruleId);
    }

    public boolean hasMorpheme() { return (this.morphemes != null && this.morphemes.length > 0); }

    public boolean hasIndices() { return (this.indices != null && this.indices.length > 0); }

    public boolean hasTags() { return (this.tags != null && this.tags.length > 0); }

    public void setIndicesToAbsCord() {
        if (indices == null) return;
        int preEojeol = -1;
        int preMorpheme = -1;
        int preAbsEojeol = 0;
        int preAbsMorpheme = 0;
        for (int i = 0; i < indices.length; i++) {
            MorphemeIndex index = indices[i];
            if (i == 0) {
            } else if (index.getEojeol() != preEojeol) {
                preAbsEojeol += (index.getEojeol() - preEojeol);
                preAbsMorpheme = 0;
            } else {
                preAbsMorpheme += (index.getMorpheme() - preMorpheme);
            }
            preEojeol = index.getEojeol();
            preMorpheme = index.getMorpheme();
            index.setEojeol(preAbsEojeol);
            index.setMorpheme(preAbsMorpheme);
        }
    }

    public void deleteExtraMorphemes(String[] roleTags) {
        if (tags == null) return;
        List<String> roleTagList = Arrays.asList(roleTags);
        List<String> morphemeList = null;
        if (morphemes != null) morphemeList = new ArrayList<String>(Arrays.asList(morphemes));
        List<MorphemeIndex> indexList = null;
        if (indices != null) indexList = new ArrayList<MorphemeIndex>(Arrays.asList(indices));
        List<String> tagList = new ArrayList<String>(Arrays.asList(tags));
        for (int i = 0; i < tagList.size();) {
            if (roleTagList.contains(tagList.get(i))) {
                i++;
            } else {
                if (morphemeList != null) morphemeList.remove(i);
                if (indexList != null) indexList.remove(i);
                tagList.remove(i);
            }
        }
        if (morphemes != null && morphemeList != null) morphemes = morphemeList.toArray(new String[morphemeList.size()]);
        if (indices != null && indexList != null) indices = indexList.toArray(new MorphemeIndex[indexList.size()]);
        tags = tagList.toArray(new String[tagList.size()]);
    }

    private static void makeClones(List<List<MorphemeIndex>> originalList, int nTimes) {
        int originalListSize = originalList.size();
        for (int i = 0; i < nTimes; i++) {
            for (int j = 0; j < originalListSize; j++) {
                List<MorphemeIndex> originalArray = originalList.get(j);
                originalList.add(new ArrayList<MorphemeIndex>(originalArray));
            }
        }
    }

    public static List<MorphemeIndex[]> getOneDepthArgIndicesList(List<List<MorphemeIndex[]>[]> originalArgIndices) {
        if (originalArgIndices == null || originalArgIndices.size() == 0) return null;
        List<MorphemeIndex[]> oneDepthArgIndices = new ArrayList<MorphemeIndex[]>();

        for (int i = 0; i < originalArgIndices.size(); i++) {
            List<MorphemeIndex[]>[] eojeolsArgIndices = originalArgIndices.get(i);
            List<List<MorphemeIndex>> oneDepthEojeolArgIndicesList = new ArrayList<List<MorphemeIndex>>();
            for (int j = 0; j < eojeolsArgIndices.length; j++) {
                List<MorphemeIndex[]> eojeolArgIndicesList = eojeolsArgIndices[j];
                makeClones(oneDepthEojeolArgIndicesList, eojeolArgIndicesList.size() - 1);

                if (oneDepthEojeolArgIndicesList.size() == 0) {
                    for (int k = 0; k < eojeolArgIndicesList.size(); k++) {
                        MorphemeIndex[] eojeolArgIndices = eojeolArgIndicesList.get(k);
                        oneDepthEojeolArgIndicesList.add(new ArrayList<MorphemeIndex>(Arrays.asList(eojeolArgIndices)));
                    }
                } else {
                    for (int k = 0; k < eojeolArgIndicesList.size(); k++) {
                        MorphemeIndex[] eojeolArgIndices = eojeolArgIndicesList.get(k);
                        for (int l = k * eojeolArgIndicesList.size(); l < (k + 1) * eojeolArgIndicesList.size(); l++) {
                            List<MorphemeIndex> oneDepthEojeolArgIndices = oneDepthEojeolArgIndicesList.get(l);
                            oneDepthEojeolArgIndices.addAll(Arrays.asList(eojeolArgIndices));
                        }
                    }
                }
            }
            for (int j = 0; j < oneDepthEojeolArgIndicesList.size(); j++) {
                List<MorphemeIndex> oneDepthEojeolArgIndices = oneDepthEojeolArgIndicesList.get(j);
                oneDepthArgIndices.add(oneDepthEojeolArgIndices.toArray(new MorphemeIndex[oneDepthEojeolArgIndices.size()]));
            }
        }

        return oneDepthArgIndices;
    }

    private static MorphemeIndex[] getArgIndicesIncludedInEojeolFromIndex(Eojeol eojeol, int eojeolIndex,
                                                                          int eojeolMorphemeStartingIndex, RuleArgument ruleArgument,
                                                                          int ruleArgIndex) {
        String[] argMorphemes = ruleArgument.getMorphemes();
        MorphemeIndex[] argIndices = ruleArgument.getIndices();
        String[] argTags = ruleArgument.getTags();
        String[] eojeolMorphemes = eojeol.getMorphemes();
        String[] eojeolTags = eojeol.getTags();
        List<MorphemeIndex> argInEojeolIndices = new ArrayList<MorphemeIndex>();

        for (int i = ruleArgIndex; i < argMorphemes.length; i++) {
            int eojeolMorphemeIndex = eojeolMorphemeStartingIndex + argIndices[i].getMorpheme();
            if (i != ruleArgIndex && !argIndices[i].getEojeol().equals(argIndices[i-1].getEojeol())) {
                return argInEojeolIndices.toArray(new MorphemeIndex[argInEojeolIndices.size()]);
            } else if (eojeolMorphemeIndex < eojeolMorphemes.length &&
                    (!eojeolMorphemes[eojeolMorphemeIndex].equals(argMorphemes[i]) ||
                            !eojeolTags[eojeolMorphemeIndex].equals(argTags[i]))) {
                return null;
            } else if (eojeolMorphemeIndex >= eojeolMorphemes.length) {
                return null;
            }
            argInEojeolIndices.add(new MorphemeIndex(eojeolIndex, eojeolMorphemeIndex));
        }
        return argInEojeolIndices.toArray(new MorphemeIndex[argInEojeolIndices.size()]);
    }

    private static boolean doesArgIncludedInEojeolFromIndex(Eojeol eojeol, int eojeolStartingIndex, RuleArgument ruleArgument,
                                                            int ruleArgIndex) {
        String[] argMorphemes = ruleArgument.getMorphemes();
        MorphemeIndex[] argIndices = ruleArgument.getIndices();
        String[] argTags = ruleArgument.getTags();
        String[] eojeolMorphemes = eojeol.getMorphemes();
        String[] eojeolTags = eojeol.getTags();

        for (int i = ruleArgIndex; i < argMorphemes.length; i++) {
            int eojeolIndex = eojeolStartingIndex + argIndices[i].getMorpheme();
            if (i != ruleArgIndex && !argIndices[i].getEojeol().equals(argIndices[i-1].getEojeol())) {
                return true;
            } else if (eojeolIndex < eojeolMorphemes.length &&
                    (!eojeolMorphemes[eojeolIndex].equals(argMorphemes[i]) || !eojeolTags[eojeolIndex].equals(argTags[i]))) {
                return false;
            } else if (eojeolIndex >= eojeolMorphemes.length) {
                return false;
            }
        }
        return true;
    }

    // 한 어절 내에 속한 Argument의 어절을 모두 찾을것인지. 어절 내에 있기만 하면 되니까 한개의 조합만 찾을 것인지.
    // 일단은 모두 찾아서 하나만 사용하는 방향으로 이용.
    private static List<MorphemeIndex[]> getArgIndicesIncludedInEojeol(Eojeol eojeol, int eojeolIndex, RuleArgument ruleArgument,
                                                                       int ruleArgIndex) {
        String[] argMorphemes = ruleArgument.getMorphemes();
        String[] argTags = ruleArgument.getTags();
        String[] eojeolMorphemes = eojeol.getMorphemes();
        String[] eojeolTags = eojeol.getTags();
        List<MorphemeIndex[]> argInEojeolIndicesList = new ArrayList<MorphemeIndex[]>();

        for (int i = 0; i < eojeolMorphemes.length; i++) {
            if (eojeolMorphemes[i].equals(argMorphemes[ruleArgIndex]) && eojeolTags[i].equals(argTags[ruleArgIndex])) {
                MorphemeIndex[] argInEojeolIndices = getArgIndicesIncludedInEojeolFromIndex(eojeol, eojeolIndex, i, ruleArgument,
                        ruleArgIndex);
                if (argInEojeolIndices != null && argInEojeolIndices.length > 0) argInEojeolIndicesList.add(argInEojeolIndices);
            }
        }
        if (argInEojeolIndicesList.size() > 0) return argInEojeolIndicesList;
        else return null;
    }

    private static boolean doesArgIncludedInEojeol(Eojeol eojeol, RuleArgument ruleArgument, int ruleArgIndex) {
        String[] argMorphemes = ruleArgument.getMorphemes();
        String[] argTags = ruleArgument.getTags();
        String[] eojeolMorphemes = eojeol.getMorphemes();
        String[] eojeolTags = eojeol.getTags();

        for (int i = 0; i < eojeolMorphemes.length; i++) {
            if (eojeolMorphemes[i].equals(argMorphemes[ruleArgIndex]) && eojeolTags[i].equals(argTags[ruleArgIndex]) &&
                    doesArgIncludedInEojeolFromIndex(eojeol, i, ruleArgument, ruleArgIndex)) {
                return true;
            }
        }
        return false;
    }

    private static int getNextRuleArgEojeolIndex(MorphemeIndex[] argIndices, int ruleArgIndex) {
        for (int i = ruleArgIndex + 1; i < argIndices.length; i++) {
            if (!argIndices[i].getEojeol().equals(argIndices[i - 1].getEojeol())) {
                return i;
            }
        }
        return argIndices.length;
    }

    @SuppressWarnings("unchecked")
    private static List<MorphemeIndex[]>[] getArgIndicesIncludedInEojeolsFromIndex(Eojeol[] eojeols, int eojeolStartingIndex,
                                                                                   RuleArgument ruleArgument) {
        MorphemeIndex[] argIndices = ruleArgument.getIndices();
        List<List<MorphemeIndex[]>> argInEojeolsIndices = new ArrayList<List<MorphemeIndex[]>>();

        int ruleArgIndex = 0;
        for (int i = eojeolStartingIndex; (i < eojeols.length && i >= 0);) {
            Eojeol eojeol = eojeols[i];
            List<MorphemeIndex[]> argInEojeolIndicesList = getArgIndicesIncludedInEojeol(eojeol, i, ruleArgument, ruleArgIndex);
            if (argInEojeolIndicesList != null) {
                argInEojeolsIndices.add(argInEojeolIndicesList);
                int preArgIndex = argIndices[ruleArgIndex].getEojeol();
                ruleArgIndex = getNextRuleArgEojeolIndex(argIndices, ruleArgIndex);
                if (ruleArgIndex >= argIndices.length) {
                    if (argInEojeolsIndices.size() > 0) return argInEojeolsIndices.toArray(new List[argInEojeolsIndices.size()]);
                    else return null;
                }
                i += argIndices[ruleArgIndex].getEojeol() - preArgIndex;
            } else {
                return null;
            }
        }
        return null;
    }

    private static boolean doesArgIncludedInEojeolsFromIndex(Eojeol[] eojeols, int eojeolStartingIndex, RuleArgument ruleArgument) {
        MorphemeIndex[] argIndices = ruleArgument.getIndices();

        int ruleArgIndex = 0;
        for (int i = eojeolStartingIndex; (i < eojeols.length && i >= 0);) {
            Eojeol eojeol = eojeols[i];
            if (doesArgIncludedInEojeol(eojeol, ruleArgument, ruleArgIndex)) {
                int preArgIndex = argIndices[ruleArgIndex].getEojeol();
                ruleArgIndex = getNextRuleArgEojeolIndex(argIndices, ruleArgIndex);
                if (ruleArgIndex >= argIndices.length) return true;
                i += argIndices[ruleArgIndex].getEojeol() - preArgIndex;
            } else {
                return false;
            }
        }
        return false;
    }

    public static List<List<MorphemeIndex[]>[]> getArgIndicesIncludedInEojeols(Eojeol[] eojeols, RuleArgument ruleArgument) {
        String[] argMorphemes = ruleArgument.getMorphemes();
        String[] argTags = ruleArgument.getTags();
        List<List<MorphemeIndex[]>[]> argInEojeolsIndicesList = new ArrayList<List<MorphemeIndex[]>[]>();

        if (!ruleArgument.hasMorpheme() || !ruleArgument.hasIndices() || !ruleArgument.hasTags()) return null;

        for (int i = 0; i < eojeols.length; i++) {
            String[] eojeolMorphemes = eojeols[i].getMorphemes();
            String[] eojeolTags = eojeols[i].getTags();
            for (int j = 0; j < eojeolMorphemes.length; j++) {
                if (eojeolMorphemes[j].equals(argMorphemes[0]) && eojeolTags[j].equals(argTags[0])) {
                    List<MorphemeIndex[]>[] argInEojeolsIndices = getArgIndicesIncludedInEojeolsFromIndex(eojeols, i, ruleArgument);
                    if (argInEojeolsIndices != null) argInEojeolsIndicesList.add(argInEojeolsIndices);
                }
            }
        }
        if (argInEojeolsIndicesList.size() > 0) return argInEojeolsIndicesList;
        else return null;
    }

    public static List<List<MorphemeIndex[]>[]> getArgIndicesIncludedInSentence(Sentence sentence, RuleArgument ruleArgument) {
        return getArgIndicesIncludedInEojeols(sentence.getEojeols(), ruleArgument);
    }

    private static boolean doesArgIncludedInEojeols(Eojeol[] eojeols, RuleArgument ruleArgument) {
        String[] argMorphemes = ruleArgument.getMorphemes();
        String[] argTags = ruleArgument.getTags();

        if (argMorphemes == null || argTags == null) return false;

        for (int i = 0; i < eojeols.length; i++) {
            String[] eojeolMorphemes = eojeols[i].getMorphemes();
            String[] eojeolTags = eojeols[i].getTags();

            for (int j = 0; j < eojeolMorphemes.length; j++) {
                if (eojeolMorphemes[j].equals(argMorphemes[0]) && eojeolTags[j].equals(argTags[0])) {
                    if (doesArgIncludedInEojeolsFromIndex(eojeols, i, ruleArgument)) {
                        return true;
                    }
                    break;
                }
            }
        }

        return false;
    }

    private static List<Integer> getIncludedArgRuleIds(Sentence sentence, RuleArgument ruleArgument) {
        Eojeol[] eojeols = sentence.getEojeols();

        if (doesArgIncludedInEojeols(eojeols, ruleArgument)) {
            return ruleArgument.getRuleIds();
        }
        return null;
    }

    public static List<Integer> getIncludedArgsRuleIds(Sentence sentence, RuleArgument[] ruleArguments) {
        Set<Integer> ruleIds = new HashSet<Integer>();

        for (RuleArgument ruleArgument : ruleArguments) {
            List<Integer> argRuleIds = getIncludedArgRuleIds(sentence, ruleArgument);
            if (argRuleIds != null) {
                ruleIds.addAll(argRuleIds);
//                System.out.println(ruleArgument);
            }
        }
        return Arrays.asList(ruleIds.toArray(new Integer[ruleIds.size()]));
    }
}
