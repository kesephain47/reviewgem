package yhannanum.comm;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Date;

import yhannanum.share.JSONUtil;

/**
 * Created by Arthur on 2014. 4. 22..
 */
public class Rule {
    /**
     * rule id. id is auto-incremental
     */
    private Integer id = null;

    /**
     * reviewId is the uid of crawling table which means the index of review in crawling table.
     */
    private Integer reviewId = null;

    /**
     * sentenceId is row index of sentences from review.
     * it starts from 1.
     */
    private Integer sentenceId = null;

    /**
     * There are three sentiment_types.
     * 0 : positive
     * 1 : negative
     * 2 : neutral
     */
    private Integer sentimentType = null;

    /**
     * We specified several origin_attr.
     * This is representative attribute string.
     */
    private String originAttr = null;

    /**
     * sentence of rule.
     */
    private String sentence = null;

    /**
     * String array of target morphemes.
     * target is usually product name.
     */
    private String[] targets = null;

    /**
     * String array of attr morphemes.
     * attr is usually the feature of the product.
     */
    private String[] attrs = null;

    /**
     * String array of sentiment morphemes.
     * sentiment is usually sentimental words and it's related with attr
     */
    private String[] sentiments = null;

    /**
     * String array of target eojeols.
     * target is usually product name.
     */
    private String[] targetEojeols = null;

    /**
     * String array of attr eojeols.
     * attr is usually the feature of the product.
     */
    private String[] attrEojeols = null;

    /**
     * String array of sentiment eojeols.
     * sentiment is usually sentimental words and it's related with attr
     */
    private String[] sentimentEojeols = null;

    /**
     * String array of target tags.
     * target is usually product name.
     */
    private String[] targetTags = null;

    /**
     * String array of attr tags.
     * attr is usually the feature of the product.
     */
    private String[] attrTags = null;

    /**
     * String array of sentiment tags.
     * sentiment is usually sentimental words and it's related with attr
     */
    private String[] sentimentTags = null;

    /**
     * MorphemeIndex array of target morpheme index.
     */
    private MorphemeIndex[] targetIndices = null;

    /**
     * MorphemeIndex array of attr morpheme index.
     */
    private MorphemeIndex[] attrIndicies = null;

    /**
     * MorphemeIndex array of sentiment morpheme index.
     */
    private MorphemeIndex[] sentimentIndices = null;

    /**
     * date that rule is saved in rule table
     */
    private Date date = null;

    public Rule() {}

    public Rule(JSONObject ruleData) throws JSONException {
        id = ruleData.getInt("id");
        reviewId = ruleData.getInt("review_id");
        sentenceId = ruleData.getInt("sentence_id");
        sentimentType = ruleData.getInt("sentiment_type");
        originAttr = ruleData.getString("origin_attr");
        sentence = ruleData.getString("sentence");
        date = (Date)ruleData.get("date");
        if (ruleData.has("target"))
            targets = JSONUtil.convertJsonStringToStrings(ruleData.getString("target"));
        if (ruleData.has("attr"))
            attrs = JSONUtil.convertJsonStringToStrings(ruleData.getString("attr"));
        if (ruleData.has("sentiment"))
            sentiments = JSONUtil.convertJsonStringToStrings(ruleData.getString("sentiment"));
        if (ruleData.has("target_eojeol"))
            targetEojeols = JSONUtil.convertJsonStringToStrings(ruleData.getString("target_eojeol"));
        if (ruleData.has("attr_eojeol"))
            attrEojeols = JSONUtil.convertJsonStringToStrings(ruleData.getString("attr_eojeol"));
        if (ruleData.has("sentiment_eojeol"))
            sentimentEojeols = JSONUtil.convertJsonStringToStrings(ruleData.getString("sentiment_eojeol"));
        if (ruleData.has("target_tag"))
            targetTags = JSONUtil.convertJsonStringToStrings(ruleData.getString("target_tag"));
        if (ruleData.has("attr_tag"))
            attrTags = JSONUtil.convertJsonStringToStrings(ruleData.getString("attr_tag"));
        if (ruleData.has("sentiment_tag"))
            sentimentTags = JSONUtil.convertJsonStringToStrings(ruleData.getString("sentiment_tag"));
        if (ruleData.has("target_index"))
            targetIndices = MorphemeIndex.convertJsonToMorphemeIndices(ruleData.getString("target_index"));
        if (ruleData.has("attr_index"))
            attrIndicies = MorphemeIndex.convertJsonToMorphemeIndices(ruleData.getString("attr_index"));
        if (ruleData.has("sentiment_index"))
            sentimentIndices = MorphemeIndex.convertJsonToMorphemeIndices(ruleData.getString("sentiment_index"));
    }

    @Override
    public String toString() {
        String str = "";
        if (id != null) str += " id : " + id;
        if (reviewId != null) str += " review_id : " + reviewId;
        if (sentenceId != null) str += " sentence_id : " + sentenceId;
        if (sentimentType != null) str += " sentiment_type : " + sentimentType;
        if (originAttr != null) str += " origin_attr : " + originAttr;
        if (sentence != null) str += " sentence : " + sentence;
        if (targets != null) str += " target : " + Arrays.deepToString(targets);
        if (attrs != null) str += " attr : " + Arrays.deepToString(attrs);
        if (sentiments != null) str += " sentiment : " + Arrays.deepToString(sentiments);
        if (targetEojeols != null) str += " target_eojeol : " + Arrays.deepToString(targetEojeols);
        if (attrEojeols != null) str += " attr_eojeol : " + Arrays.deepToString(attrEojeols);
        if (sentimentEojeols != null) str += " sentiment_eojeol : " + Arrays.deepToString(sentimentEojeols);
        if (targetTags != null) str += " target_tag : " + Arrays.deepToString(targetTags);
        if (attrTags != null) str += " attr_tag : " + Arrays.deepToString(attrTags);
        if (sentimentTags != null) str += " sentiment_tag : " + Arrays.deepToString(sentimentTags);
        if (targetIndices != null) str += " target_index : " + Arrays.deepToString(targetIndices);
        if (attrIndicies != null) str += " attr_index : " + Arrays.deepToString(attrIndicies);
        if (sentimentIndices != null) str += " sentiment_index : " + Arrays.deepToString(sentimentIndices);
        if (date != null) str += " date : " + date.toString();
        return str;
    }

    public Integer getId() { return id; }

    public Integer getReviewId() { return reviewId; }

    public Integer getSentenceId() { return sentenceId; }

    public Integer getSentimentType() { return sentimentType; }

    public String getOriginAttr() { return originAttr; }

    public String getSentence() { return sentence; }

    public String[] getTargets() { return targets; }

    public String[] getAttrs() { return attrs; }

    public String[] getSentiments() { return sentiments; }

    public String[] getTargetEojeols() { return targetEojeols; }

    public String[] getAttrEojeols() { return attrEojeols; }

    public String[] getSentimentEojeols() { return sentimentEojeols; }

    public String[] getTargetTags() { return targetTags; }

    public String[] getAttrTags() { return attrTags; }

    public String[] getSentimentTags() { return sentimentTags; }

    public MorphemeIndex[] getTargetIndices() { return targetIndices; }

    public MorphemeIndex[] getAttrIndices() { return attrIndicies; }

    public MorphemeIndex[] getSentimentIndices() { return sentimentIndices; }

    public Date getDate() { return date; }

}
