package yhannanum.comm;

import kr.ac.kaist.swrc.jhannanum.comm.Sentence;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;

/**
 * Created by Arthur on 2014. 4. 28..
 */
public class RsfWorkerResult {

    private Integer reviewId = null;

    private Integer sentenceId = null;

    private Sentence sentence = null;

    private List<Integer> ruleIds = null;

    private List<List<MorphemeIndex[]>> targetIndices = null;

    private List<List<MorphemeIndex[]>> attrIndices = null;

    private List<List<MorphemeIndex[]>> sentimentIndices = null;

    public RsfWorkerResult(Integer reviewId, Integer sentenceId, Sentence sentence) {
        this.reviewId = reviewId;
        this.sentenceId = sentenceId;
        this.sentence = sentence;
    }

    public RsfWorkerResult(JSONObject jsonObject) throws JSONException {
        if (jsonObject.has("reviewId")) reviewId = jsonObject.getInt("reviewId");
        if (jsonObject.has("sentenceId")) sentenceId = jsonObject.getInt("sentenceId");
        if (jsonObject.has("sentence")) sentence = new Sentence(jsonObject.getJSONObject("sentence"));
        if (jsonObject.has("ruleIds")) {
            JSONArray ruleIdJsonArray = jsonObject.getJSONArray("ruleIds");
            ruleIds = new ArrayList<Integer>(ruleIdJsonArray.length());
            for (int i = 0; i < ruleIdJsonArray.length(); i++) {
                ruleIds.add(ruleIdJsonArray.getInt(i));
            }
        }
        if (jsonObject.has("targetIndices")) {
            targetIndices = convertJsonArrayToListListMorphemeIndexArray(jsonObject.getJSONArray("targetIndices"));
        }
        if (jsonObject.has("attrIndices")) {
            attrIndices = convertJsonArrayToListListMorphemeIndexArray(jsonObject.getJSONArray("attrIndices"));
        }
        if (jsonObject.has("sentimentIndices")) {
            sentimentIndices = convertJsonArrayToListListMorphemeIndexArray(jsonObject.getJSONArray("sentimentIndices"));
        }
    }

    public Integer getReviewId() { return reviewId; }

    public Integer getSentenceId() { return sentenceId; }

    public Sentence getSentence() { return sentence; }

    public List<Integer> getRuleIds() { return ruleIds; }

    public List<List<MorphemeIndex[]>> getTargetIndices() { return targetIndices; }

    public List<List<MorphemeIndex[]>> getAttrIndices() { return attrIndices; }

    public List<List<MorphemeIndex[]>> getSentimentIndices() { return sentimentIndices; }

    private JSONArray convertListListMorphemeIndexArrayToJsonArray(List<List<MorphemeIndex[]>> indices) {
        JSONArray indexJsonArray = new JSONArray();
        for (List<MorphemeIndex[]> indicesListForRuleId : indices) {
            JSONArray indexListForRuleIdJsonArray = new JSONArray();
            for (MorphemeIndex[] indicesForRuleId : indicesListForRuleId) {
                JSONArray indexForRuleIdJsonArray = new JSONArray();
                for (MorphemeIndex index : indicesForRuleId) {
                    indexForRuleIdJsonArray.put(index.toJsonArray());
                }
                indexListForRuleIdJsonArray.put(indexForRuleIdJsonArray);
            }
            indexJsonArray.put(indexListForRuleIdJsonArray);
        }
        return indexJsonArray;
    }

    private List<List<MorphemeIndex[]>> convertJsonArrayToListListMorphemeIndexArray(JSONArray jsonArray) throws JSONException {
        ArrayList<List<MorphemeIndex[]>> resultList = new ArrayList<List<MorphemeIndex[]>>(jsonArray.length());
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONArray jsonArrayForListMorphemeArray = jsonArray.getJSONArray(i);
            ArrayList<MorphemeIndex[]> resultListOfMorphemeIndexArray = new ArrayList<MorphemeIndex[]>(jsonArrayForListMorphemeArray.length());
            for (int j = 0; j < jsonArrayForListMorphemeArray.length(); j++) {
                JSONArray jsonArrayForMorphemeArray = jsonArrayForListMorphemeArray.getJSONArray(j);
                MorphemeIndex[] morphemeIndexArray = new MorphemeIndex[jsonArrayForMorphemeArray.length()];
                for (int k = 0; k < jsonArrayForMorphemeArray.length(); k++) {
                    morphemeIndexArray[k] = new MorphemeIndex(jsonArrayForMorphemeArray.getJSONArray(k));
                }
                resultListOfMorphemeIndexArray.add(morphemeIndexArray);
            }
            resultList.add(resultListOfMorphemeIndexArray);
        }
        return resultList;
    }

    public JSONObject toJsonObject() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        if (reviewId != null) jsonObject.put("reviewId", reviewId);
        if (sentenceId != null) jsonObject.put("sentenceId", sentenceId);
        if (sentence != null) jsonObject.put("sentence", sentence.toJsonObject());
        if (ruleIds != null) jsonObject.put("ruleIds", ruleIds);
        if (targetIndices != null) {
            JSONArray indexJsonArray = convertListListMorphemeIndexArrayToJsonArray(targetIndices);
            jsonObject.put("targetIndices", indexJsonArray);
        }
        if (attrIndices != null) {
            JSONArray indexJsonArray = convertListListMorphemeIndexArrayToJsonArray(attrIndices);
            jsonObject.put("attrIndices", indexJsonArray);
        }
        if (sentimentIndices != null) {
            JSONArray indexJsonArray = convertListListMorphemeIndexArrayToJsonArray(sentimentIndices);
            jsonObject.put("sentimentIndices", indexJsonArray);
        }
        return jsonObject;
    }

    public JSONObject getMatchedRuleJson(int index, RuleArgRelation[] ruleArgRelations, Rule[] rules, JSONObject ruleMatchingResult) throws JSONException {
        if (index >= ruleIds.size() || index < 0) return null;
        int ruleIndex = ruleIds.get(index);
        RuleArgRelation ruleArgRelation = ruleArgRelations[ruleIndex];
        Rule rule = rules[ruleIndex];
        JSONObject matchedRuleJson = new JSONObject();
        matchedRuleJson.put("rule_id", ruleArgRelation.getId());
        matchedRuleJson.put("target", Arrays.deepToString(ruleArgRelation.getTarget().getMorphemes()));
        matchedRuleJson.put("abs_target_index", Arrays.deepToString(ruleArgRelation.getTarget().getIndices()));
        matchedRuleJson.put("original_target_index", Arrays.deepToString(ruleArgRelation.getTargetIndices()));
        matchedRuleJson.put("target_tag", Arrays.deepToString(ruleArgRelation.getTarget().getTags()));
        matchedRuleJson.put("attr", Arrays.deepToString(ruleArgRelation.getAttr().getMorphemes()));
        matchedRuleJson.put("abs_attr_index", Arrays.deepToString(ruleArgRelation.getAttr().getIndices()));
        matchedRuleJson.put("original_attr_index", Arrays.deepToString(ruleArgRelation.getAttrIndices()));
        matchedRuleJson.put("attr_tag", Arrays.deepToString(ruleArgRelation.getAttr().getTags()));
        matchedRuleJson.put("modified_sentiment", Arrays.deepToString(ruleArgRelation.getSentiment().getMorphemes()));
        matchedRuleJson.put("abs_modified_sentiment_index", Arrays.deepToString(ruleArgRelation.getSentiment().getIndices()));
        matchedRuleJson.put("original_modified_sentiment_index", Arrays.deepToString(ruleArgRelation.getSentimentIndices()));
        matchedRuleJson.put("modified_sentiment_tag", Arrays.deepToString(ruleArgRelation.getSentiment().getTags()));
        matchedRuleJson.put("sentiment", Arrays.deepToString(rule.getSentiments()));
        matchedRuleJson.put("original_sentiment_index", Arrays.deepToString(rule.getSentimentIndices()));
        matchedRuleJson.put("sentiment_tag", Arrays.deepToString(rule.getSentimentTags()));
        matchedRuleJson.put("sentiment_type", ruleArgRelation.getSentimentType());
        matchedRuleJson.put("origin_attr", rule.getOriginAttr());
        if (ruleMatchingResult != null && ruleMatchingResult.has("not a rule")) {
            matchedRuleJson.put("compare_with_result", "not a rule");
        } else if (ruleMatchingResult != null && ruleMatchingResult.has(rule.getOriginAttr())) {
            JSONObject attr = ruleMatchingResult.getJSONObject(rule.getOriginAttr());
            if (!attr.has(String.valueOf(ruleArgRelation.getSentimentType()))) {
                if (attr.has(String.valueOf(-2))) {
                    matchedRuleJson.put("compare_with_result", "not this attr");
                } else {
                    matchedRuleJson.put("compare_with_result", "wrong");
                }
            } else {
                matchedRuleJson.put("compare_with_result", "correct");
            }
        } else {
            matchedRuleJson.put("compare_with_result", "no result");
        }

        List<MorphemeIndex[]> targetIndices = this.targetIndices.get(index);
        List<MorphemeIndex[]> attrIndices = this.attrIndices.get(index);
        List<MorphemeIndex[]> sentimentIndices = this.sentimentIndices.get(index);
        JSONObject matchedIndices = new JSONObject();
        matchedIndices.put("target", Arrays.deepToString(targetIndices.toArray(new MorphemeIndex[][]{})));
        matchedIndices.put("attr", Arrays.deepToString(attrIndices.toArray(new MorphemeIndex[][]{})));
        matchedIndices.put("sentiment", Arrays.deepToString(sentimentIndices.toArray(new MorphemeIndex[][]{})));
        matchedRuleJson.put("matched_indices", matchedIndices);
        return matchedRuleJson;
    }

    public static JSONObject getCheckedIndices(Sentence sentence, RuleArgRelation ruleArgRelation) throws JSONException {
        RuleArgument target = ruleArgRelation.getTarget();
        RuleArgument attr = ruleArgRelation.getAttr();
        RuleArgument sentiment = ruleArgRelation.getSentiment();
        List<MorphemeIndex[]> targetIndices = RuleArgument.getOneDepthArgIndicesList(RuleArgument.getArgIndicesIncludedInSentence(sentence, target));
        List<MorphemeIndex[]> attrIndices = RuleArgument.getOneDepthArgIndicesList(RuleArgument.getArgIndicesIncludedInSentence(sentence, attr));
        List<MorphemeIndex[]> sentimentIndices = RuleArgument.getOneDepthArgIndicesList(RuleArgument.getArgIndicesIncludedInSentence(sentence, sentiment));

        if (target != null && target.getMorphemes() != null && target.getMorphemes().length > 0 && (targetIndices == null || targetIndices.size() == 0)) return null;
        if (attr != null && attr.getMorphemes() != null && attr.getMorphemes().length > 0 && (attrIndices == null || attrIndices.size() == 0)) return null;
        if (sentiment != null && sentiment.getMorphemes() != null && sentiment.getMorphemes().length > 0 && (sentimentIndices == null || sentimentIndices.size() == 0)) return null;

        RuleArgRelation.IndexOffset target_attr = ruleArgRelation.getTarget_attr();
        RuleArgRelation.IndexOffset attr_sentiment = ruleArgRelation.getAttr_sentiment();
        RuleArgRelation.IndexOffset target_sentiment = ruleArgRelation.getTarget_sentiment();

        if (target_attr != null && (targetIndices == null || attrIndices == null)) return null;
        if (attr_sentiment != null && (attrIndices == null || sentimentIndices == null)) return null;
        if (target_sentiment != null && (targetIndices == null || sentimentIndices == null)) return null;

        if (target_attr != null && attr_sentiment != null && target_sentiment != null) {
            List<MorphemeIndex[]>[] resultIndices = RuleArgRelation.getCheckedEojeolIndices(targetIndices, attrIndices, sentimentIndices,
                    target_attr, attr_sentiment, target_sentiment);
            if (resultIndices == null) return null;
            targetIndices = resultIndices[0];
            attrIndices = resultIndices[1];
            sentimentIndices = resultIndices[2];
        } else if (target_attr != null) {
            List<MorphemeIndex[]>[] resultIndices = RuleArgRelation.getCheckedEojeolIndices(targetIndices, attrIndices, target_attr, 10);
            if (resultIndices == null) return null;
            targetIndices = resultIndices[0];
            attrIndices = resultIndices[1];
            sentimentIndices = null;
        } else if (attr_sentiment != null) {
            List<MorphemeIndex[]>[] resultIndices = RuleArgRelation.getCheckedEojeolIndices(attrIndices, sentimentIndices, attr_sentiment, 2);
            if (resultIndices == null) return null;
            targetIndices = null;
            attrIndices = resultIndices[0];
            sentimentIndices = resultIndices[1];
        } else if (target_sentiment != null) {
            List<MorphemeIndex[]>[] resultIndices = RuleArgRelation.getCheckedEojeolIndices(targetIndices, sentimentIndices, target_sentiment, 10);
            if (resultIndices == null) return null;
            targetIndices = resultIndices[0];
            attrIndices = null;
            sentimentIndices = resultIndices[1];
        } else {
            return null;
        }

        JSONObject indices = new JSONObject();
        if (targetIndices != null) indices.put("targetIndices", (Object)targetIndices);
        if (attrIndices != null) indices.put("attrIndices", (Object)attrIndices);
        if (sentimentIndices != null) indices.put("sentimentIndices", (Object)sentimentIndices);
        if (indices.length() == 0) return null;
        else return indices;
    }

    // 인덱스를 이용해서, 해당 Relation의 모든 Arg의 거리가 처리 가능한지. 가능하다면 가능한 리스트만 리턴하도록???
    // 가능한 리스트는 Object로 받기 때문에, Method 내에서 변경하면 될듯.
    @SuppressWarnings("unchecked")
    public void checkAndAddRuleResult(Sentence sentence, int ruleArgRelationIndex, RuleArgRelation ruleArgRelation) throws JSONException {
        if (this.sentence == null) this.sentence = sentence;
        JSONObject checkedIndices = RsfWorkerResult.getCheckedIndices(sentence, ruleArgRelation);
        if (checkedIndices == null) return;
        List<MorphemeIndex[]> targetIndices = null;
        List<MorphemeIndex[]> attrIndices = null;
        List<MorphemeIndex[]> sentimentIndices = null;
        if (checkedIndices.has("targetIndices")) targetIndices = (List<MorphemeIndex[]>)checkedIndices.get("targetIndices");
        if (checkedIndices.has("attrIndices")) attrIndices = (List<MorphemeIndex[]>)checkedIndices.get("attrIndices");
        if (checkedIndices.has("sentimentIndices")) sentimentIndices = (List<MorphemeIndex[]>)checkedIndices.get("sentimentIndices");

        addRuleResult(ruleArgRelationIndex, targetIndices, attrIndices, sentimentIndices);
    }

    public void addRuleResult(int ruleIndex, List<MorphemeIndex[]> targetIndices, List<MorphemeIndex[]> attrIndices, List<MorphemeIndex[]> sentimentIndices) {
        if (this.ruleIds == null) this.ruleIds = new ArrayList<Integer>();
        this.ruleIds.add(ruleIndex);
        if (this.targetIndices == null) this.targetIndices = new ArrayList<List<MorphemeIndex[]>>();
        if (targetIndices == null) targetIndices = new ArrayList<MorphemeIndex[]>();
        this.targetIndices.add(targetIndices);
        if (this.attrIndices == null) this.attrIndices = new ArrayList<List<MorphemeIndex[]>>();
        if (attrIndices == null) attrIndices = new ArrayList<MorphemeIndex[]>();
        this.attrIndices.add(attrIndices);
        if (this.sentimentIndices == null) this.sentimentIndices = new ArrayList<List<MorphemeIndex[]>>();
        if (sentimentIndices == null) sentimentIndices = new ArrayList<MorphemeIndex[]>();
        this.sentimentIndices.add(sentimentIndices);
    }
}