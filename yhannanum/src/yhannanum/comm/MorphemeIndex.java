package yhannanum.comm;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.*;

/**
 * Created by Arthur on 2014. 4. 18..
 */
public class MorphemeIndex {

    /**
     * index of eojeol
     */
    private Integer eojeol = null;

    /**
     * index of morpheme in eojeol
     */
    private Integer morpheme = null;

    public MorphemeIndex() {}

    public MorphemeIndex(Integer eojeol, Integer morpheme) {
        this.eojeol = eojeol;
        this.morpheme = morpheme;
    }

    public MorphemeIndex(JSONArray morpheme) throws JSONException {
        this.eojeol = morpheme.getInt(0);
        if (this.eojeol < 0) this.eojeol = null;
        this.morpheme = morpheme.getInt(1);
        if (this.morpheme < 0) this.morpheme = null;
    }

    public static boolean areForwardDirection(MorphemeIndex left, MorphemeIndex right) {
        if (left.getMorpheme() != null && right.getMorpheme() != null) {
            return ((left.getEojeol() < right.getEojeol()) ||
                    (left.getEojeol().intValue() == right.getEojeol().intValue() && left.getMorpheme() <= right.getMorpheme()));
        } else {
            return (left.getEojeol() <= right.getEojeol());
        }
    }

    public static void addIndexToIndexList(ArrayList<MorphemeIndex> destinationIndexList, MorphemeIndex sourceIndex) {
        if (destinationIndexList == null || sourceIndex == null) return;
        else if (destinationIndexList.size() == 0) {
            destinationIndexList.add(sourceIndex);
            return;
        }
        for (int i = 0; i < destinationIndexList.size(); i++) {
            MorphemeIndex leftIndex = destinationIndexList.get(i);
            if (leftIndex.equals(sourceIndex)) return;
            if (!areForwardDirection(leftIndex, sourceIndex)) {
                destinationIndexList.add(i, sourceIndex);
                return;
            } else if (i < destinationIndexList.size() - 1) {
                MorphemeIndex rightIndex = destinationIndexList.get(i + 1);
                if (rightIndex.equals(sourceIndex)) return;
                if (areForwardDirection(sourceIndex, rightIndex)) {
                    destinationIndexList.add(i + 1, sourceIndex);
                    return;
                }
            } else {
                destinationIndexList.add(sourceIndex);
                return;
            }
        }
    }

    public static void addIndicesToIndexList(ArrayList<MorphemeIndex> destinationIndexList, MorphemeIndex[] sourceIndices) {
        for (MorphemeIndex sourceIndex : sourceIndices) {
            MorphemeIndex.addIndexToIndexList(destinationIndexList, sourceIndex);
        }
    }

    public static void addIndicesListToIndexList(ArrayList<MorphemeIndex> destinationIndexList, List<MorphemeIndex[]> sourceIndicesList) {
        for (MorphemeIndex[] sourceIndices : sourceIndicesList) {
            MorphemeIndex.addIndicesToIndexList(destinationIndexList, sourceIndices);
        }
    }

    public static ArrayList<MorphemeIndex> makeMergedIndicesList(List<MorphemeIndex[]> indicesList) {
        ArrayList<MorphemeIndex> mergedIndicesList = new ArrayList<MorphemeIndex>();
        MorphemeIndex.addIndicesListToIndexList(mergedIndicesList, indicesList);
        return mergedIndicesList;
    }

    public static MorphemeIndex[] getMergedIndices(MorphemeIndex[] destinationIndices, MorphemeIndex[] sourceIndices) {
        if (destinationIndices == null) destinationIndices = new MorphemeIndex[0];
        ArrayList<MorphemeIndex> destinationIndexList = new ArrayList<MorphemeIndex>(Arrays.asList(destinationIndices));
        addIndicesToIndexList(destinationIndexList, sourceIndices);

        return destinationIndexList.toArray(new MorphemeIndex[destinationIndexList.size()]);
    }

    public static MorphemeIndex[] getMergedIndices(List<MorphemeIndex[]> indicesList) {
        ArrayList<MorphemeIndex> mergedIndicesList = makeMergedIndicesList(indicesList);
        if (mergedIndicesList.size() > 0) return mergedIndicesList.toArray(new MorphemeIndex[mergedIndicesList.size()]);
        else return null;
    }

    @Override
    public MorphemeIndex clone() {
        MorphemeIndex clone = new MorphemeIndex();
        if (this.eojeol != null) clone.setEojeol(this.eojeol.intValue());
        if (this.morpheme != null) clone.setMorpheme(this.morpheme.intValue());
        return clone;
    }

    public boolean equals(MorphemeIndex morphemeIndex) {
        if (!eojeol.equals(morphemeIndex.eojeol)) return false;
        else if (!morpheme.equals(morphemeIndex.morpheme)) return false;
        return true;
    }

    public static MorphemeIndex[] convertJsonToMorphemeIndices(JSONArray json) throws JSONException {
        MorphemeIndex[] morphemes = new MorphemeIndex[json.length()];
        for (int i = 0; i < json.length(); i++) {
            morphemes[i] = new MorphemeIndex(json.getJSONArray(i));
        }

        return morphemes;
    }

    public static MorphemeIndex[] convertJsonToMorphemeIndices(String jsonString) throws JSONException {
        return convertJsonToMorphemeIndices(new JSONArray(jsonString));
    }

    @Override
    public String toString() {
        if (eojeol != null && morpheme != null) {
            return "[" + eojeol + "," + morpheme + "]";
        } else if (eojeol != null) {
            return "[" + eojeol + "]";
        } else {
            return "";
        }
    }

    public JSONArray toJsonArray() {
        JSONArray jsonArray = new JSONArray();
        if (eojeol == null) {
            jsonArray.put(-1);
        } else {
            jsonArray.put(eojeol);
        }
        if (morpheme == null) {
            jsonArray.put(-1);
        } else {
            jsonArray.put(morpheme);
        }
        return jsonArray;
    }

    public Integer getEojeol() { return eojeol; }

    public Integer getMorpheme() { return morpheme; }

    public void setEojeol(Integer eojeol) { this.eojeol = eojeol; }

    public void setMorpheme(Integer morpheme) { this.morpheme = morpheme; }

    public static Set<Integer> extractEojeolIndicesSet(MorphemeIndex[] indices) {
        Set<Integer> highlightingEojeols = new HashSet<Integer>();
        for (MorphemeIndex index : indices) {
            highlightingEojeols.add(index.getEojeol());
        }
        return highlightingEojeols;
    }

    public static Integer[] extractEojeolIndicesArray(MorphemeIndex[] indices) {
        Set<Integer> highlightingEojeols = extractEojeolIndicesSet(indices);
        return highlightingEojeols.toArray(new Integer[highlightingEojeols.size()]);
    }

}
