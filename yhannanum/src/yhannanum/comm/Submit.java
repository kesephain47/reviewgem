package yhannanum.comm;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Created by Arthur on 2014. 5. 12..
 */
public class Submit {
    private Integer uid = null;

    private Integer crawling_uid = null;

    private String user = null;

    private String url = null;

    private String path = null;

    private int review_point = -1;

    private int pos_neg_point = -1;

    private Date date = null;

    public Submit(JSONObject submitData) throws JSONException {
        if (submitData.has("uid")) uid = submitData.getInt("uid");
        if (submitData.has("crawling_uid")) crawling_uid = submitData.getInt("crawling_uid");
        if (submitData.has("user")) user = submitData.getString("user");
        if (submitData.has("url")) url = submitData.getString("url");
        if (submitData.has("path")) path = submitData.getString("path");
        if (submitData.has("review_point")) review_point = submitData.getInt("review_point");
        if (submitData.has("pos_neg_point")) pos_neg_point = submitData.getInt("pos_neg_point");
        if (submitData.has("date")) date = (Date)submitData.get("date");
    }

    public Integer getUid() { return uid; }

    public Integer getCrawling_uid() { return crawling_uid; }

    public String getUser() { return user; }

    public String getUrl() { return url; }

    public String getPath() { return path; }

    public int getReview_point() { return review_point; }

    public int getPos_neg_point() { return pos_neg_point; }

    public Date getDate() { return date; }
}
