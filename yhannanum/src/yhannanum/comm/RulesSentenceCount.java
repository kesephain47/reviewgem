package yhannanum.comm;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Set;

/**
 * Created by Arthur on 2014. 6. 10..
 */
public class RulesSentenceCount {
    private HashMap<Integer, RuleSentenceCount> ruleSentenceCountHashMap = new HashMap<Integer, RuleSentenceCount>();
    private int count = 0;

    public Set<Integer> getRuleIds() { return ruleSentenceCountHashMap.keySet(); }

    public int getRuleCount(int ruleId) {
        RuleSentenceCount ruleSentenceCount = ruleSentenceCountHashMap.get(ruleId);
        return ruleSentenceCount.getCount();
    }

    public String getRuleSentence(int ruleId) {
        RuleSentenceCount ruleSentenceCount = ruleSentenceCountHashMap.get(ruleId);
        return ruleSentenceCount.getSentence();
    }

    public int getRulesCount() { return count; }

    public void addRuleSentenceCount(int ruleId, String sentence) {
        if (ruleSentenceCountHashMap.containsKey(ruleId)) {
            RuleSentenceCount ruleSentenceCount = ruleSentenceCountHashMap.get(ruleId);
            ruleSentenceCount.addSentence(sentence);
        } else {
            ruleSentenceCountHashMap.put(ruleId, new RuleSentenceCount(sentence));
        }
        count++;
    }

    public void setRuleSentenceCount(int ruleId, String sentence, int count) {
        if (ruleSentenceCountHashMap.containsKey(ruleId)) {
            RuleSentenceCount ruleSentenceCount = ruleSentenceCountHashMap.get(ruleId);
            ruleSentenceCount.setSentenceCount(sentence, count);
        } else {
            ruleSentenceCountHashMap.put(ruleId, new RuleSentenceCount(sentence, count));
        }
    }

    public String getMaxCountSentence() {
        int maxCount = -1;
        String maxCountSentence = null;
        Set<Integer> ruleIdSet = ruleSentenceCountHashMap.keySet();
        for (Integer ruleId : ruleIdSet) {
            RuleSentenceCount ruleSentenceCount = ruleSentenceCountHashMap.get(ruleId);
            int count = ruleSentenceCount.getCount();
            String sentence = ruleSentenceCount.getSentence();
            if (count > maxCount && sentence != null) {
                maxCount = count;
                maxCountSentence = sentence;
            } else if (count == maxCount &&
                    (maxCountSentence == null ||
                            (sentence != null && maxCountSentence.length() > sentence.length()) ) ) {
                maxCountSentence = sentence;
            }
        }
        return maxCountSentence;
    }

    public JSONArray toJsonArray() throws JSONException {
        JSONArray rulesSentenceCountJsonArray = new JSONArray();
        Set<Integer> ruleIdSet = ruleSentenceCountHashMap.keySet();
        for (Integer ruleId : ruleIdSet) {
            RuleSentenceCount ruleSentenceCount = ruleSentenceCountHashMap.get(ruleId);
            JSONObject ruleSentenceCountJsonObject = ruleSentenceCount.toJsonObject();
            ruleSentenceCountJsonObject.put("ruleId", ruleId);
            rulesSentenceCountJsonArray.put(ruleSentenceCountJsonObject);
        }
        return rulesSentenceCountJsonArray;
    }

    class RuleSentenceCount {
        private String sentence = null;
        private int count = 0;
        RuleSentenceCount(String sentence) {
            if (sentence != null && sentence.replace(" ", "").length() > 0) this.sentence = sentence;
            this.count = 1;
        }

        RuleSentenceCount(String sentence, int count) {
            this(sentence);
            this.count = count;
        }

        void setSentenceCount(String sentence, int count) {
            if (sentence != null && sentence.replace(" ", "").length() > 0) this.sentence = sentence;
            this.count = count;
        }

        void addSentence(String sentence) {
            if (sentence != null && sentence.replace(" ", "").length() > 0 &&
                    (this.sentence == null || this.sentence.length() > sentence.length())) {
                this.sentence = sentence;
            }
            this.count++;
        }

        String getSentence() { return sentence; }

        int getCount() { return count; }

        JSONObject toJsonObject() throws JSONException {
            JSONObject ruleSentenceCountJsonObject = new JSONObject();
            ruleSentenceCountJsonObject.put("sentence", sentence);
            ruleSentenceCountJsonObject.put("count", count);
            return ruleSentenceCountJsonObject;
        }
    }
}