package yhannanum.comm;

import org.json.JSONException;
import org.json.JSONObject;
import yhannanum.share.JsonObjectStringBuilder;

import java.util.HashMap;
import java.util.Set;

/**
 * Created by Arthur on 2014. 6. 11..
 */
public class HighLightResultIndex {
    private HashMap<Integer, HashMap> allAllHashMap = new HashMap<Integer, HashMap>();
    private HashMap<Integer, HashMap> allSentimentalHashMap = new HashMap<Integer, HashMap>();
    private HashMap<Integer, HashMap> filterAllHashMap = new HashMap<Integer, HashMap>();
    private HashMap<Integer, HashMap> filterSentimentalHashMap = new HashMap<Integer, HashMap>();

    public static final String ALL_ALL = "allAll";
    public static final String ALL_SENTIMENTAL = "allSentimental";
    public static final String FILTER_ALL = "filterAll";
    public static final String FILTER_SENTIMENTAL = "filterSentimental";


    @SuppressWarnings("unchecked")
    private HashMap<Integer, HashMap> getProductHashMap(HashMap<Integer, HashMap> hashMap, int productId) {
        if (!hashMap.containsKey(productId)) {
            hashMap.put(productId, new HashMap());
        }
        return hashMap.get(productId);
    }

    @SuppressWarnings("unchecked")
    private HashMap<String, HashMap> getOriginAttrHashMap(HashMap<String, HashMap> hashMap, String originAttr) {
        if (!hashMap.containsKey(originAttr)) {
            hashMap.put(originAttr, new HashMap());
        }
        return hashMap.get(originAttr);
    }

    @SuppressWarnings("unchecked")
    private HashMap<Integer, HashMap> getSentimentTypeHashMap(HashMap<Integer, HashMap> hashMap, int sentimentType) {
        if (!hashMap.containsKey(sentimentType)) {
            hashMap.put(sentimentType, new HashMap());
        }
        return hashMap.get(sentimentType);
    }

    @SuppressWarnings("unchecked")
    private HashMap<Integer, Integer> getReviewHashMap(HashMap<Integer, HashMap> hashMap, int reviewId) {
        if (!hashMap.containsKey(reviewId)) {
            hashMap.put(reviewId, new HashMap<Integer, Integer>());
        }
        return hashMap.get(reviewId);
    }

    public void setAllAllHashMap(JSONObject allAllJsonObject) throws JSONException {
        String[] productIds = JSONObject.getNames(allAllJsonObject);
        for (String productId : productIds) {
            JSONObject productJsonObject = allAllJsonObject.getJSONObject(productId);
            String[] reviewIds = JSONObject.getNames(productJsonObject);
            for (String reviewId : reviewIds) {
                JSONObject reviewJsonObject = productJsonObject.getJSONObject(reviewId);
                String[] sentenceIds = JSONObject.getNames(reviewJsonObject);
                HashMap<Integer, Integer> reviewHashMap = getAllAllHashMap(Integer.valueOf(productId), Integer.valueOf(reviewId));
                for (String sentenceId : sentenceIds) {
                    reviewHashMap.put(Integer.valueOf(sentenceId), reviewJsonObject.getInt(sentenceId));
                }
            }
        }
    }

    public HashMap<Integer, Integer> getAllAllHashMap(int productId, int reviewId) {
        HashMap<Integer, HashMap> productHashMap = getProductHashMap(allAllHashMap, productId);
        return getReviewHashMap(productHashMap, reviewId);
    }

    public HashMap<Integer, HashMap> getAllAllHashMap() {
        return allAllHashMap;
    }

    @SuppressWarnings("unchecked")
    public String getAllAllHashMapJsonString() {
        JsonObjectStringBuilder allAllJsonStringBuilder = new JsonObjectStringBuilder();
        Set<Integer> productIdSet = allAllHashMap.keySet();
        for (Integer productId : productIdSet) {
            JsonObjectStringBuilder productJsonStringBuilder = new JsonObjectStringBuilder();
            HashMap<Integer, HashMap> productHashMap = allAllHashMap.get(productId);
            Set<Integer> reviewIdSet = productHashMap.keySet();
            for (Integer reviewId : reviewIdSet) {
                JsonObjectStringBuilder reviewJsonStringBuilder = new JsonObjectStringBuilder();
                HashMap<Integer, Integer> reviewHashMap = productHashMap.get(reviewId);
                Set<Integer> sentenceIdSet = reviewHashMap.keySet();
                for (Integer sentenceId : sentenceIdSet) {
                    reviewJsonStringBuilder.putValue(sentenceId.toString(), reviewHashMap.get(sentenceId));
                }
                productJsonStringBuilder.putJson(reviewId.toString(), reviewJsonStringBuilder);
            }
            allAllJsonStringBuilder.putJson(productId.toString(), productJsonStringBuilder);
        }
        return allAllJsonStringBuilder.toString();
    }

    public void setFilterAllHashMap(JSONObject filterAllJsonObject) throws JSONException {
        String[] productIds = JSONObject.getNames(filterAllJsonObject);
        for (String productId : productIds) {
            JSONObject productJsonObject = filterAllJsonObject.getJSONObject(productId);
            String[] originAttrs = JSONObject.getNames(productJsonObject);
            for (String originAttr : originAttrs) {
                JSONObject originAttrJsonObject = productJsonObject.getJSONObject(originAttr);
                String[] reviewIds = JSONObject.getNames(originAttrJsonObject);
                for (String reviewId : reviewIds) {
                    JSONObject reviewJsonObject = originAttrJsonObject.getJSONObject(reviewId);
                    String[] sentenceIds = JSONObject.getNames(reviewJsonObject);
                    HashMap<Integer, Integer> reviewHashMap = getFilterAllHashMap(Integer.valueOf(productId), originAttr, Integer.valueOf(reviewId));
                    for (String sentenceId : sentenceIds) {
                        reviewHashMap.put(Integer.valueOf(sentenceId), reviewJsonObject.getInt(sentenceId));
                    }
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    public HashMap<Integer, Integer> getFilterAllHashMap(int productId, String originAttr, int reviewId) {
        HashMap productHashMap = getProductHashMap(filterAllHashMap, productId);
        HashMap originAttrHashMap = getOriginAttrHashMap(productHashMap, originAttr);
        return getReviewHashMap(originAttrHashMap, reviewId);
    }

    public HashMap<Integer, HashMap> getFilterAllHashMap() {
        return filterAllHashMap;
    }

    @SuppressWarnings("unchecked")
    public String getFilterAllJsonString() {
        JsonObjectStringBuilder filterAllJsonStringBuilter = new JsonObjectStringBuilder();
        Set<Integer> productIdSet = filterAllHashMap.keySet();
        for (Integer productId : productIdSet) {
            JsonObjectStringBuilder productJsonStringBuilder = new JsonObjectStringBuilder();
            HashMap<String, HashMap> productHashMap = filterAllHashMap.get(productId);
            Set<String> originAttrSet = productHashMap.keySet();
            for (String originAttr : originAttrSet) {
                JsonObjectStringBuilder originAttrJsonStringBuilder = new JsonObjectStringBuilder();
                HashMap<Integer, HashMap> originAttrHashMap = productHashMap.get(originAttr);
                Set<Integer> reviewIdSet = originAttrHashMap.keySet();
                for (Integer reviewId : reviewIdSet) {
                    JsonObjectStringBuilder reviewJsonStringBuilder = new JsonObjectStringBuilder();
                    HashMap<Integer, Integer> reviewHashMap = originAttrHashMap.get(reviewId);
                    Set<Integer> sentenceIdSet = reviewHashMap.keySet();
                    for (Integer sentenceId : sentenceIdSet ) {
                        reviewJsonStringBuilder.putValue(sentenceId.toString(), reviewHashMap.get(sentenceId));
                    }
                    originAttrJsonStringBuilder.putJson(reviewId.toString(), reviewJsonStringBuilder);
                }
                productJsonStringBuilder.putJson(originAttr, originAttrJsonStringBuilder);
            }
            filterAllJsonStringBuilter.putJson(productId.toString(), productJsonStringBuilder);
        }
        return filterAllJsonStringBuilter.toString();
    }

    public void setAllSentimentalHashMap(JSONObject allSentimentalJsonObject) throws JSONException {
        String[] productIds = JSONObject.getNames(allSentimentalJsonObject);
        for (String productId : productIds) {
            JSONObject productJsonObject = allSentimentalJsonObject.getJSONObject(productId);
            String[] sentimentTypes = JSONObject.getNames(productJsonObject);
            for (String sentimentType : sentimentTypes) {
                JSONObject sentimentTypeJsonObject = productJsonObject.getJSONObject(sentimentType);
                String[] reviewIds = JSONObject.getNames(sentimentTypeJsonObject);
                for (String reviewId : reviewIds) {
                    JSONObject reviewJsonObject = sentimentTypeJsonObject.getJSONObject(reviewId);
                    String[] sentenceIds = JSONObject.getNames(reviewJsonObject);
                    HashMap<Integer, Integer> reviewHashMap = getAllSentimentalHashMap(Integer.valueOf(productId), Integer.valueOf(sentimentType), Integer.valueOf(reviewId));
                    for (String sentenceId : sentenceIds) {
                        reviewHashMap.put(Integer.valueOf(sentenceId), reviewJsonObject.getInt(sentenceId));
                    }
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    public HashMap<Integer, Integer> getAllSentimentalHashMap(int productId, int sentimentType, int reviewId) {
        HashMap productHashMap = getProductHashMap(allSentimentalHashMap, productId);
        HashMap sentimentTypeHashMap = getSentimentTypeHashMap(productHashMap, sentimentType);
        return getReviewHashMap(sentimentTypeHashMap, reviewId);
    }

    public HashMap<Integer, HashMap> getAllSentimentalHashMap() {
        return allSentimentalHashMap;
    }

    @SuppressWarnings("unchecked")
    public String getAllSentimentalJsonString() {
        JsonObjectStringBuilder allSentimentalJsonStringBuilder = new JsonObjectStringBuilder();
        Set<Integer> productIdSet = allSentimentalHashMap.keySet();
        for (Integer productId : productIdSet) {
            JsonObjectStringBuilder productJsonStringBuilder = new JsonObjectStringBuilder();
            HashMap<Integer, HashMap> productHashMap = allSentimentalHashMap.get(productId);
            Set<Integer> sentimentTypeSet = productHashMap.keySet();
            for (Integer sentimentType : sentimentTypeSet) {
                JsonObjectStringBuilder sentimentTypeJsonStringBuilder = new JsonObjectStringBuilder();
                HashMap<Integer, HashMap> sentimentTypeHashMap = productHashMap.get(sentimentType);
                Set<Integer> reviewIdSet = sentimentTypeHashMap.keySet();
                for (Integer reviewId : reviewIdSet) {
                    JsonObjectStringBuilder reviewJsonStringBuilder = new JsonObjectStringBuilder();
                    HashMap<Integer, Integer> reviewHashMap = sentimentTypeHashMap.get(reviewId);
                    Set<Integer> sentenceIdSet = reviewHashMap.keySet();
                    for (Integer sentenceId : sentenceIdSet) {
                        reviewJsonStringBuilder.putValue(sentenceId.toString(), reviewHashMap.get(sentenceId));
                    }
                    sentimentTypeJsonStringBuilder.putJson(reviewId.toString(), reviewJsonStringBuilder);
                }
                productJsonStringBuilder.putJson(sentimentType.toString(), sentimentTypeJsonStringBuilder);
            }
            allSentimentalJsonStringBuilder.putJson(productId.toString(), productJsonStringBuilder);
        }
        return allSentimentalJsonStringBuilder.toString();
    }

    public void setFilterSentimentalHashMap(JSONObject filterSentimentalJsonObject) throws JSONException {
        String[] productIds = JSONObject.getNames(filterSentimentalJsonObject);
        for (String productId : productIds) {
            JSONObject productJsonObject = filterSentimentalJsonObject.getJSONObject(productId);
            String[] originAttrs = JSONObject.getNames(productJsonObject);
            for (String originAttr : originAttrs) {
                JSONObject originAttrJsonObject = productJsonObject.getJSONObject(originAttr);
                String[] sentimentTypes = JSONObject.getNames(originAttrJsonObject);
                for (String sentimentType : sentimentTypes) {
                    JSONObject sentimentTypeJsonObject = originAttrJsonObject.getJSONObject(sentimentType);
                    String[] reviewIds = JSONObject.getNames(sentimentTypeJsonObject);
                    for (String reviewId : reviewIds) {
                        JSONObject reviewJsonObject = sentimentTypeJsonObject.getJSONObject(reviewId);
                        String[] sentenceIds = JSONObject.getNames(reviewJsonObject);
                        HashMap<Integer, Integer> reviewHashMap = getFilterSentimentalHashMap(Integer.valueOf(productId), originAttr, Integer.valueOf(sentimentType), Integer.valueOf(reviewId));
                        for (String sentenceId : sentenceIds) {
                            reviewHashMap.put(Integer.valueOf(sentenceId), reviewJsonObject.getInt(sentenceId));
                        }
                    }
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    public HashMap<Integer, Integer> getFilterSentimentalHashMap(int productId, String originAttr, int sentimentType, int reviewId) {
        HashMap productHashMap = getProductHashMap(filterSentimentalHashMap, productId);
        HashMap originAttrHashMap = getOriginAttrHashMap(productHashMap, originAttr);
        HashMap sentimentTypeHashMap = getSentimentTypeHashMap(originAttrHashMap, sentimentType);
        return getReviewHashMap(sentimentTypeHashMap, reviewId);
    }

    public HashMap<Integer, HashMap> getFilterSentimentalHashMap() {
        return filterSentimentalHashMap;
    }

    @SuppressWarnings("unchecked")
    public String getFilterSentimentalJsonString() {
        JsonObjectStringBuilder filterSentimentalJsonStringBuilder = new JsonObjectStringBuilder();
        Set<Integer> productIdSet = filterSentimentalHashMap.keySet();
        for (Integer productId : productIdSet) {
            JsonObjectStringBuilder productJsonStringBuilder = new JsonObjectStringBuilder();
            HashMap<String, HashMap> productHashMap = filterSentimentalHashMap.get(productId);
            Set<String> originAttrSet = productHashMap.keySet();
            for (String originAttr : originAttrSet) {
                JsonObjectStringBuilder originAttrJsonStringBuilder = new JsonObjectStringBuilder();
                HashMap<Integer, HashMap> originAttrHashMap = productHashMap.get(originAttr);
                Set<Integer> sentimentTypeSet = originAttrHashMap.keySet();
                for (Integer sentimentType : sentimentTypeSet) {
                    JsonObjectStringBuilder sentimentTypeJsonStringBuilder = new JsonObjectStringBuilder();
                    HashMap<Integer, HashMap> sentimentTypeHashMap = originAttrHashMap.get(sentimentType);
                    Set<Integer> reviewIdSet = sentimentTypeHashMap.keySet();
                    for (Integer reviewId : reviewIdSet) {
                        JsonObjectStringBuilder reviewJsonStringBuilder = new JsonObjectStringBuilder();
                        HashMap<Integer, Integer> reviewHashMap = sentimentTypeHashMap.get(reviewId);
                        Set<Integer> sentenceIdSet = reviewHashMap.keySet();
                        for (Integer sentenceId : sentenceIdSet) {
                            reviewJsonStringBuilder.putValue(sentenceId.toString(), reviewHashMap.get(sentenceId));
                        }
                        sentimentTypeJsonStringBuilder.putJson(reviewId.toString(), reviewJsonStringBuilder);
                    }
                    originAttrJsonStringBuilder.putJson(sentimentType.toString(), sentimentTypeJsonStringBuilder);
                }
                productJsonStringBuilder.putJson(originAttr, originAttrJsonStringBuilder);
            }
            filterSentimentalJsonStringBuilder.putJson(productId.toString(), productJsonStringBuilder);
        }
        return filterSentimentalJsonStringBuilder.toString();
    }
}
