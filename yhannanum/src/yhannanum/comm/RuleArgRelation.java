package yhannanum.comm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Arthur on 2014. 4. 25..
 */
public class RuleArgRelation {

    public class IndexOffset {
        public Integer min = null;

        public Integer max = null;

        @Override
        public String toString() {
            String str = "";
            if (min != null) str += " min : " + min.toString();
            if (max != null) str += " max : " + max.toString();
            return str;
        }
    }

    private IndexOffset getIndexOffset(MorphemeIndex[] indices1, MorphemeIndex[] indices2) {
        if (indices1 == null || indices2 == null || indices1.length == 0 || indices2.length == 0) return null;
        IndexOffset indexOffset = new IndexOffset();
        indexOffset.min = Math.abs(indices1[0].getEojeol() - indices2[0].getEojeol());
        indexOffset.max = indexOffset.min;
        for (MorphemeIndex index1 : indices1) {
            for (MorphemeIndex index2 : indices2) {
                Integer offset = Math.abs(index1.getEojeol() - index2.getEojeol());
                if (offset < indexOffset.min) indexOffset.min = offset;
                if (offset > indexOffset.max) indexOffset.max = offset;
            }
        }

        return indexOffset;
    }

    private Integer id = null;

    private RuleArgument target = null;

    private RuleArgument attr = null;

    private RuleArgument sentiment = null;

    private MorphemeIndex[] targetIndices = null;

    private MorphemeIndex[] attrIndices = null;

    private MorphemeIndex[] sentimentIndices = null;

    private IndexOffset target_attr = null;

    private IndexOffset attr_sentiment = null;

    private IndexOffset target_sentiment = null;

    private Integer sentimentType = null;

    public RuleArgRelation(Rule rule) {
        this(rule, null);
    }

    public RuleArgRelation(Rule rule, String[] roleTags) {
        id = rule.getId();
        target = new RuleArgument(rule.getTargets(), rule.getTargetIndices(), rule.getTargetTags());
        attr = new RuleArgument(rule.getAttrs(), rule.getAttrIndices(), rule.getAttrTags());
        sentiment = new RuleArgument(rule.getSentiments(), rule.getSentimentIndices(), rule.getSentimentTags());
        target.setIndicesToAbsCord();
        attr.setIndicesToAbsCord();
        sentiment.setIndicesToAbsCord();
        targetIndices = rule.getTargetIndices();
        attrIndices = rule.getAttrIndices();
        sentimentIndices = rule.getSentimentIndices();
        sentimentType = rule.getSentimentType();
        if (roleTags != null && sentimentIndices != null) {
            String[] tags = sentiment.getTags().clone();
            sentiment.deleteExtraMorphemes(new String[]{"NC", "NB", "NN", "NP", "NQ", "PV", "PA", "PX", "F", "MA", "MM", "SE", "SP", "SU"});
            List<String> roleTagList = Arrays.asList(roleTags);
            List<MorphemeIndex> indexList = new ArrayList<MorphemeIndex>(Arrays.asList(sentimentIndices));
            List<String> tagList = new ArrayList<String>(Arrays.asList(tags));
            for (int i = 0; i < tagList.size(); ) {
                if (roleTagList.contains(tagList.get(i))) {
                    i++;
                } else {
                    indexList.remove(i);
                    tagList.remove(i);
                }
            }
            sentimentIndices = indexList.toArray(new MorphemeIndex[indexList.size()]);
        }
        target_attr = getIndexOffset(targetIndices, attrIndices);
        attr_sentiment = getIndexOffset(attrIndices, sentimentIndices);
        target_sentiment = getIndexOffset(targetIndices, sentimentIndices);
    }

    public static RuleArgRelation[] getRuleRelations(Rule[] rules, String[] roleTags) {
        RuleArgRelation[] ruleArgRelations = new RuleArgRelation[rules.length];
        for (int i = 0; i < rules.length; i++) {
            ruleArgRelations[i] = new RuleArgRelation(rules[i], roleTags);
        }

        return ruleArgRelations;
    }

    public Integer getId() { return id; }

    public RuleArgument getTarget() { return target; }

    public RuleArgument getAttr() { return attr; }

    public RuleArgument getSentiment() { return sentiment; }

    public MorphemeIndex[] getTargetIndices() { return targetIndices; }

    public MorphemeIndex[] getAttrIndices() { return attrIndices; }

    public MorphemeIndex[] getSentimentIndices() { return sentimentIndices; }

    public IndexOffset getTarget_attr() { return target_attr; }

    public IndexOffset getAttr_sentiment() { return attr_sentiment; }

    public IndexOffset getTarget_sentiment() { return target_sentiment; }

    public Integer getSentimentType() { return sentimentType; }

    @Override
    public String toString() {
        String str = "";
        if (id != null) str += " id : " + id.toString();
        if (target != null) str += " target : " + target.toString();
        if (attr != null) str += " attr : " + attr.toString();
        if (sentiment != null) str += " sentiment : " + sentiment.toString();
        if (targetIndices != null) str += " targetIndices : " + Arrays.deepToString(targetIndices);
        if (attrIndices != null) str += " attrIndices : " + Arrays.deepToString(attrIndices);
        if (sentimentIndices != null) str += " sentimentIndices : " + Arrays.deepToString(sentimentIndices);
        if (target_attr != null)  str += " target_attr : " + target_attr.toString();
        if (attr_sentiment != null) str += " attr_sentiment : " + attr_sentiment.toString();
        if (target_sentiment != null) str += " target_sentiment : " + target_sentiment.toString();
        return str;
    }

    public static boolean checkEojeolIndices(MorphemeIndex[] indices1, MorphemeIndex[] indices2, IndexOffset indexOffset, int plusEojeolRange) {
        if (indexOffset == null || indexOffset.max == null || indexOffset.max == null) return true;
        else if (indices1 == null || indices1.length == 0 || indices2 == null || indices2.length == 0) return false;
        int max = 0;
        for (MorphemeIndex index1 : indices1) {
            for (MorphemeIndex index2 : indices2) {
                Integer offset = Math.abs(index1.getEojeol() - index2.getEojeol());
                if (offset > max) max = offset;
            }
        }

        return (max <= indexOffset.max + plusEojeolRange);
    }

    @SuppressWarnings("unchecked")
    public static List<MorphemeIndex[]>[] getCheckedEojeolIndices(List<MorphemeIndex[]> target, List<MorphemeIndex[]> attr, List<MorphemeIndex[]> sentiment,
                                                                  RuleArgRelation.IndexOffset target_attr, RuleArgRelation.IndexOffset attr_sentiment,
                                                                  RuleArgRelation.IndexOffset target_sentiment) {
        List<MorphemeIndex[]> acceptableTarget = new ArrayList<MorphemeIndex[]>();
        List<MorphemeIndex[]> acceptableAttr = new ArrayList<MorphemeIndex[]>();
        List<MorphemeIndex[]> acceptableSentiment = new ArrayList<MorphemeIndex[]>();
        for (MorphemeIndex[] targetIndices : target) {
            for (MorphemeIndex[] attrIndices : attr) {
                for (MorphemeIndex[] sentimentIndices : sentiment) {
                    if (RuleArgRelation.checkEojeolIndices(targetIndices, attrIndices, target_attr, 10) &&
                            RuleArgRelation.checkEojeolIndices(attrIndices, sentimentIndices, attr_sentiment, 2) &&
                            RuleArgRelation.checkEojeolIndices(targetIndices, sentimentIndices, target_sentiment, 10)) {
                        acceptableTarget.add(targetIndices);
                        acceptableAttr.add(attrIndices);
                        acceptableSentiment.add(sentimentIndices);
                    }
                }
            }
        }
        if (acceptableTarget.size() == 0 || acceptableAttr.size() == 0 || acceptableSentiment.size() == 0) return null;
        List<List<MorphemeIndex[]>> acceptableEojeolIndicesList = new ArrayList<List<MorphemeIndex[]>>();
        acceptableEojeolIndicesList.add(acceptableTarget);
        acceptableEojeolIndicesList.add(acceptableAttr);
        acceptableEojeolIndicesList.add(acceptableSentiment);
        return acceptableEojeolIndicesList.toArray(new List[3]);
    }

    @SuppressWarnings("unchecked")
    public static List<MorphemeIndex[]>[] getCheckedEojeolIndices(List<MorphemeIndex[]> indices1, List<MorphemeIndex[]> indices2,
                                                                  RuleArgRelation.IndexOffset indexOffset, int plusEojeolRange) {
        List<MorphemeIndex[]> acceptableIndices1 = new ArrayList<MorphemeIndex[]>();
        List<MorphemeIndex[]> acceptableIndices2 = new ArrayList<MorphemeIndex[]>();
        for (MorphemeIndex[] eojeolIndices1 : indices1) {
            for (MorphemeIndex[] eojeolIndices2 : indices2) {
                if (RuleArgRelation.checkEojeolIndices(eojeolIndices1, eojeolIndices2, indexOffset, plusEojeolRange)) {
                    acceptableIndices1.add(eojeolIndices1);
                    acceptableIndices2.add(eojeolIndices2);
                }
            }
        }
        List<List<MorphemeIndex[]>> acceptableEojeolIndicesList = new ArrayList<List<MorphemeIndex[]>>();
        acceptableEojeolIndicesList.add(acceptableIndices1);
        acceptableEojeolIndicesList.add(acceptableIndices2);
        return acceptableEojeolIndicesList.toArray(new List[2]);
    }
}
