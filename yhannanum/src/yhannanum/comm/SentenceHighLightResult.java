package yhannanum.comm;

import kr.ac.kaist.swrc.jhannanum.comm.Sentence;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Arthur on 2014. 5. 6..
 */
public class SentenceHighLightResult {
    // Todo delete sentimentType

    private int reviewId = -1;
    private int sentenceId = 0;
    private String plainSentence = null;
    private Sentence sentence = null;
    private ArrayList<String> positiveOriginAttr = new ArrayList<String>();
    private ArrayList<String> negativeOriginAttr = new ArrayList<String>();
    private ArrayList<String> neutralOriginAttr = new ArrayList<String>();
    private MorphemeIndex[] targetIndices = null;
    private MorphemeIndex[] attrIndices = null;
    private MorphemeIndex[] sentimentIndices = null;

    public SentenceHighLightResult(int reviewId, int sentenceId, String plainSentence, Sentence sentence, String originAttr, int sentimentType, MorphemeIndex[] targetIndices,
                                   MorphemeIndex[] attrIndices, MorphemeIndex[] sentimentIndices) {
        this.reviewId = reviewId;
        this.sentenceId = sentenceId;
        this.plainSentence = plainSentence;
        this.sentence = sentence;
        if(sentimentType == 0) {
            positiveOriginAttr.add(originAttr);
        } else if (sentimentType == 1) {
            negativeOriginAttr.add(originAttr);
        } else if (sentimentType == 2) {
            neutralOriginAttr.add(originAttr);
        }
        this.targetIndices = targetIndices;
        this.attrIndices = attrIndices;
        this.sentimentIndices = sentimentIndices;
    }

    public SentenceHighLightResult(JSONObject jsonObject) throws JSONException {
        if (jsonObject.has("reviewId")) {
            reviewId = jsonObject.getInt("reviewId");
        }
        if (jsonObject.has("sentenceId")) {
            sentenceId = jsonObject.getInt("sentenceId");
        }
        if (jsonObject.has("plainSentence")) {
            plainSentence = jsonObject.getString("plainSentence");
        }
        if (jsonObject.has("sentence")) {
            sentence = new Sentence(jsonObject.getJSONObject("sentence"));
        }
        if (jsonObject.has("positiveOriginAttr")) {
            JSONArray positiveOriginAttrJsonArray = jsonObject.getJSONArray("positiveOriginAttr");
            positiveOriginAttr = new ArrayList<String>(positiveOriginAttrJsonArray.length());
            for (int i = 0; i < positiveOriginAttrJsonArray.length(); i++) {
                positiveOriginAttr.add(positiveOriginAttrJsonArray.getString(i));
            }
        }
        if (jsonObject.has("negativeOriginAttr")) {
            JSONArray negativeOriginAttrJsonArray = jsonObject.getJSONArray("negativeOriginAttr");
            negativeOriginAttr = new ArrayList<String>(negativeOriginAttrJsonArray.length());
            for (int i = 0; i < negativeOriginAttrJsonArray.length(); i++) {
                negativeOriginAttr.add(negativeOriginAttrJsonArray.getString(i));
            }
        }
        if (jsonObject.has("neutralOriginAttr")) {
            JSONArray neutralOriginAttrJsonArray = jsonObject.getJSONArray("neutralOriginAttr");
            neutralOriginAttr = new ArrayList<String>(neutralOriginAttrJsonArray.length());
            for (int i = 0; i < neutralOriginAttrJsonArray.length(); i++) {
                neutralOriginAttr.add(neutralOriginAttrJsonArray.getString(i));
            }
        }
        if (jsonObject.has("targetIndices")) {
            JSONArray targetIndexJsonArray = jsonObject.getJSONArray("targetIndices");
            targetIndices = new MorphemeIndex[targetIndexJsonArray.length()];
            for (int i = 0; i < targetIndexJsonArray.length(); i++) {
                targetIndices[i] = new MorphemeIndex(targetIndexJsonArray.getJSONArray(i));
            }
        }
        if (jsonObject.has("attrIndices")) {
            JSONArray attrIndexJsonArray = jsonObject.getJSONArray("attrIndices");
            attrIndices = new MorphemeIndex[attrIndexJsonArray.length()];
            for (int i = 0; i < attrIndexJsonArray.length(); i++) {
                attrIndices[i] = new MorphemeIndex(attrIndexJsonArray.getJSONArray(i));
            }
        }
        if (jsonObject.has("sentimentIndices")) {
            JSONArray sentimentIndexJsonArray = jsonObject.getJSONArray("sentimentIndices");
            sentimentIndices = new MorphemeIndex[sentimentIndexJsonArray.length()];
            for (int i = 0; i < sentimentIndexJsonArray.length(); i++) {
                sentimentIndices[i] = new MorphemeIndex(sentimentIndexJsonArray.getJSONArray(i));
            }
        }
    }

    public int getReviewId() { return reviewId; }

    public int getSentenceId() { return sentenceId; }

    public String getPlainSentence() { return plainSentence; }

    public Sentence getSentence() { return sentence; }

    public List<String> getPositiveOriginAttr() { return positiveOriginAttr; }

    public List<String> getNegativeOriginAttr() { return negativeOriginAttr; }

    public List<String> getNeutralOriginAttr() { return neutralOriginAttr; }

    public MorphemeIndex[] getTargetIndices() { return targetIndices; }

    public MorphemeIndex[] getAttrIndices() { return attrIndices; }

    public MorphemeIndex[] getSentimentIndices() { return sentimentIndices; }

    public void addIndices(String originAttr, int sentimentType, MorphemeIndex[] targetIndices, MorphemeIndex[] attrIndices, MorphemeIndex[] sentimentIndices) {
        if(sentimentType == 0) {
            positiveOriginAttr.add(originAttr);
        } else if (sentimentType == 1) {
            negativeOriginAttr.add(originAttr);
        } else if (sentimentType == 2) {
            neutralOriginAttr.add(originAttr);
        }
        if (targetIndices != null) {
            this.targetIndices = MorphemeIndex.getMergedIndices(this.targetIndices, targetIndices);
        }
        if (attrIndices != null) {
            this.attrIndices = MorphemeIndex.getMergedIndices(this.attrIndices, attrIndices);
        }
        if (sentimentIndices != null) {
            this.sentimentIndices = MorphemeIndex.getMergedIndices(this.sentimentIndices, sentimentIndices);
        }
    }

    public JSONObject getSentenceHighLightOutputJsonObject() throws JSONException {
        JSONObject sentenceOutput = new JSONObject();
        sentenceOutput.put("sentence", plainSentence);
        JSONObject highlighting = new JSONObject();
        String[] plainEojeols = sentence.getPlainEojeols();
        if (attrIndices != null) {
            Integer[] attrEojeolIndices = MorphemeIndex.extractEojeolIndicesArray(attrIndices);
            JSONArray attrHighlightingIndices = getHighlightingIndicesJsonArray(attrEojeolIndices, plainEojeols, plainSentence);
            highlighting.put("attr", attrHighlightingIndices);
        }
        if (sentimentIndices != null) {
            Integer[] sentimentEojeolIndices = MorphemeIndex.extractEojeolIndicesArray(sentimentIndices);
            JSONArray sentimentHighlightingIndices = getHighlightingIndicesJsonArray(sentimentEojeolIndices, plainEojeols, plainSentence);
            highlighting.put("sentiment", sentimentHighlightingIndices);
        }
        sentenceOutput.put("highlighting", highlighting);

        return sentenceOutput;
    }

    private JSONArray getHighlightingIndicesJsonArray(Integer[] indices, String[] eojeols, String sentence) {
        JSONArray highlightingIndices = new JSONArray();
        Arrays.sort(indices);

        int sentenceIndex = 0;
        int i = 0;

        for (Integer index : indices) {
            for (; i < index; i++) {
                String eojeol = eojeols[i];
                sentenceIndex = sentence.indexOf(eojeol, sentenceIndex);
                sentenceIndex += eojeol.length() - 1;
            }
            int startingIndex = sentence.indexOf(eojeols[index], sentenceIndex);
            sentenceIndex = startingIndex + eojeols[index].length();
            JSONArray eojeolIndex = new JSONArray();
            eojeolIndex.put(startingIndex);
            eojeolIndex.put(sentenceIndex - 1);
            highlightingIndices.put(eojeolIndex);
            i++;
        }
        return highlightingIndices;
    }

    public JSONObject toJsonObject() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("reviewId", reviewId);
        jsonObject.put("sentenceId", sentenceId);
        if (plainSentence != null) {
            jsonObject.put("plainSentence", plainSentence);
        }
        if (sentence != null) {
            jsonObject.put("sentence", sentence.toJsonObject());
        }
        jsonObject.put("positiveOriginAttr", positiveOriginAttr);
        jsonObject.put("negativeOriginAttr", negativeOriginAttr);
        jsonObject.put("neutralOriginAttr", neutralOriginAttr);
        if (targetIndices != null) {
            JSONArray targetIndexJsonArray = new JSONArray();
            for (MorphemeIndex targetIndex : targetIndices) {
                targetIndexJsonArray.put(targetIndex.toJsonArray());
            }
            jsonObject.put("targetIndices", targetIndexJsonArray);
        }
        if (attrIndices != null) {
            JSONArray attrIndexJsonArray = new JSONArray();
            for (MorphemeIndex attrIndex : attrIndices) {
                attrIndexJsonArray.put(attrIndex.toJsonArray());
            }
            jsonObject.put("attrIndices", attrIndexJsonArray);
        }
        if (sentimentIndices != null) {
            JSONArray sentimentIndexJsonArray = new JSONArray();
            for (MorphemeIndex sentimentIndex : sentimentIndices) {
                sentimentIndexJsonArray.put(sentimentIndex.toJsonArray());
            }
            jsonObject.put("sentimentIndices", sentimentIndexJsonArray);
        }
        return jsonObject;
    }

    @Override
    public String toString() {
        String str = "";
        str += " review_id : " + reviewId;
        str += " sentence_id : " + sentenceId;
        str += " sentence : " + plainSentence;
        if (positiveOriginAttr.size() > 0) str += " positiveOriginAttr : " + Arrays.deepToString(positiveOriginAttr.toArray());
        if (negativeOriginAttr.size() > 0) str += " negativeOriginAttr : " + Arrays.deepToString(negativeOriginAttr.toArray());
        if (neutralOriginAttr.size() > 0) str += " neutralOriginAttr : " + Arrays.deepToString(neutralOriginAttr.toArray());
        if (targetIndices != null) str += " target_index : " + Arrays.deepToString(targetIndices);
        if (attrIndices != null) str += " attr_index : " + Arrays.deepToString(attrIndices);
        if (sentimentIndices != null) str += " sentiment_index : " + Arrays.deepToString(sentimentIndices);
        return str;
    }
}
