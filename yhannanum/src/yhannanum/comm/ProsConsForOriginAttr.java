package yhannanum.comm;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Set;

/**
 * Created by Arthur on 2014. 6. 11..
 */
public class ProsConsForOriginAttr {
    private SentimentForOriginAttr prosSentences = new SentimentForOriginAttr(0);
    private SentimentForOriginAttr consSentences = new SentimentForOriginAttr(1);

    public void addSentence(int sentimentType, String originAttr, int ruleId, String sentence) {
        if (sentimentType == 0) {
            prosSentences.addSentence(originAttr, ruleId, sentence);
        } else if (sentimentType == 1) {
            consSentences.addSentence(originAttr, ruleId, sentence);
        }
    }

    public void setSentence(int sentimentType, String originAttr, int ruleId, String sentence, int count) {
        if (sentimentType == 0) {
            prosSentences.setSentence(originAttr, ruleId, sentence, count);
        } else if (sentimentType == 1) {
            consSentences.setSentence(originAttr, ruleId, sentence, count);
        }
    }

    public String getSentence(int sentimentType, String originAttr) {
        if (sentimentType == 0) {
            return prosSentences.getSentence(originAttr);
        } else if (sentimentType == 1) {
            return consSentences.getSentence(originAttr);
        } else {
            return null;
        }
    }

    public JSONArray toJsonArray() throws JSONException {
        JSONArray prosConsJsonArray = new JSONArray();
        JSONArray prosJsonArray = prosSentences.toJsonArray();
        JSONArray consJsonArray = consSentences.toJsonArray();
        for (int i = 0; i < prosJsonArray.length(); i++) {
            prosConsJsonArray.put(prosJsonArray.get(i));
        }
        for (int i = 0; i < consJsonArray.length(); i++) {
            prosConsJsonArray.put(consJsonArray.get(i));
        }
        return prosConsJsonArray;
    }

    class SentimentForOriginAttr {
        private int sentimentType = -1;
        private HashMap<String, RulesSentenceCount> sentimentSentences = new HashMap<String, RulesSentenceCount>();

        SentimentForOriginAttr(int sentimentType) {
            this.sentimentType = sentimentType;
        }

        void addSentence(String originAttr, int ruleId, String sentence) {
            if (!sentimentSentences.containsKey(originAttr)) {
                sentimentSentences.put(originAttr, new RulesSentenceCount());
            }
            RulesSentenceCount rulesSentenceCount = sentimentSentences.get(originAttr);
            rulesSentenceCount.addRuleSentenceCount(ruleId, sentence);
        }

        void setSentence(String originAttr, int ruleId, String sentence, int count) {
            if (!sentimentSentences.containsKey(originAttr)) {
                sentimentSentences.put(originAttr, new RulesSentenceCount());
            }
            RulesSentenceCount rulesSentenceCount = sentimentSentences.get(originAttr);
            rulesSentenceCount.setRuleSentenceCount(ruleId, sentence, count);
        }

        String getSentence(String originAttr) {
            if (sentimentSentences.containsKey(originAttr)) {
                return sentimentSentences.get(originAttr).getMaxCountSentence();
            } else {
                return null;
            }
        }

        JSONArray toJsonArray() throws JSONException {
            JSONArray sentimentSentencesJsonArray = new JSONArray();
            Set<String> originAttrSet = sentimentSentences.keySet();
            for (String originAttr : originAttrSet) {
                RulesSentenceCount rulesSentenceCount = sentimentSentences.get(originAttr);
                JSONArray rulesSentenceCountJsonArray = rulesSentenceCount.toJsonArray();
                for (int i = 0; i < rulesSentenceCountJsonArray.length(); i++) {
                    JSONObject ruleSentenceCountJsonObject = rulesSentenceCountJsonArray.getJSONObject(i);
                    ruleSentenceCountJsonObject.put("originAttr", originAttr);
                    ruleSentenceCountJsonObject.put("sentimentType", sentimentType);
                    sentimentSentencesJsonArray.put(ruleSentenceCountJsonObject);
                }
            }
            return sentimentSentencesJsonArray;
        }
    }
}